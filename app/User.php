<?php
namespace App;

use App\Document;
use App\Ticket;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    protected $guarded = [
        'id', 'password', 'api_token', 'remember_token', 'created_at', 'updated_at', 'selected', 'loading', 'relocation_files', 'files'
    ];

    protected $appends = ['selected', 'loading'];

    protected $casts = [
        'relocation_files' => 'array',
        'files' => 'array',
        'roles' => 'array'
    ];

    protected $perPage = 10;

    public static function getAuthUser()
    {
        $user = auth()->user();
        $user->password = '';
        return $user;
    }
    public function getSelectedAttribute()
    {
        return false;
    }

    public function getLoadingAttribute()
    {
        return false;
    }

    public function setToken()
    {
        $this->api_token = Str::random(10) . $this->id;
        $this->save();
    }

    public function setPassword()
    {
        if (isset(request()->password) && strlen(request()->password) > 0) {
            $this->password = bcrypt(request()->password);
            $this->save();
        }
    }

    public function Documents()
    {
        return $this->hasMany(Document::class, "customer_id", "id");
    }


    public function ContactClients()
    {
        return $this->hasMany(ClientSegmentContact::class);
    }

    public function Tickets()
    {
        return $this->hasMany(Ticket::class);
    }

    public function isBackOffice()
    {
        return $this->isAdmin() || $this->isSales();
    }

    public function isAdmin()
    {
        return in_array(1, auth()->user()->roles);
    }

    public function isSales()
    {
        return in_array(2, auth()->user()->roles);
    }

    public function isCallCenter()
    {
        return in_array(4, auth()->user()->roles);
    }

    public function isOnlyCallCenter()
    {
        return $this->isAdmin() == false && $this->isCallCenter();
    }

    public function getRoles()
    {
        $roles = new Collection();

        if ($this->isAdmin())
            $roles->push("admin");

        if ($this->isCustomer())
            $roles->push("customer");

        if ($this->isSales())
            $roles->push("sales");

        if ($this->isCallCenter())
            $roles->push("callCenter");

        return $roles;
    }

    public function isCustomer()
    {
        return in_array(3, auth()->user()->roles);
    }

    public function hasRoles($roles)
    {

        foreach ($roles as $role) {

            if ($role == 'admin' && $this->isAdmin())
                return true;

            if ($role == 'sales' && $this->isSales())
                return true;

            if ($role == 'customers' && $this->isCustomer())
                return true;

            if ($role == 'callCenter' && $this->isCallCenter())
                return true;


        }

        return false;
    }

    public function scopeBO($query)
    {

        return $query->where(function ($query) {
            $query->whereRaw(
                'JSON_CONTAINS(roles, \'[1,2,4]\')'
            );
        })->get();
    }


    public function scopeCallCenter($query)
    {
        return $query->where(function ($query) {
            $query->where('roles', 'like', '%4%');
        })->get();

    }
}
