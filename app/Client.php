<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $guarded = ['id', 'selected', 'loading'];

    protected $appends = ['selected', 'loading'];

    protected $fillable = [];

    protected $casts = [
        'files' => 'array'
    ];


    public function getSelectedAttribute()
    {
        return false;
    }

    public function getLoadingAttribute()
    {
        return false;
    }

    public function Company()
    {
        return $this->belongsTo(AObject::class, 'company_type_id', 'id');
    }

    public function Segments()
    {
        return $this->hasMany(ClientSegment::class);
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1)->get();
    }
}
