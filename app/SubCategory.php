<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $table = 'sub_categories';

    protected $guarded = ['id', 'selected', 'loading'];

    protected $fillable = [];

    protected $appends = ['selected', 'loading'];

    protected $casts = [
        'entities' => 'array'
    ];

    public function getSelectedAttribute()
    {
        return false;
    }

    public function getLoadingAttribute()
    {
        return false;
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1)->get();
    }

    public function scopeEntities($query, $values)
    {

        $values = json_encode($values);
        $sql = "active =1 and  JSON_CONTAINS(entities,'{$values}')";

        return $query->whereRaw($sql)->get();
    }

    public function Category()
    {
        return $this->belongsTo(Category::class, "category_id");
    }
}
