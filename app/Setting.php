<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    public static function mainPage () {
        return 'partials.' . app()->getLocale() . '.main';
    }
}
