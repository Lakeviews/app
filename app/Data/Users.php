<?php

namespace App\Data;

use App\FileManager;
use App\Mail\MailMessage;
use App\Msg;
use App\Product;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class Users 
{

	public function paginate()
	{
		$data = User::where('name', 'like' , '%'. request()->search .'%')->orWhere('email', 'like' , '%'. request()->search .'%');

		if(isset(request()->sorts))
		{


			foreach(request()->sorts as $key => $value)
			{	

				$data = $data->orderBy($key, $value);
			}
		}

		return $data->paginate();

	}

	public function create(){

		$user = new User();
		$user->id = -1;
		$user->name = '';
		$user->email = '';
		$user->password = '';
		$user->api_token ='';
		$user->active = 1;
		$user->roles = [];
		$user->birthdate = Carbon::now()->addYears(-20)->toDateString();
		$user->gender = 1;
		$user->mobile = '';
		$user->phone = '';	
		$user->address= '';
		$user->town = '';
		$user->postcode = '';
		$user->country  = '';
		$user->notes = '';
		//RELOACTION
		$user->relocation= 0;
		$user->relocation_company_id = null;
		$user->relocation_comments = '';
		$user->relocation_files = [];
		
		$user->files = [];
		
		return $user;
	}

	public function save(){

		$user = new User();
		$rules =['name' => 'required', 
		'email' => 'required|email|unique:users,email,' . request()->id . ',id'];

		if(request()->id == -1)
			$rules['password'] = 'required|min:6|max:10';
		else
			$user = User::find(request()->id);

		if(request()->relocation == 1)
			$rules['relocation_company_id'] = 'required|exists:companies,id';

		$validData = request()->validate($rules);

		try
		{
			$user->fill(request()->all());
			$user->save();
			$user->setPassword();
			if(request()->id == -1)
				$user->setToken();
			$user->save();
			
		
			$user->relocation_files = FileManager::saveField(request()->get('relocation_files'), $user->relocation_files, 'users/'.$user->id.'//relocation/');
			$user->files = FileManager::saveField(request()->get('files'), $user->files, 'users/'.$user->id.'//files/');
			$user->save();

			//reset password
			$user->password = '';

			return Msg::modelSaved($user, 'User saved');
		}
		catch(\Exception $ex)
		{
			return Msg::error($ex->getMessage());
		}
	}

	public function delete($ids)
	{
		try
		{
			$ids = explode(',', $ids);
			

			foreach ($ids as $id) {
				$user = User::find($id);
				FileManager::deleteDir('users/'.$id);
				$user->delete();
			}

			return Msg::success(count($ids) > 1  ? 'Users Deleted' : 'User Deleted');
		}
		catch(\Exception $ex)
		{
			return Msg::error($ex->getMessage());
		}

	}

	public function insurances($year){
		
	
		return  Product::whereHas('documents', function($query) use ($year) {

			$query->where('customer_id', '=', auth()->user()->id)->where('year', $year);
		})->with(['documents' => function ($query) use ($year) {
			$query->where('customer_id', '=', auth()->user()->id )->where('year', $year);
		},'documents.customer'])
	
		->get();


	}


	public function sendMessage(){

		$rules =['title' => 'required', 
		'name' => 'required',
		'email' => 'required|email',
		'phone' => 'required',
		'message' => 'required|min:50|max:255'];

		$validData = request()->validate($rules);
	
		try
		{
			
			Mail::to(env('MAIL_FROM'))->send(new MailMessage($validData, true));
			Mail::to($validData['email'])->send(new MailMessage($validData, false));
			return Msg::success('Message Sent');
		}
		catch(\Exception $ex)
		{
			return Msg::error($ex->getMessage());
		}
	}
}
