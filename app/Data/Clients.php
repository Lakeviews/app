<?php

namespace App\Data;

use App\Client;
use App\Msg;


class Clients
{

    public function paginate()
    {
        $data = Client::with('Company')->where([['name', 'like', '%' . request()->search . '%'],
        ['company_type_id','like', isset(request()->company_type) && request()->company_type > 0 ? request()->company_type : '%%']]);

        if (isset(request()->sorts)) {
            foreach (request()->sorts as $key => $value)
                $data = $data->orderBy($key, $value);
        }

        return $data->paginate();
    }

    public function create()
    {

        $client = new Client();
        $client->id = -1;
        $client->name = '';
        $client->type = 1;
        $client->company_type_id = '';
        $client->active = 1;
        $client->number = '';

        $client->contact_name = '';
        $client->contact_role = '';
        $client->contact_details = '';

        $client->description = '';

        $client->mobile = '';
        $client->phone = '';
        $client->phone_2 = '';
        $client->fax = '';
        $client->address = '';
        $client->town = '';
        $client->postcode = '';
        $client->country = '';
        $client->latitude = '';
        $client->longitude = '';

        $client->files =[];


        return $client;
    }

    public function save()
    {

        $client = new Client();

        $rules = [
            'name' => 'required',
            'active' => 'required',
            'company_type_id' => 'required|exists:objects,id',
        ];

		 //validate the request
        request()->validate($rules);

        if (request()->id > 0)
            $client = Client::find(request()->id);

        try {
            $client->fill(request()->all());
            $client->save();

            return Msg::modelSaved($client, 'Client saved');

        } catch (\Exception $ex) {
            return Msg::error($ex->getMessage());
        }
    }

    public function delete($ids)
    {
        try {
            $ids = explode(',', $ids);
            Client::destroy($ids);
            return Msg::success(count($ids) > 1 ? 'Clients Deleted' : 'Client Deleted');
        } catch (\Exception $ex) {
            return Msg::error($ex->getMessage());
        }
    }

}
