<?php

namespace App\Data;

use App\Company;
use App\Msg;
use Carbon\Carbon;

class Companies 
{

	public function paginate()
	{
		$data = Company::where('name', 'like' , '%'. request()->search .'%')
			->orWhere('description', 'like' , '%'. request()->search .'%');

		if(isset(request()->sorts))
		{
			foreach(request()->sorts as $key => $value)
				$data = $data->orderBy($key, $value);
		}

		return $data->paginate();
	}

	public function create(){

		$company = new Company();
		$company->id = -1;
		$company->name = '';
		$company->email = '';
		$company->description = '';
		$company->type = 1;
		$company->active = 1;
		$company->mobile = '';
		$company->phone = '';	
		$company->phone_2 = '';	
		$company->fax = '';	
		$company->address= '';
		$company->town = '';
		$company->postcode = '';
		$company->country  = '';
		$company->latitude = '';
		$company->longitude = '';

		return $company;
	}

	public function save(){

		$company = new Company();
		$rules = ['name' => 'required', 
		'type' => 'required',
		'email' => 'nullable|email|unique:companies,email,' . request()->id . ',id'];

		 //validate the request
		request()->validate($rules);

		if(request()->id > 0)
			$company = Company::find(request()->id);

		try
		{
			$company->fill(request()->all());
			$company->save();

			return Msg::modelSaved($company, 'Company saved');
		}
		catch(\Exception $ex)
		{
			return Msg::error($ex->getMessage());
		}
	}

	public function delete($ids)
	{
		try
		{
			$ids = explode(',', $ids);
			Company::destroy($ids);
			return Msg::success(count($ids) > 1  ? 'Companies Deleted' : 'Company Deleted');
		}
		catch(\Exception $ex)
		{
			return Msg::error($ex->getMessage());
		}
	}
}
