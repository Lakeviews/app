<?php

namespace App\Data;

use App\Category;
use App\Msg;


class Categories
{
  

    public function paginate()
    {
        $data = Category::where('name', 'like', '%' . request()->search . '%')
            ->orWhere('alias', 'like', '%' . request()->search . '%');

        if (isset(request()->sorts)) {
            foreach (request()->sorts as $key => $value)
                $data = $data->orderBy($key, $value);
        }

        return $data->paginate();
    }

    public function create()
    {

        $category = new Category();
        $category->id = -1;
        $category->name = '';
        $category->alias = '';
        $category->active = 1;
        $category->entities = [];

        return $category;
    }

    public function save()
    {

        $category = new Category();

        $rules = [
            'name' => 'required',
            'alias' => 'required',
            'active' => 'required',
            'entities' => 'required|array'
        ];

		 //validate the request
        request()->validate($rules);

        if (request()->id > 0)
            $category = Category::find(request()->id);

        try {
            $category->fill(request()->all());
            $category->save();

            return Msg::modelSaved($category, 'Category saved');
        } catch (\Exception $ex) {
            return Msg::error($ex->getMessage());
        }
    }

    public function delete($ids)
    {
        try {
            $ids = explode(',', $ids);
            Category::destroy($ids);
            return Msg::success(count($ids) > 1 ? 'Categories Deleted' : 'Category Deleted');
        } catch (\Exception $ex) {
            return Msg::error($ex->getMessage());
        }
    }
}
