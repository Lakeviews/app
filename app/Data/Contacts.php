<?php

namespace App\Data;

use App\Msg;
use App\ClientSegmentContact;
use Illuminate\Support\Carbon;
use App\ClientSegment;


class Contacts
{


    public function pdf($id)
    {

        $segment = ClientSegment::with('Client')->find($id);
        $contacts = $this->contacts($id)->orderBy('updated_at', 'desc')->get();

        return ['segment' => $segment, 'contacts' => $contacts];

    }

    public function contacts(int $id = null)
    {

        $segment_id = isset(request()->client_segment_id) ? request()->client_segment_id : $id;


        $data = ClientSegmentContact::with('User')
            ->where([
                ['client_segment_id', $segment_id],
                ['status', 'like', isset(request()->status) && request()->status >= 0 ? request()->status : '%%'],
            ]);

        if (strlen(request()->start) > 0 && strlen(request()->end) > 0) {
            $data = ClientSegmentContact::with('User')
                ->where([
                    ['client_segment_id', $segment_id],
                    ['status', 'like', isset(request()->status) && request()->status >= 0 ? request()->status : '%%'],
                    ['updated_at', '>=', request()->start],
                    ['updated_at', '<=', request()->end]
                ]);
        }


        if (strlen(request()->start) > 0 && strlen(request()->end) > 0 && request()->status == 3) {
            $data = ClientSegmentContact::with('User')
                ->where([
                    ['client_segment_id', $segment_id],
                    ['status', 'like', isset(request()->status) && request()->status >= 0 ? request()->status : '%%'],
                    ['contact_later_date', '>=', request()->start],
                    ['contact_later_date', '<=', request()->end]
                ]);

        }

        return $data;
    }

    public function paginate()
    {
        $data = $this->contacts();

        if (isset(request()->sorts)) {
            foreach (request()->sorts as $key => $value) {

                $data = $data->orderBy($key, $value);
            }
        }

        $data->orderBy('updated_at', 'desc');

        return $data->paginate();
    }

    public function calls()
    {

        // ['segment_type_id', 'like', isset(request()->segment_type_id) && request()->segment_type_id > 0 ? request()->segment_type_id : '%%'],
        // ['client_id', 'like', isset(request()->client_id) && request()->client_id > 0 ? request()->client_id : '%%']

        $data = ClientSegmentContact::with(['Segment', 'Segment.Client', 'Segment.Segment', 'User'])
            ->where([
                ['status', 3],
                ['contact_recall_status', 0],
                ['contact_later_name', 'like', isset(request()->search) && strlen(request()->search) > 0 ? '%' . request()->search . '%' : '%%']
            ])
            ->orWhere([
                ['status', 3],
                ['contact_recall_status', 0],
                ['contact_postcode', 'like', isset(request()->search) && strlen(request()->search) > 0 ? '%' . request()->search . '%' : '%%']
            ])
            ->orWhere([
                ['status', 3],
                ['contact_recall_status', 0],
                ['name', 'like', isset(request()->search) && strlen(request()->search) > 0 ? '%' . request()->search . '%' : '%%']
            ])
            ->WhereHas('Segment', function ($query) {
                $query->where([
                    ['client_id', 'like', isset(request()->client_id) && request()->client_id > 0 ? request()->client_id : '%%'],
                    ['segment_type_id', 'like', isset(request()->segment_type_id) && request()->segment_type_id > 0 ? request()->segment_type_id : '%%']
                ]);
            });

  

    
        

        if (isset(request()->user_id) && strlen(request()->user_id) > 0)
            $data = $data->where('user_id', request()->user_id);


        if (isset(request()->sorts)) {
            foreach (request()->sorts as $key => $value) {


                if ($key == 'client_id') {
                    $data = $data->join('client_segments  as u1', 'client_segments_contact.client_segment_id', '=', 'u1.id')

                        ->orderBy('u1.client_id', $value);
                } else if ($key == 'segment_type_id') {
                    $data = $data->join('client_segments  as u2', 'client_segments_contact.client_segment_id', '=', 'u2.id')

                        ->orderBy('u2.segment_type_id', $value);
                } else
                    $data = $data->orderBy($key, $value);
            }
        }

        $data->orderBy('client_segments_contact.updated_at', 'desc');

        return $data->paginate();
    }

    public function create()
    {

        $client_contacts = new ClientSegmentContact();
        $client_contacts->id = -1;
        $client_contacts->client_segment_id = null;
        $client_contacts->type = 1;

        $client_contacts->name = '';
        $client_contacts->contact_name = '';
        $client_contacts->contact_details = '';
        $client_contacts->contact_role = '';
        $client_contacts->contact_postcode = '';

        $client_contacts->status = null;
        $client_contacts->comments = '';

        $client_contacts->contact_later_name = null;
        $client_contacts->contact_later_role = null;
        $client_contacts->contact_later_details = null;
        $client_contacts->contact_later_date = null;
        $client_contacts->contact_recall_status = 0;

        return $client_contacts;
    }

    public function save()
    {

        $client_contacts = new ClientSegmentContact();

        $rules = [
            'client_segment_id' => 'required|exists:client_segments,id',
            'type' => 'required',
            'name' => 'required',
            'contact_name' => 'required',
            'contact_details' => 'required',
            'contact_role' => 'required',
            'status' => 'required',
            'comments' => 'required'
        ];

        if (request()->status == 3) {
            $rules['contact_later_name'] = 'required';
            $rules['contact_later_role'] = 'required';
            $rules['contact_later_details'] = 'required';
            $rules['contact_later_date'] = 'required';
        }


		//validate the request
        request()->validate($rules);

        if (request()->id > 0)
            $client_contacts = ClientSegmentContact::find(request()->id);

        try {
            $client_contacts->fill(request()->all());
            $client_contacts->user_id = auth()->user()->id;
            $client_contacts->save();

            return Msg::modelSaved($client_contacts, 'Contact saved');
        } catch (\Exception $ex) {
            return Msg::error($ex->getMessage());
        }
    }

    public function delete($ids)
    {
        try {
            $ids = explode(',', $ids);
            ClientSegmentContact::destroy($ids);
            return Msg::success(count($ids) > 1 ? 'Contact Deleted' : 'Contacts Deleted');
        } catch (\Exception $ex) {
            return Msg::error($ex->getMessage());
        }
    }
}