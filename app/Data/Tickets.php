<?php

namespace App\Data;


use App\Events\UserNotification;
use App\FileManager;
use App\Msg;
use App\Ticket;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Tickets
{

	public function paginate()
	{
		$data = [];

		if (isset(request()->origin) && request()->origin == "/dashboard") {
			$data = Ticket::with(['Customer', 'Product'])->where('title', 'like', '%' . request()->search . '%')->where('customer_id', auth()->user()->id);
		} else {
			$data = Ticket::with(['Customer', 'Product'])->where('title', 'like', '%' . request()->search . '%')
				->orWhereHas('Customer', function ($query) {
					$query->where('name', 'like', '%' . request()->search . '%');
				});
		}

		if (isset(request()->sorts)) {
			foreach (request()->sorts as $key => $value) {
				$data = $data->orderBy($key, $value);
			}
		}

		if (isset(request()->origin) && request()->origin == '/dashboard')
			return $data->paginate(6);
		else
			return $data->paginate();

	}

	public function create()
	{

		$ticket = new Ticket();
		$ticket->id = -1;
		$ticket->title = '';
		$ticket->customer_id = null;
		$ticket->product_id = '';
		$ticket->log = [];
		$ticket->status = 1;
		$ticket->files = [];
		return $ticket;
	}

	public function save()
	{


		$ticket = new Ticket();

		$rules = [
			'title' => 'required',
			'product_id' => 'required'
		];



		if (request()->id > 0) {
			$ticket = Ticket::find(request()->id);

			//if is a customer check if is the owner of the tickete
			if (auth()->user()->isCustomer() && $ticket->customer_id != auth()->user()->id)
				return Msg::unauthorized();

		} else
			$rules['message.text'] = 'required';

		if (!request()->silent)
			$validData = request()->validate($rules);

		try {
			$ticket->fill(request()->all());

			if (!request()->silent && strlen(request()->message['text']) > 0) {
				$log = $ticket->log;
				array_push($log, request()->message);
				$ticket->log = $log;
			}

			$ticket->save();

			$ticket->files = FileManager::saveField(request()->get('files'), $ticket->files, 'users/' . $ticket->customer_id . '//tickets/' . $ticket->id . '/');

			$ticket->save();

			$product = $ticket->Product;
			$user = $ticket->Customer;

			broadcast(new UserNotification($ticket->customer_id, $ticket->id));

			return Msg::modelSaved($ticket, 'Ticket saved');
		} catch (\Exception $ex) {
			return Msg::error($ex->getMessage());
		}
	}

	public function delete($ids)
	{
		try {
			$ids = explode(',', $ids);


			foreach ($ids as $id) {
				$ticket = Ticket::find($id);
				$customer_id = $ticket->customer_id;

				if (auth()->user()->isAdmin() == false && auth()->user()->isSales() == false && $ticket->customer_id != auth()->user()->id)
					return Msg::unauthorized();

				FileManager::deleteDir('users/' . $ticket->customer_id . '//tickets/' . $id);
				$ticket->delete();
				broadcast(new UserNotification($customer_id, null, $ids));
			}


			return Msg::success(count($ids) > 1 ? 'Tickets Deleted' : 'Ticket Deleted');
		} catch (\Exception $ex) {
			return Msg::error($ex->getMessage());
		}

	}

	public function ticketsWorkflow($workflow)
	{

		$tickets = [];

		if ($workflow == 'customer')
			$tickets = DB::select("SELECT * FROM tickets WHERE customer_id = ? and JSON_CONTAINS(log,'{\"status\":2}')", [auth()->user()->id]);
		else
			$tickets = DB::select("SELECT * FROM tickets WHERE JSON_CONTAINS(log,'{\"status\":1}')");

		return $tickets;
	}
}
