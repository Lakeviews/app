<?php

namespace App\Data;

use App\ClientSegment;
use App\Msg;
use App\FileManager;



class Segments
{



    public function paginate()
    {

        $data = ClientSegment::with(['Segment', 'Client'])->where([
            ['contact_name', 'like', '%' . request()->search . '%'],
            ['contact_role', 'like', '%' . request()->search . '%'],
            ['description', 'like', '%' . request()->search . '%'],
            ['segment_type_id', 'like', isset(request()->segment_type_id) && request()->segment_type_id > 0 ? request()->segment_type_id : '%%'],
            ['client_id', 'like', isset(request()->client_id) && request()->client_id > 0 ? request()->client_id : '%%']
        ])->orWhere([
            ['contact_name', 'like', '%' . request()->search . '%'],
            ['contact_role', 'like', '%' . request()->search . '%'],
            ['segment_type_id', 'like', isset(request()->segment_type_id) && request()->segment_type_id > 0 ? request()->segment_type_id : '%%'],
            ['client_id', 'like', isset(request()->client_id) && request()->client_id > 0 ? request()->client_id : '%%']
        ]);



        if (isset(request()->sorts)) {
            foreach (request()->sorts as $key => $value)
                $data = $data->orderBy($key, $value);
        }

        return $data->paginate();
    }

    public function create()
    {

        $segment = new ClientSegment();
        $segment->id = -1;
        $segment->client_id = '';
        $segment->segment_type_id = '';

        $segment->contact_name = '';
        $segment->contact_role = '';
        $segment->contact_details = '';
        $segment->description = '';
        $segment->files = [];

        return $segment;
    }

    public function save()
    {

        $segment = new ClientSegment();

        $rules = [
            'client_id' => 'required|exists:clients,id',
            'segment_type_id' => 'required|exists:objects,id',
        ];

		 //validate the request
        request()->validate($rules);

        if (request()->id > 0)
            $segment = ClientSegment::find(request()->id);

        try {
            $segment->fill(request()->all());
            $segment->save();

            $segment->files = FileManager::saveField(request()->get('files'), $segment->files, 'segments/' . $segment->id . '/');
            $segment->save();
            return Msg::modelSaved($segment, 'Segment saved');

        } catch (\Exception $ex) {
            return Msg::error($ex->getMessage());
        }
    }

    public function delete($ids)
    {
        try {
            $ids = explode(',', $ids);
            ClientSegment::destroy($ids);
            return Msg::success(count($ids) > 1 ? 'Segements Deleted' : 'Segement Deleted');
        } catch (\Exception $ex) {
            return Msg::error($ex->getMessage());
        }
    }

}