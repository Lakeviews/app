<?php

namespace App\Data;

use App\AObject;
use App\Msg;


class Objects
{

    public function paginate()
    {
        $data = AObject::with('Category')->where([
            ['name', 'like', '%' . request()->search . '%'],
            ['category_id', 'like', (isset(request()->category_id) && request()->category_id > 0) ? request()->category_id: '%%']
        ])->orWhere([
            ['alias', 'like', '%' . request()->search . '%'],
            ['category_id', 'like', (isset(request()->category_id) && request()->category_id > 0) ? request()->category_id : '%%']
        ]);

        if (isset(request()->sorts)) {
            foreach (request()->sorts as $key => $value)
                $data = $data->orderBy($key, $value);
        }

        return $data->paginate();
    }

    public function create()
    {

        $object = new AObject();
        $object->id = -1;
        $object->name = '';
        $object->alias = '';
        $object->category_id = '';
        $object->active = 1;

        return $object;
    }

    public function save()
    {

        $object = new AObject();

        $rules = [
            'name' => 'required',
            'alias' => 'required',
            'active' => 'required',
            'category_id' => 'required|exists:categories,id'
        ];

		 //validate the request
        request()->validate($rules);

        if (request()->id > 0)
            $object = AObject::find(request()->id);

        try {
            $object->fill(request()->all());
            $object->save();

            return Msg::modelSaved($object, 'Object saved');
        } catch (\Exception $ex) {
            return Msg::error($ex->getMessage());
        }
    }

    public function delete($ids)
    {
        try {
            $ids = explode(',', $ids);
            AObject::destroy($ids);
            return Msg::success(count($ids) > 1 ? 'Objects Deleted' : 'Object Deleted');
        } catch (\Exception $ex) {
            return Msg::error($ex->getMessage());
        }
    }

}