<?php

namespace App\Data;

use App\Document;
use App\FileManager;
use App\Msg;
use App\Product;
use Carbon\Carbon;

class Documents
{

	public function paginate()
	{

		$product_id = strlen(request()->get('product_id')) > 0 ? request()->get('product_id') : '';

		$data = Document::with(['Customer', 'Product'])
			->where([
				['reference', 'like', '%' . request()->search . '%'],
				['product_id', 'like', '%' . $product_id . '%']
			])
			->orWhere([
				['description', 'like', '%' . request()->search . '%'],
				['product_id', 'like', '%' . $product_id . '%']
			])
			->orWhere(function ($query) use ($product_id) {

				$query->where('product_id', 'like', '%' . $product_id . '%')
					
				->whereHas('Customer', function ($subquery) {
						return $subquery->where('name', 'like', '%' . request()->search . '%');
					});

			});




		if (isset(request()->sorts)) {
			foreach (request()->sorts as $key => $value)
				$data = $data->orderBy($key, $value);
		}

		return $data->paginate();
	}

	public function create()
	{

		$document = new Document();
		$document->id = -1;
		$document->reference = '';
		$document->customer_id = '';
		$document->product_id = '';
		$document->year = Carbon::now()->year;
		$document->active = 1;
		$document->managed = 1;
		$document->closed = 0;
		$document->description = '';
		$document->long_description = '';
		$document->files = [];

		//Insurance Product
		$document->product_type = null;
		$document->name = '';
		$document->mobile = '';
		$document->phone = '';
		$document->gender = 1;
		$document->birthdate = Carbon::now()->addYears(-20)->toDateString();
		$document->email = '';
		$document->address = '';
		$document->postcode = '';
		$document->town = '';
		$document->country = '';
		//Insurance Payement
		$document->amount = null;
		$document->payment_type = null;
		$document->date_start = Carbon::now()->toDateString();
		$document->date_end = Carbon::now()->addMonths(12)->toDateString();
		$document->notification_period = 30;
		$document->marketing_period = 30;
		$document->permissions = [];

		return $document;
	}

	public function save()
	{

		$document = new Document();
		$rules = [
			'reference' => 'required',
			'year' => 'required',
			'product_id' => 'required',
			'customer_id' => 'required',
			'payment_type' => 'required',
			'amount' => 'required'
		];




		if (request()->product_type == 1) {
			$rules['name'] = 'required';
			$rules['birthdate'] = 'required';
			$rules['mobile'] = 'required';
			$rules['address'] = 'required';
			$rules['town'] = 'required';
			$rules['postcode'] = 'required';
			$rules['country'] = 'required';

		}

		request()->validate($rules);


		request()->offsetUnset('product');

		if (request()->id > 0)
			$document = Document::find(request()->id);


		try {
			$document->fill(request()->all());
			$document->save();

			$document->files = FileManager::saveField(request()->get('files'), $document->files, 'users/' . $document->customer_id . '//documents/' . $document->id . '/');
			$document->save();

			return Msg::modelSaved($document, 'Document saved');
		} catch (\Exception $ex) {
			return Msg::error($ex->getMessage());
		}
	}

	public function delete($ids)
	{
		try {
			$ids = explode(',', $ids);
			
			//delete all process directories
			foreach ($ids as $id) {
				$document = Document::find($id);
				FileManager::deleteDir('users/' . $document->customer_id . '//documents/' . $document->id);
				$document->delete();
			}

			return Msg::success(count($ids) > 1 ? 'Documents Deleted' : 'Document Deleted');
		} catch (\Exception $ex) {
			return Msg::error($ex->getMessage());
		}
	}
}
