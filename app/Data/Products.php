<?php

namespace App\Data;

use App\Document;
use App\Msg;
use App\Product;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use App\MetaProduct;

class Products
{

	public function paginate()
	{
		$data = Product::where('name', 'like', '%' . request()->search . '%')
			->orWhere('description', 'like', '%' . request()->search . '%');

		if (isset(request()->sorts)) {
			foreach (request()->sorts as $key => $value)
				$data = $data->orderBy($key, $value);
		}

		return $data->paginate();
	}

	public function create()
	{

		$product = new Product();
		$product->id = -1;
		$product->name = '';
		$product->active = 1;
		$product->description = '';
		$product->type = 1;
		$product->categories = [];
		$product->company_id = '';
		$product->available_for_ticket = 1;
		$product->meta = MetaProduct::getMetaLocales();


		return $product;
	}

	public function save()
	{

		$product = new Product();
		$rules = [
			'name' => 'required',
			'categories' => 'required',
			'type' => 'required',
			'available_for_ticket' => 'required'
		];

		 //validate the request
		request()->validate($rules);

		if (request()->id > 0)
			$product = Product::find(request()->id);

		try {
			request()->offsetUnset('company');

			$product->fill(request()->all());
			$product->save();

			return Msg::modelSaved($product, 'Product saved');
		} catch (\Exception $ex) {
			return Msg::error($ex->getMessage());
		}
	}

	public function delete($ids)
	{
		try {
			$ids = explode(',', $ids);
			Product::destroy($ids);
			return Msg::success(count($ids) > 1 ? 'Products Deleted' : 'Product Deleted');
		} catch (\Exception $ex) {
			return Msg::error($ex->getMessage());
		}
	}

	public function productsDocumentsYearMontly($year)
	{

		$productDocsTotalByMonth = new Collection();

		$productDocs = Product::with(['documents' => function ($query) use ($year) {
			$query->whereYear('date_start', '=', $year);
		}])->get();

		$productDocs->each(function ($product, $key) use ($productDocsTotalByMonth) {

			if ($product->documents->count() > 0) {
				$months = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

				foreach ($months as $index => $month) {

					foreach ($product->documents as $document) {
						if (Carbon::parse($document->date_start)->month == $index + 1)
							$months[$index] += 1;

					}
				}
				$company = isset($product->Company) ? $product->Company->name . ' | ' : '';
				$productDocsTotalByMonth->add(['name' => $company . $product->name, 'data' => $months]);
			}

		});

		return $productDocsTotalByMonth;
		//return Carbon::parse($data[0]->documents[0]->date_start)->month;
	}
}
