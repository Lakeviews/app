<?php

namespace App\Data;

use App\Mail\Recover;
use App\Msg;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class Authorization
{

	public function login()
	{

		$validData = request()->validate([
			'email' => 'required|exists:users,email',
			'password' => 'required'
		]);

	
		if (Auth::attempt($validData, true)) {
			$data = ['route' => 'dashboard'];

			if (auth()->user()->isCallCenter())
				$data = ['route' => 'admin'];

			return Msg::model(true, auth()->user(), null, $data);
		} else
			return Msg::error('Wrong Credentials');
	}

	public function recover()
	{

		$validData = request()->validate(['email' => 'required|exists:users,email']);

		try {
			$user = User::where('email', $validData['email'])->first();

			Mail::to($user->email)->send(new Recover($user));
			return Msg::success('A email has been sent to your mailbox containing the instructions to change you password! <br> Thanks');
		} catch (\Exception $ex) {
			return Msg::error('Our Server is currently down, please try again later');
		}

	}

	public function recoverByToken($token)
	{
		auth()->logout();

		$user = User::where('api_token', $token)->first();

		if (isset($user)) {
			auth()->login($user, true);
			$user->setToken();
			return redirect()->route('profile');
		} else
			return redirect()->route('home')->with("flash", "toastr['error']('Expired Token, make a new request please.')");

	}

	public function office($token)
	{
		auth()->logout();

		$user = User::where('api_token', $token)->first();

		if (isset($user)) {
			auth()->login($user, true);
			$user->setToken();
			return redirect()->route('admin');
		} else
			return redirect()->route('home')->with("flash", "toastr['error']('Expired Token, make a new request please.')");

	}


	public function logout()
	{
		auth()->logout();
		return redirect()->route('home');
	}


	public static function logoutS()
	{
		auth()->logout();
		return redirect()->route('home');
	}
}