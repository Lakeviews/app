<?php

namespace App;

use App\Company;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{

	protected $fillable = [];

	protected $guarded = ['id', 'files', 'selected', 'loading', 'files', 'permissions', 'product_type', 'status'];
	
	protected $appends = ['selected', 'loading', 'product_type', 'status'];

    protected $casts = [
        'files' => 'array',
    ];

    protected $perPage = 10;

 

    public function getStatusAttribute()
    {
        $today = Carbon::now();

        $end = Carbon::parse($this->date_end);


        $renew_max = Carbon::parse($this->date_end);
        $renew_min = Carbon::parse($this->date_end)->addDay(-$this->notification_period);
        $alert_min = Carbon::parse($this->date_end)->addDay(-$this->marketing_period);


        $end = Carbon::parse($this->date_end);
        
        if ($today < $alert_min)
            return "RUNNING";
        else if ($today >= $alert_min && $today < $renew_min)
            return "MARKETING";
        else if ($today >= $renew_min && $today <= $renew_max)
            return "RENEW";
        else if ($today > $end)
            return "EXPIRED";
    }


    public function getProductTypeAttribute(){
        if(isset($this->Product))
            return $this->Product->type;
        else
            return null;
    }

    public function getSelectedAttribute(){
        return false;
    }

    public function getLoadingAttribute(){
        return false;
    }

    public function Customer()
    {
        return $this->belongsTo(User::class, 'customer_id');
    }

    public function Product()
    {
        return $this->belongsTo(Product::class);
    }

}
