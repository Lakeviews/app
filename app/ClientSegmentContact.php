<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientSegmentContact extends Model
{
    protected $table = 'client_segments_contact';

    protected $guarded = ['id', 'selected', 'loading'];

    protected $appends = ['selected', 'loading'];

    protected $fillable = [];


    public function getSelectedAttribute()
    {
        return false;
    }

    public function getLoadingAttribute()
    {
        return false;
    }


    public function Segment()
    {
        return $this->belongsTo(ClientSegment::class, 'client_segment_id');
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function getStatus()
    {

        if ($this->status == 3 && $this->contact_recall_status == 1)
            return __('backoffice.contact_later').' | '.__('backoffice.done');

        switch ($this->status) {

            case 1:
                return __('backoffice.complete');
                break;

            case 2:
                return __('backoffice.unavailable');
                break;


            case 3:
                return __('backoffice.contact_later');
                break;

            case 4:
                return __('backoffice.successfull');
                break;



            case 5:
                return __('backoffice.unsuccessful');
                break;


        }

    }
}
