<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class AObject extends Model
{
    protected $table = "objects";

    protected $guarded = ['id', 'selected', 'loading'];

    protected $fillable = [];

    protected $appends = ['selected', 'loading'];

    public function getSelectedAttribute()
    {
        return false;
    }

    public function getLoadingAttribute()
    {
        return false;
    }

    public function Category()
    {
        return $this->belongsTo(Category::class);
    }

    public function Clients()
    {
        return $this->hasMany(Client::class, "company_type_id");
    }


    public function Segment()
    {
        return $this->belongsTo(ClientSegment::class, 'segment_type_id');
    }


    public function scopeByCategory($query, $aliases)
    {
        $categories = Category::whereIn('alias', $aliases)->get();

        return $query->whereIn('category_id', $categories->pluck('id')->toArray())->get();
    }
}
