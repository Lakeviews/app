<?php

namespace App;

use App\Message;
use App\Product;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
	protected $fillable = [];

	protected $guarded = ['id', 'selected', 'loading', 'message', 'product', 'customer', 'silent', 'files'];

	protected $appends = ['selected', 'loading', 'message', 'silent'];
	
	protected $casts = [
		'log' => 'array',
		'files' => 'array',
	];

	protected $perPage = 10;


	public function getSilentAttribute(){
		return false;
	}

	public function getSelectedAttribute(){
		return false;
	}

	public function getLoadingAttribute(){
		return false;
	}

	public function getMessageAttribute(){
		return Message::create();
	}


	public function Customer(){		
		return $this->belongsTo(User::class);
	}


	public function Product(){

		return $this->belongsTo(Product::class);
	}

	public function scopeChanged($query)
	{
		return $query->where('status', 1)->get();
	}


}
