<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientSegment extends Model
{
    protected $guarded = ['id', 'selected', 'loading', 'files'];

    protected $appends = ['selected', 'loading'];

    protected $fillable = [];

    protected $casts = ['files' => 'array'];

    public static function Sectors()
    {
        return explode(',', env('CLIENT_SECTORS'));    
    }

    public static function Categories()
    {
        return explode(',', env('CALLCENTER_CATEGORIES'));
    }

    public function getSelectedAttribute()
    {
        return false;
    }

    public function getLoadingAttribute()
    {
        return false;
    }

    public function Client()
    {
        return $this->belongsTo(Client::class);
    }

    public function SegmentContact()
    {
        return $this->belongsTo(ClientSegmentContact::class);
    }

    public function Segment()
    {
        return $this->belongsTo(AObject::class, 'segment_type_id');
    }
}
