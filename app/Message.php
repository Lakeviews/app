<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public static function create()
    {
    	$msg = new Message();
    	$msg->type = null;
    	$msg->text = '';
    	$msg->date = null;
        $msg->status = 1;
    	$msg->posted_by_id = '';
    	$msg->posted_by_name = '';

    	return $msg;
    }
}
