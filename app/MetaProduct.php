<?php

namespace App;

use Illuminate\Support\Collection;



class MetaProduct
{
    public $name;
    public $description;
    public $lang;
    public $default_lang;

    public static function getMetaLocales()
    {
        $metaData = new Collection();

        foreach (config('app.locales') as $locale) {
          
            $meta = new MetaProduct();
            $meta->name = '';
            $meta->description = '';
            $meta->lang = $locale;

            if (env('LNG') == $locale)
                $meta->default_lang = true;

            $metaData->push($meta);
        }

        $metaData->sortByDesc('default_lang');

        $metaDataSorted = new Collection();


        foreach ($metaData->sortByDesc('default_lang') as  $locale) {
            $metaDataSorted->push($locale);
        }

        return $metaDataSorted;
    }
}