<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
	protected $table = 'companies';

	protected $fillable = [];

	protected $guarded = ['id', 'selected', 'loading'];

	protected $appends = ['selected', 'loading'];

	protected $perPage = 10;
	
	public function getSelectedAttribute(){
		return false;
	}

	public function getLoadingAttribute(){
		return false;
	}
	

	public function Products(){
		return $this->hasMany(Product::class);
	}

	public function scopeRelocation($query)
	{
		return $query->where('type',2)->where('active', 1)->get();
	}
}
