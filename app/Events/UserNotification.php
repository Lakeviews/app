<?php

namespace App\Events;

use App\Events\BONotification;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserNotification implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    public $notifications = [];

    public function __construct($userId, $tickedChanged = null, $ticketsDeleted = [])
    {   
    
        $notifications = ['userId' => $userId, 
                        'tickets' => [
                            'tickedChanged' => $tickedChanged, 
                            'ticketsDeleted' => $ticketsDeleted
                            ]
                        ];



        $this->notifications = $notifications;

         broadcast(new BONotification($tickedChanged, $ticketsDeleted));
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('user-notification.'.$this->notifications['userId']);
    }
}
