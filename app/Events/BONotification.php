<?php
namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Facades\Mail;
use App\Mail\TicketChanged;
use App\User;

class BONotification implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    public $notifications;

    public function __construct($tickedChanged = null, $ticketsDeleted = [])
    {
        $notifications = ['tickets' => [
            'tickedChanged' => $tickedChanged,
            'ticketsDeleted' => $ticketsDeleted
        ]];

        $this->notifications = $notifications;

        foreach (User::BO() as $user) {
            Mail::to($user->email)->send(new TicketChanged($user));
        };
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('bo-notication');
    }
}
