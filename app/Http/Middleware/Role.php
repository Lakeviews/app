<?php

namespace App\Http\Middleware;

use App\Msg;
use Closure;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $roles, int $id = 0)
    {
        $roles = explode('|', $roles);
      
//  echo json_encode($roles);
// // die();

        if (auth()->check() && auth()->user()->hasRoles($roles)) {

          
            if ($id > 0) {

                if (auth()->user()->id == $id)
                    return $next($request);
                else {

                    if ($request->ajax())
                        return Msg::unauthorized();
                    else
                        return redirect()->route('login');
                }
            } else

                return $next($request);

        } else {
            if ($request->ajax())
                return Msg::unauthorized();
            else
                return redirect()->route('login');

        }

    }
}
