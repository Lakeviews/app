<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use App\Data\Clients;
use App\AObject;
use App\ClientSegment;
use Illuminate\Support\Facades\Route;

class ClientsController extends Controller
{

    protected $clients;

    public function __construct(Clients $clients)
    {
        $this->clients = $clients;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->clients->paginate();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->clients->create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->clients->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      
        return view('clients.index')
        ->with('segments', AObject::ByCategory(ClientSegment::Categories()))
        ->with('clients', Client::all())
        ->with('CompanyTypes',  AObject::ByCategory(ClientSegment::Sectors()));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        return $client;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy($ids)
    {
        return $this->clients->delete($ids);
    }
}
