<?php

namespace App\Http\Controllers;

use App\Data\Tickets;
use App\Product;
use App\Ticket;
use App\User;
use Illuminate\Http\Request;

class TicketsController extends Controller
{
    protected $tickets;

    public function __construct(Tickets $tickets){

        $this->middleware(['auth', 'role:admin|sales|callCenter|customers']);
        $this->tickets = $tickets;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->tickets->paginate();
    }


  

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {    
        return $this->tickets->create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->tickets->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('tickets.index')->with('products', Product::Active( ))->with('customers', User::all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {   $product = $ticket->Product;
        $user = $ticket->Customer;
        
        return $ticket;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy($ids)
    {
        return $this->tickets->delete($ids);
    }

    public function ticketsWorkflow($workflow){

        return $this->tickets->ticketsWorkflow($workflow);

    }
}
