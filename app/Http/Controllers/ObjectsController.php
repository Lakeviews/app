<?php

namespace App\Http\Controllers;

use App\AObject;
use Illuminate\Http\Request;
use App\Data\Objects;
use App\Category;

class ObjectsController extends Controller
{

    protected $objects;

    public function __construct(Objects $objects)
    {
        $this->objects = $objects;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->objects->paginate();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->objects->create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->objects->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AObject  $object
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('objects.index')->with('categories', Category::Entities([2]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AObject  $object
     * @return \Illuminate\Http\Response
     */
    public function edit(AObject $object)
    {
        return $object;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AObject  $object
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AObject $object)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AObject  $object
     * @return \Illuminate\Http\Response
     */
    public function destroy($ids)
    {
        return $this->objects->delete($ids);
    }
}
