<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Data\Contacts;
use App\ClientSegmentContact;
use PDF;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class SegmentContactsController extends Controller
{

    protected $contacts;

    public function __construct(Contacts $contacts)
    {

        $this->contacts = $contacts;
    }



    public function pdf($id)
    {
        Carbon::setLocale('fr');

        $data = $this->contacts->pdf($id);
        // return view('segments.report', compact('data'));

        $pdf = PDF::loadView('segments.report', compact('data'));

        return $pdf->download('report-' . Carbon::now()->toDateTimeString() . '.pdf');


    }


    public function excel($id)
    {
        Carbon::setLocale('fr');



        $contacts = $this->contacts->pdf($id);
        // return view('segments.report', compact('data'));
        $name =  __('backoffice.reports_calls').' - '.Carbon::now()->toDateTimeString();

        $spreadsheet = new Spreadsheet();
        // Set document properties
        $spreadsheet->getProperties()
            ->setTitle($name);
           


        // Add some data
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', __('backoffice.status'))
            ->setCellValue('B1', __('backoffice.info_contact'))
            ->setCellValue('C1', __('backoffice.person_name'))
            ->setCellValue('D1', __('backoffice.person_role'))
            ->setCellValue('E1', __('backoffice.postcode'))
            ->setCellValue('F1', __('backoffice.person_contact'))
            ->setCellValue('G1','Later '. __('backoffice.name'))
            ->setCellValue('H1', 'Later '.__('backoffice.role'))
            ->setCellValue('I1', 'Later '.__('backoffice.details'))
            ->setCellValue('J1', 'Later '.__('backoffice.date'))
            ->setCellValue('K1', __('backoffice.comments'));


            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(30);
            $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(30);
            $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(30);
            $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(30);
            $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(30);
            $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(30);
            $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(30);
            $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(30);
            $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(30);
            $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(30);
            $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(30);
            $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(200);

        foreach ($contacts['contacts'] as $index => $contact) {
          
         
            $index = $index +2;
       


            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A' . $index, $contact->getStatus())
                ->setCellValue('B' . $index, $contact->name)
                ->setCellValue('C' . $index, $contact->contact_name)
                ->setCellValue('D' . $index, $contact->contact_role)
                ->setCellValue('E' . $index, $contact->contact_postcode)
                ->setCellValue('F' . $index, $contact->contact_details)
                ->setCellValue('G' . $index, $contact->contact_later_name)
                ->setCellValue('H' . $index, $contact->contact_later_role)
                ->setCellValue('I' . $index, $contact->contact_later_details)
                ->setCellValue('J' . $index, Carbon::parse($contact->contact_later_date)->formatLocalized('%d %B %Y - %H:%M'))
                ->setCellValue('K' . $index,$contact->comments);
     

        }
       



        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$name.'.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->contacts->paginate();
    }

    public function recall(ClientSegmentContact $contact, $value)
    {
        $contact->contact_recall_status =$value;
        if ($value == 1)
            $contact->status = 3;
            
        $contact->save();

        return $contact;
    }


    public function calls()
    {
        return $this->contacts->calls();
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->contacts->create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->contacts->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client_Contacts  $client_Contacts
     * @return \Illuminate\Http\Response
     */
    public function show(ClientSegmentContact $client_Contacts)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client_Contacts  $client_Contacts
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return ClientSegmentContact::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ClientSegmentContact  $client_Contacts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClientSegmentContact $client_Contacts)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client_Contacts  $client_Contacts
     * @return \Illuminate\Http\Response
     */
    public function destroy($ids)
    {
        return $this->contacts->delete($ids);
    }
}
