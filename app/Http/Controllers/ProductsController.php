<?php

namespace App\Http\Controllers;

use App\Company;
use App\Data\Products;
use App\Product;
use Illuminate\Http\Request;
use App\MetaProduct;
use App\Category;

class ProductsController extends Controller
{
    protected $products;

    public function __construct(Products $products)
    {

        $this->middleware(['auth', 'role:admin|sales|callCenter']);

        $this->products = $products;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->products->paginate();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->products->create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->products->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($product)
    {

        return view('products.index')->with('companies', Company::all())->with('categories', Category::Entities([1]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        if ($product->meta == null)
            $product->meta = MetaProduct::getMetaLocales();
        return $product;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($ids)
    {
        return $this->products->delete($ids);
    }

    //Statistics methods for ploting
    public function productsDocumentsYearMontly($year)
    {

        return $this->products->productsDocumentsYearMontly($year);
    }
}
