<?php

namespace App\Http\Controllers;

use App\Company;
use App\Data\Users;
use App\Msg;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;


class UsersController extends Controller
{
    protected $users;

    public function __construct(Users $users)
    {

        $this->middleware(['auth', 'role:admin|sales|callCenter'])->except([
            'insurancesView', 'dashboardView',
            'dashboardViewTicket', 'relocationView', 'storageView', 'ticketsView', 'profileView','insurances','store'
        ]);

        $this->middleware(['auth', 'role:admin|customers'])->only([
            'insurancesView', 'dashboardView',
            'dashboardViewTicket', 'relocationView', 'storageView', 'ticketsView','insurances','store'
        ]);


      
       


        $this->middleware(['auth'])->only(['profileView']);

        $this->users = $users;


    }

    public function sendMessage()
    {
        return $this->users->sendMessage();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {

        return $this->users->paginate();
    }

    public function dashboardView()
    {
        $ticketId = 0;
        if (isset(request()->id))
            $ticketId = request()->id;

        return view('users.dashboard')->with('ticketId', $ticketId)->with('products', Product::Active());
    }

    public function seeTicket($id)
    {

        return view('users.dashboard');
    }

    public function dashboardViewTicket($id)
    {

        return redirect()->route('dashboard', ['id' => $id]);
    }

    public function profileView()
    {
        
        return view('users.profile');
    }

    public function insurancesView()
    {
        return view('users.insurances');
    }

    public function insurances($year)
    {
        return $this->users->insurances($year);
    }

    public function relocationView()
    {
        return view('users.relocation')->with('relCompanies', Company::Relocation());
    }

    public function relocation()
    {
        return view('users.relocation');
    }

    public function storageView()
    {
        return view('users.storage');
    }

    public function storage()
    {
        return view('users.storage');
    }

    public function ticketsView()
    {
        return view('users.tickets')->with('products', Product::Active());
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->users->create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (auth()->user()->isCustomer() && request()->id != auth()->user()->id)
            return Msg::unauthorized();

        return $this->users->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($user)
    {
        return view('users.index')->with('relCompanies', Company::Relocation());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {

        $user->password = '';
        return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($ids)
    {
        return $this->users->delete($ids);
    }
}
