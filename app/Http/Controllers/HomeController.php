<?php

namespace App\Http\Controllers;

use App\FileManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Lang;
use App\Category;



class HomeController extends Controller
{

	public function __construct()
	{	
	
		$this->middleware(['auth', 'role:admin|customer|callCenter|sales'])->except(['index', 'language','changeLocale', 's3',]);
	}

	public function changeLocale($locale)
	{

		session()->put('locale', $locale);
		return redirect()->back();
	}

	public function index()
	{
		if (!session()->has('locale'))
			session()->put('locale', app()->getLocale());
			
			
		return view('layout');
	}


	public function s3($entity, $id, int $index)
	{

		return FileManager::download($entity, $id, $index);
	}

	public function admin()
	{

		if (!session()->has('locale'))
			session()->put('locale', app()->getLocale());

		return view('admin.layout');
	}

	public function dashboard()
	{
		return view('admin.dashboard');
	}

	public function language()
    {
        return require(app()->langPath() . '/' . app()->getLocale() . '/client.php');
	}
	
	public function dictionary()
    {
        return ['backoffice' => require(app()->langPath() . '/' . app()->getLocale() . '/backoffice.php')];
	}
	
}
