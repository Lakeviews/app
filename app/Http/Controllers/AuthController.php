<?php

namespace App\Http\Controllers;

use App\Data\Authorization;
use Illuminate\Http\Request;

class AuthController extends Controller
{
	protected $auth;

	public function __construct(Authorization $auth)
	{
		
		$this->middleware(['guest'])->only(['loginView', 'login']);
		$this->middleware(['auth'])->only(['logout']);

		$this->auth = $auth;
	}

	public function broadcast()
	{
		if (auth()->check())
			return json_encode(true);
	}

	public function remember()
	{
		return redirect()->route(request()->route);
	}

	public function loginView()
	{

		return view('auth.login');
	}

	public function login()
	{
		return $this->auth->login();
	}

	public function recover()
	{
		return $this->auth->recover();
	}

	public function recoverByToken()
	{
		return $this->auth->recoverByToken();
	}

	public function office($token)
	{
		return $this->auth->office($token);
	}

	public function logout()
	{
		return $this->auth->logout();
	}
}
