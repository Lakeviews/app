<?php

namespace App\Http\Controllers;

use App\ClientSegment;
use Illuminate\Http\Request;
use App\Client;
use App\Data\Segments;
use App\User;

class SegmentsController extends Controller
{
    protected $segments;

    public function __construct(Segments $segments)
    {

        $this->segments = $segments;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->segments->paginate();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->segments->create();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->segments->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ClientSegment  $clientSegment
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('segments.index')->with('clients', Client::Active())
            ->with('segments', \App\AObject::ByCategory(ClientSegment::Categories()))->with('users', User::CallCenter());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ClientSegment  $clientSegment
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        return ClientSegment::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ClientSegment  $clientSegment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClientSegment $clientSegment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ClientSegment  $clientSegment
     * @return \Illuminate\Http\Response
     */
    public function destroy($ids)
    {
        return $this->segments->delete($ids);
    }
}
