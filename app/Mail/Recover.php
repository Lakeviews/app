<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Recover extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {   
      
        $this->user = $user;

        $this->subject("LAKEVIEWS – NEW MESSAGE - TICKET");
        $this->from(env('MAIL_FROM'), 'LAKEVIEWS');
        $this->subject = 'LAKEVIEWS WEBAPP - PASSWORD RECOVER';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.recover')->with('user', $this->user);
    }
}
