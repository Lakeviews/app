<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailMessage extends Mailable
{
    use Queueable, SerializesModels;
    protected $message;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($message, $admin)
    {   
        $this->message = $message;
        $this->from(env('MAIL_FROM'), 'LAKEVIEWS');
        
        if($admin)
        {
            if($message['title'] == 'CONTACT REQUEST')
              $this->subject = 'LAKEVIEWS WEBAPP - Hi, '.$message['name'].' ask a '.$message['title'];
          else
            $this->subject = 'LAKEVIEWS WEBAPP - Hi, '.$message['name'].' says '.$message['title'];
    }
    else
        $this->subject = 'LAKEVIEWS WEBAPP - Hi, '.$message['name'].' here is copy of you message';
}

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.message')->with('message', $this->message);
    }
}
