<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use DB;
use Event;
use Log;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (env('APP_ENV') === 'local') {
            
            DB::connection()->enableQueryLog();

           
            \Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {
                
                   
                    // Log::info($query->sql);

                    Log::info($query->bindings);
                
            });
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
