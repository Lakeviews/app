<?php

namespace App;

class Msg 
{
	public static function modelSaved($model, $msg ='Saved with Success'){
		return  ['result' => true, 'model' => $model, 'msg' => $msg];
	}

	public static function model($result, $model, $msg ='Saved with Success', $data =[]){
		return  ['result' => $result, 'model' => $model, 'msg' => $msg, 'data' => $data];
	}

	public static function error($error)
	{
		return ['result' => false, 'msg'=> $error];
	}


	public static function success($msg)
	{
		return ['result' => true, 'msg'=> $msg];
	}

	public static function unauthorized(){
		return ['result' => false, 'msg' => 'Unauthorized'];
	}
}