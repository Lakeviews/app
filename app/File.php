<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
	

	public static function parse($file)
	{

		$newFile = new File();
		$newFile->id = $file['id'];
		$newFile->name = $file['name'];
		$newFile->extension = $file['extension'];
		$newFile->data = $file['data'];
		$newFile->url = $file['url'];
		$newFile->type = $file['type'];
		if(isset($file['description']))
			$newFile->description = $file['description'];
		
		return $newFile;
	}

	public function saveS3($path){

		if(strlen($this->data) > 0)
		{

			try{
			list($type, $this->data) = explode(';', $this->data);
			list(, $this->data) = explode(',', $this->data);
			$data = base64_decode($this->data);

			Storage::disk('s3')->makeDirectory($path);
			$server_path = $path.$this->name;
			Storage::disk('s3')->put($server_path, $data);
			$this->data = null;
			$this->url = $server_path;
			}
			catch(\Exception $ex)
			{
				dd(ex);
			}
		}
	}


}