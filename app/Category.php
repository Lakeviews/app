<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Category extends Model
{
    protected $guarded = ['id', 'selected', 'loading'];

    protected $fillable = [];

    protected $appends = ['selected', 'loading'];

    protected $casts =[
        'entities' => 'array' 
    ];

    public function getSelectedAttribute()
    {
        return false;
    }

    public function getLoadingAttribute()
    {
        return false;
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1)->get();
    }

    public function scopeEntities($query, $values)
    {

        $values =  json_encode($values);
        $sql = "active =1 and  JSON_CONTAINS(entities,'{$values}')";

        return $query->whereRaw($sql)->get();
    }

    public function subCategories()
    {
        return $this->hasMany(SubCategory::class , "category_id", "id");

    }

    public static function aliases()
    {
        $aliases = env('CALLCENTER_CATEGORIES') . ',' . env('CLIENT_SECTORS'). ',' . env('ENTITIES');

        $data = explode(',', $aliases);
    
        return $data;
    }

}
