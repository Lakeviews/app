<?php

namespace App;

use App\Document;
use App\File;
use App\Ticket;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;
use App\Data\Segments;
use App\Data\Authorization;

class FileManager
{

	public static function saveField($files, $field, $path)
	{

		//return a collection of files
		$filesField = FileManager::parseFiles($field);

		foreach ($filesField as $index => $file) {
			//find if the file still exists in the uploaded file list
			$foundFiles = collect($files)->where('name', $file->name);
			//if not in file list we can
			if ($foundFiles->count() == 0)
				FileManager::delete($file->url);

		}
		
		//create a new collection to store the uploaded files
		$newFiles = new Collection();
		//looop thougth the files param and add to the field collection
		foreach ($files as $index => $file) {
			$file['id'] = $index;
			$newFile = File::parse($file);
			$newFile->saveS3($path);
			$newFiles->add($newFile);
		}

		return $newFiles;
	}

	public static function parseFiles($field)
	{

		if (!is_array($field))
			$field = [];

		$filesField = new Collection();
		foreach ($field as $file)
			$filesField->add(File::parse($file));

		return $filesField;
	}

	public static function delete($url)
	{

		$exists = Storage::disk('s3')->exists($url);
		if ($exists)
			Storage::disk('s3')->delete($url);

	}

	public static function deleteDir($directory)
	{
		Storage::disk('s3')->deleteDirectory($directory);
	}

	public static function download($entity, $id, $index)
	{
		$permission = true;

		try {
			switch ($entity) {
				case 'documents':


					$document = Document::find($id);


					if (auth()->user()->id == $document->customer_id || auth()->user()->isBackOffice() || auth()->user()->isCallCenter())
						$file = collect($document->files)->where('id', $index)->first();
					else
						$permission == false;


		
					break;


				case 'users':

					$user = User::find($id);

				
					if (auth()->user()->id == $user->id || auth()->user()->isBackOffice() || auth()->user()->isCallCenter())
						$file = collect($user->files)->where('id', $index)->first();
					else
						$permission = false;


				

					break;


				case 'relocation':

					$user = User::find($id);

					
					if (auth()->user()->id == $user->id || auth()->user()->isBackOffice() || auth()->user()->isCallCenter())
						$file = collect($user->relocation_files)->where('id', $index)->first();
					else
						$permission == false;


				

					break;

				case 'tickets':

					$ticket = Ticket::find($id);


					if (auth()->user()->id == $ticket->customer_id || auth()->user()->isBackOffice() || auth()->user()->isCallCenter())
						$file = collect($ticket->files)->where('id', $index)->first();
					else
						$permission == false;

					break;


			}
		} catch (\Exception $ex) {
			return Authorization::logoutS();
		}

		if ($permission) {

			$fileContent = Storage::disk('s3')->get($file['url']);
			$mimetype = Storage::disk('s3')->mimeType($file['url']);
			$size = Storage::disk('s3')->size($file['url']);

			$response = response($fileContent, 200, [
				'Content-Type' => $mimetype,
				'Content-Length' => $size,
				'Content-Description' => 'File Transfer',
				'Content-Disposition' => "attachment; filename={$file['name']}",
				'Content-Transfer-Encoding' => 'binary',
			]);

			return $response;

		} else
			return redirect()->route('login');

	}
}	


