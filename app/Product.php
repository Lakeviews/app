<?php

namespace App;

use App\Data\Documents;
use App\Ticket;
use Illuminate\Database\Eloquent\Model;
use function GuzzleHttp\json_decode;
use Illuminate\Foundation\Auth\User;

class Product extends Model
{
	protected $fillable = [];

	protected $guarded = ['id', 'selected', 'loading', 'product_type', 'full_name', 'clientFn'];

	protected $appends = ['selected', 'loading', 'product_type', 'full_name', 'clientFn'];

	protected $perPage = 10;

	protected $casts = [
		'meta' => 'array',
		'categories' => 'array'
	];

	public function getClientFnAttribute(){

		return "documentProduct({$this->id})";
	}

	public function getMetattribute()
	{
		return json_decode($this->meta);
	}

	public function getSelectedAttribute()
	{
		return false;
	}

	public function getFullNameAttribute()
	{

		if ($this->company_id != null && $this->company_id > 0)
			return $this->company->name . ' | ' . $this->name;
		else
			return $this->name;
	}

	public function getLoadingAttribute()
	{
		return false;
	}
	public function getProductTypeAttribute()
	{
		return null;
	}



	public function Company()
	{
		return $this->belongsTo(Company::class);
	}

	public function Documents()
	{
		return $this->hasMany(Document::class);
	}

	public function Tickets()
	{
		return $this->hasMany(Ticket::class);
	}

	public function scopeActive($query)
	{
		return $query->where('active', 1)->get();
	}

	public function scopegetAll($query)
	{
		return $query->where('name', '!=', 'GENERIC TICKET')->get();
	}

	public function scopewithDocs($query)
	{
		return $query->has('Documents', '>', 0)->where('name', '!=', 'GENERIC TICKET')->get();
	}

}
