<?php

use Illuminate\Database\Seeder;
use App\Category;
use App\SubCategory;


class CategoriesSeeder extends Seeder
{

    public function run()
    {
        
        //Add Product without company
        $category = new Category();
        $category->name = 'Insurances';
        $category->description = 'All Insurances';
        $category->alias = 'insurances';
        $entities = [1];
        $category->entities = $entities;
        $category->active   = 1;
        $category->save();
     
        //add a subcategory

        $subcategory = new SubCategory();
        $subcategory->name = "Health Insurance";
        $subcategory->alias = "health-insurance";
        $subcategory->entities = $entities;
        $subcategory->active = 1;

        //save into category

        $category->subCategories()->save($subcategory);
       

    }

}
