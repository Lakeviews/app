<?php

use Illuminate\Database\Seeder;
use App\Setting;
use App\Company;
use App\Product;
use App\User;
use App\Category;

class InitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public static function run()
    {
        //Set up the Settings
        $settings = new Setting();
        $settings->name= 'Lakeviews, SA';
        $settings->email = 'info@lakeviews.ch';
        $settings->language= 'eng';
        $settings->save();

        //Set up the companies

        //Insurance Helsana Company
        $helsana = new Company();
        $helsana->name = 'Helsana';
        //type for insurance
        $helsana->type = 1;
        $helsana->active = 1;
        $helsana->email = 'sales.helsana@helsana.ch';
        $helsana->description = 'Helsana, Insurance Company';
        $helsana->phone = '0844 80 81 82';
        $helsana->address = 'Zürichstrasse, 130';
        $helsana->town = 'Dübendorf';
        $helsana->postcode = '8600';
        $helsana->country = 'Switzerland';
        
        $helsana->save();
        
        



        //Relocation Company
        $silverline = new Company();
        $silverline->name = 'Silverline Relocations';
        //type for company
        $silverline->type = 2;
        $silverline->active = 1;
        $silverline->description = 'Relocation Services Geneva';
        $silverline->email = 'info@silverline-relocations.com';
        $silverline->phone = '+41 22 349 17 30';
        $silverline->address = 'Eugène Marziano 15, Les Acacias';
        $silverline->town = 'Geneva';
        $silverline->postcode = '1227';
        $silverline->country = 'Switzerland';
        //geo
        $silverline->latitude = '46.190427';
        $silverline->longitude = '6.132237';

        $silverline->save();

        //generic company
        $generic = new Company();
        $generic->name = 'SIG Geneva';
        //type for company
        $generic->type = 3;
        $generic->active = 1;
        $generic->email = 'sig.geneva@sales.ch';
        $generic->description = 'Electricity and Water - Geneva';
        $generic->phone = '0844 800 808';
        $generic->address = 'Chemin Château-Bloch 2';
        $generic->town = 'Geneva';
        $generic->postcode = '1201';
        $generic->country = 'Switzerland';
        $generic->save();


    

        //Create Users

        //Admin
        $admin = new User();
        $admin->name = 'Thomas Spycher';
        $admin->email = 'info@lakeviews.ch';
        $admin->password = bcrypt('soltero65');
        $admin->active = 1;
        $admin->roles = [1];
        $admin->gender = 1;
        $admin->relocation = 0;
        $admin->relocation_files = [];
        $admin->files =[];
        $admin->save();
        $admin->setToken();


        //Sales Person
        $admin = new User();
        $admin->name = 'Joao';
        $admin->email = 'joao@lakeviews.ch';
        $admin->password = bcrypt('joao');
        $admin->active = 1;
        $admin->roles = [2];
        $admin->gender = 1;
        $admin->relocation = 0;
        $admin->relocation_files = [];
        $admin->files =[];
        $admin->save();
        $admin->setToken();

        //Admin
        $patrick = new User();
        $patrick->name = 'Patrick Reis';
        $patrick->email = 'pcr2043@gmail.com';
        $patrick->password = bcrypt('secret');
        $patrick->active = 1;
        $patrick->gender = 1;
        $patrick->roles = [1];
        $patrick->relocation = 0;
        $patrick->relocation_files = [];
        $patrick->files =[];
        $patrick->save();
        $patrick->setToken();

        //Customer
        $caroline = new User();
        $caroline->name = 'Caroline Castillon';
        $caroline->email = 'caroline.castillon@lakeviews.ch';
        $caroline->password = bcrypt('caroline');
        $caroline->active = 1;
        $caroline->gender = 1;
        $caroline->roles = [3];
        $caroline->relocation = 0;
        $caroline->relocation_files = [];
        $caroline->files =[];
        $caroline->save();
        $caroline->setToken();
    }
}
