<?php

use App\Company;
use App\Product;
use App\Setting;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    protected static $instance;


    public static function Instance()
    {

       
        if (!isset(self::$instance))
            self::$instance = new DatabaseSeeder();

        return self::$instance;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call(InitSeeder::class);
        $this->call(CategoriesSeeder::class);
    }
}
