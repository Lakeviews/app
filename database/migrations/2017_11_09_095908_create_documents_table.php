<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {

            $table->increments('id');

            /*GENERIC INFO*/
            $table->string('reference');
            $table->integer('customer_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->string('product_name');
            $table->string('year');
            $table->tinyInteger('active')->nullable();
            $table->tinyInteger('managed')->nullable();
            $table->tinyInteger('closed')->nullable();
            $table->text('description')->nullable();
            $table->longText('long_description')->nullable();
            $table->text('files')->nullable();
            
            //INSURACE CUSTOMER INFO
            $table->string('name')->nullable();
            $table->tinyInteger('gender')->nullable();
            $table->date('birthdate')->nullable();
            $table->string('mobile')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->string('postcode')->nullable();
            $table->string('town')->nullable();
            $table->string('country')->nullable();

            //INSURANCE PRODUCT
            $table->decimal('amount', 8, 2);
            $table->string('payment_type');
            $table->date('date_start');
            $table->date('date_end');
            $table->integer('notification_period');
            $table->integer('marketing_period');

            //PERMITIONS
            $table->string('permissions')->nullable();


            //FOREIGN KEYS
            $table->foreign('customer_id', 'ref_customer')->references('id')->on('users')->onDelete('cascade');

            $table->foreign('product_id', 'ref_product')->references('id')->on('products')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
