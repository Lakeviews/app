<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {

            $table->increments('id');

            //polymorphic relation can be client or segment
            $table->integer('client_id')->unsigned();

            //email, message, boite au lettre
            $table->tinyInteger('contact_id');
            $table->tinyInteger('contact_type');
            
            //who made the call
            $table->integer('user_id')->unsigned();

            //status of the call
            $table->tinyInteger('status');
            // $table->string('name')->nullable();
            // $table->string('contact_name')->nullable();
            // $table->string('contact_details')->nullable();
            // $table->string('contact_role')->nullable();
            // $table->string('contact_postcode')->nullable();
            // //not replied | not availale
            // //contact later
            // //not interessed
            // //interessed
           
            // $table->string('contact_later_name')->nullable();
            // $table->string('contact_later_role')->nullable();
            // $table->string('contact_later_details')->nullable();
            // $table->dateTime('contact_later_date')->nullable();
            // $table->tinyInteger('contact_recall_status')->nullable();
           
            $table->text('comments')->nullable();
            $table->text('meta')->nullable();
            $table->text('files');
            $table->timestamps();

            // $table->foreign('client_segment_id', 'ref_client_seg')->references('id')->on('client_segments')->onDelete('cascade');
         

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_segments_contact');
    }
}
