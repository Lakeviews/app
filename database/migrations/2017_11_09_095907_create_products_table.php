<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {


            $table->increments('id');
            $table->string('name');
            $table->string('description')->nullable();
            // TYPE CAN BE: INSURANCE, RELOCATION, RENT ...
            $table->tinyInteger('type');
            $table->tinyInteger('active');
           
            $table->tinyInteger('available_for_ticket');
           
            //category
            $table->integer('category_id')->unsigned();
            $table->integer('sub_category_id')->unsigned();

            //phone numbers overriding
            $table->string('urgent_number')->nullable();
            $table->string('urgent_number_foreign')->nullable();
            $table->text('meta')->nullable();
            
            $table->foreign('category_id', 'ref_category_prod')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('sub_category_id', 'ref_sub_category_prod')->references('id')->on('sub_categories')->onDelete('cascade');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
