<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            /*USER INFO*/
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->string('api_token')->nullable();
            $table->rememberToken();
            $table->tinyInteger('active');

            //Permission Type 'admin, sales, customer'
            $table->string('roles')->nullable();
            
            /*USER INFO DETAILS*/
            $table->date('birthdate')->nullable();
            $table->tinyInteger('gender')->nullable();
            $table->string('mobile')->nullable();
            $table->string('phone')->nullable();
            $table->string('address')->nullable();
            $table->string('town')->nullable();
            $table->string('postcode')->nullable();
            $table->string('country')->nullable();
            $table->string('notes', 300)->nullable();

            /*RELOCATION INFO*/
            $table->tinyInteger('relocation')->nullable();
            $table->integer('relocation_company_id')->unsigned()->nullable();
            $table->text('relocation_comments')->nullable();
            $table->text('relocation_files')->nullable();
            

            /* PROFILE INFO */
            $table->text('profile')->nullable();
            
            $table->text('files')->nullable();
            //FOREIGN KEYS
            $table->foreign('relocation_company_id', 'ref_relocation_company')->references('id')->on('companies')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
