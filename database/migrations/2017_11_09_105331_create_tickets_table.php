<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('customer_id')->unsigned();
            $table->integer('product_id')->unsigned();

            //INFO
            $table->text('log');
            //1 - Open
            //1 - Submited
            //2 - Replied
            //3 - Closed

            $table->text('files')->nullable();
            $table->tinyInteger('status');
            $table->timestamps();

            //FOREIGN KEYS
            $table->foreign('customer_id', 'ref_ticket_customer')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('product_id', 'ref_ticket_product')->references('id')->on('products')->onDelete('cascade');


        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
