<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientSegmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_segments', function(Blueprint $table){

            $table->increments('id');
           
            $table->integer('client_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->integer('sub_category_id')->unsigned();
           
            $table->string('contact_name')->nullable();
            $table->string('contact_details')->nullable();
            $table->string('contact_role')->nullable();

            $table->text('description')->nullable();
            $table->text('files')->nullable();

            $table->timestamps();
            
            $table->foreign('client_id', 'ref_client')->references('id')->on('clients')->onDelete('cascade');
          
            $table->foreign('category_id', 'ref_category_seg')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('sub_category_id', 'ref_sub_category_seg')->references('id')->on('sub_categories')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_segments');
    }
}
