<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name');
            $table->tinyInteger('type');
           
            $table->integer('category_id')->unsigned();
            $table->integer('sub_category_id')->unsigned();
           
            $table->tinyInteger('active');
            $table->text('description')->nullable();
            $table->string('contact_name')->nullable();
            $table->string('contact_details')->nullable();
            $table->string('contact_role')->nullable();
            $table->string('contact_number')->nullable();

            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->string('town')->nullable();
            $table->string('postcode')->nullable();
            $table->string('country')->nullable();
            $table->string('phone')->nullable();
            $table->string('phone_2')->nullable();
            $table->string('fax')->nullable();
            $table->string('mobile')->nullable();
            $table->text('files')->nullable();


               /*GEO*/
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();


            $table->timestamps();

            $table->foreign('category_id', 'ref_category_client')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('sub_category_id', 'ref_sub_category_client')->references('id')->on('sub_categories')->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
