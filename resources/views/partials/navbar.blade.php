<nav class="navbar navbar-expand-md navbar-light">
	<div class="navbar-brand">
		<div class="brand">
			<img src="images/logo.png" alt="logo lakeviews">
			@if(count(config('app.locales')) > 1)
			<div class="btn-group locales" role="group" aria-label="Languages">
				@foreach(config('app.locales') as $locale)
				<a href="{{ url('language/'.$locale) }}" role="button" class="btn btn-primary btn-sm {{ session()->get('locale') == $locale ? 'active' : '' }}">{{ $locale }}</a> @endForeach
			</div>
			@endif 
		</div>
	</div>
	@if(auth()->check())
	<a @click="openNotifications()" class="nav-link comments-mobile" data-toggle="collapse" data-target="#notifications">
		<span class="fa fa-comments"></span>
		<span class="badge badge-danger count" v-text="notys.length"></span>
	</a> @endif
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
	 aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="navbar-collapse collapse" id="navbarNav">
		<ul class="navbar-nav ml-auto">
			<li class="nav-item" :class="{'active': navItem.active }" v-for="(navItem, index)  in navItens">
				<a class="nav-link" @click="scrollNav(navItem, index, 1000)">@{{ navItem.name }}<span class="sr-only">(current)</span></a>
			</li>
	@include('partials/auth-menu')

		</ul>
	</div>
</nav>