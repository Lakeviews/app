	<section  @mouseover="notyOver = true" @mouseout="notyOver = false" class="notifications" :class="{ 'popup' : showNotification }">

		<div class="scrollable">


			<div class="list-group">
					<a  class="list-group-item list-group-item-action flex-column align-items-start active first-item">
					<div class="d-flex w-100">
						<span @click="showNotification=!showNotification" class="fa fa-close float-right"></span>
						<span class="text-first">MESSAGES</span>
					</div>
					
				</a>
				<a v-if="notys.length == 0" class="list-group-item bg-white no-messages	 list-group-item-action flex-column align-items-start active">
					<div class="d-flex w-100 justify-content-between">
						<h6 class="mb-1">NO MESSAGES AVAILABLE</h6>
					</div>
				</a>

				<a @click="openTicket(noti.id)" v-for="noti in notys" class="list-group-item list-group-item-action flex-column align-items-start active">
					<div class="d-flex w-100 justify-content-between">
						<h6 class="mb-1" v-text="noti.title"></h6>
						<small v-text="DiffNow(noti.message.date)"></small>
					</div>
					<p class="mb-1" v-text="noti.message.text"></p>
				</a>
			
			</div>
		</div>

	</section>