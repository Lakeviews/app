<form @keyup.enter="sendQuotation">
    <div class="form-group">
        <label for="name" class="form-control-label">@lang('client.form.name'):</label>
        <input type="text" class="form-control" name="name" v-model="formMessage.name" :class="{ 'is-invalid' : formMessage.errors.has('name') }">
        <div v-if="formMessage.errors.has('name')" class="invalid-feedback" v-text="formMessage.errors.get('name')"></div>
    </div>
    <div class="form-group">
        <label for="email" class="form-control-label">@lang('client.form.email'):</label>
        <input type="email" class="form-control" name="email" v-model="formMessage.email" required="" :class="{ 'is-invalid' : formMessage.errors.has('email') }">
        <div v-if="formMessage.errors.has('email')" class="invalid-feedback" v-text="formMessage.errors.get('email')"></div>
    </div>
    <div class="form-group">
        <label for="phone" class="form-control-label">@lang('client.form.phone_mobile'):</label>
        <input type="phone" class="form-control" name="phone" v-model="formMessage.phone" :class="{ 'is-invalid' : formMessage.errors.has('phone') }">
        <div v-if="formMessage.errors.has('phone')" class="invalid-feedback" v-text="formMessage.errors.get('phone')"></div>
    </div>
    <div class="form-group">
        <label for="message" class="form-control-label">@lang('client.form.message'):</label>
        <span class="badge badge-info float-right" v-text="QuotationCharsLeft"></span>
        <textarea rows="3" class="form-control" name="message" maxlength="255" v-model="formMessage.message" :class="{ 'is-invalid' : formMessage.errors.has('message') }"></textarea>
        <div v-if="formMessage.errors.has('message')" class="invalid-feedback" v-text="formMessage.errors.get('message')"></div>
    </div>
</form>
<button type="button" :disabled="loading" @click="formMessage.title='CONTACT REQUEST'; sendQuotation()" class="btn btn-info float-right">
            <i v-if="loading" class="fa fa-gear faa-spin animated"></i>
        @lang('client.form.send')</button>
</form>