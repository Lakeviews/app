	@if(auth()->check())
	<li class="nav-item">
		<a @click="openNotifications()" class="nav-link comments"  data-toggle="collapse" data-target="#notifications">
			<span class="fa fa-comments"></span>
			<span class="badge badge-danger count" v-text="notys.length"></span>
		</a>
	</li>

	<li class="nav-item dropdown">

		<a class="nav-link dropdown-toggle" href="#" id="user-menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<span class="blue"> @lang('client.welcome')</span> {{ auth()->user()->name }}
		</a>
		<div class="dropdown-menu" aria-labelledby="user-menu">
			@if(auth()->user()->isAdmin() || auth()->user()->isSales() || auth()->user()->isCallCenter())
			<a class="dropdown-item" href="/admin">Administration</a>
			@endIf
			@if(auth()->user()->isAdmin() || auth()->user()->isCustomer())
			<a class="dropdown-item" href="/dashboard">Dashboard</a>
			@endIf
			<a class="dropdown-item" href="/profile">@lang('client.profile')</a>
			
			<a class="dropdown-item" href="/logout">
				<i class="fa fa-power-off mr-1 danger" aria-hidden="true"></i>
				Logout
			</a>
		</div>
	</li>
	
	@else
	<li class="nav-item">
		<a href="/login" class="nav-link">
			<i class="fa fa-power-off" aria-hidden="true"></i>
		</a>
	</li>
	@endif