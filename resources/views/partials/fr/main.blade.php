{{-- WHO WE ARE --}}
<section class="who-we-are">
    <div class="container">
        <div class="row">
            <div class="content col-md-7">
                <h5 class="primary">QUI SOMMES NOUS</h5>
                <p>Nous sommes un conseiller indépendant en assurances, gestion de patrimoine et facilitateur de transactions
                    immobilières. Nous fournissons également un soutien administratif solide dans différentes entreprises.
                </p>
                <br>
                <p>
                    Notre mission est de vous assister et de vous conseiller pour atteindre vos objectifs dans tous nos domaines d'expertise.</p>
                <br>
                <p>
                    Seulement grâce à notre situation indépendante, avons-nous le privilège de servir absolument nos clients dans leur meilleur
                    intérêt.
                </p>
            </div>
            <div class="col-md-4 offset-md-1 image">
                <img id="sir" class="svg" src="/images/who-we-are.svg" alt="who we are" />
            </div>
        </div>
    </div>
</section>
{{-- INSURANCES --}}
<section class="insurances">
    <div class="container">
        <div class="row">
            <div class="col-md-6 images">
                <ul class="products">
                    <li @click="newQuote('Je veux en savoir plus sur l\'assurance Santé')">
                        <img src="images/insurances/health.svg" alt="health insurance">
                        <div class="title">
                            Santé
                        </div>
                    </li>
                    <li @click="newQuote('Je veux en savoir plus sur l\'assurance Vie')">
                        <img src="images/insurances/life.svg" alt="life insurance">
                        <div class="title">
                            Vie
                        </div>
                    </li>
                    <li @click="newQuote('Je veux en savoir plus sur l\'assurance Domestique')">
                        <img src="images/insurances/thing.svg" alt="thing insurance">
                        <div class="title">
                            Domestique
                        </div>
                    </li>
                    <li @click="newQuote('Je veux en savoir plus sur l\'assurance Véhicule')">
                        <img src="images/insurances/car.svg" alt="car insurance">
                        <div class="title">
                            Véhicule
                        </div>
                    </li>
                    <li @click="newQuote('Je veux en savoir plus sur l\'assurance Animal')">
                        <img src="images/insurances/animal.svg" alt="animal insurance">
                        <div class="title">
                            Animal
                        </div>
                    </li>
                    <li @click="newQuote('Je veux en savoir plus sur l\'assurance Legal')">
                        <img src="images/insurances/lawyer.svg" alt="lawyer insurance">
                        <div class="title">
                            Legal
                        </div>
                    </li>
                    <li @click="newQuote('Je veux en savoir plus sur l\'assurance Civil')">
                        <img src="images/insurances/civil.svg" alt="civil insurance">
                        <div class="title">
                            Civil
                        </div>
                    </li>
                    <li @click="newQuote('Je veux en savoir plus sur l\'assurance Frontalier')">
                        <img src="images/insurances/border.svg" alt="border insurance">
                        <div class="title">
                            Frontalier
                        </div>
                    </li>
                    <li @click="newQuote('Je veux en savoir plus sur l\'assurance Professionnel')">
                        <img src="images/insurances/prossional.svg" alt="prossional insurance">
                        <div class="title">
                            Professionnel
                        </div>
                    </li>
                </ul>
            </div>
            <div class="offset-lg-1 col-lg-5 col-md-6 content">
                <h5 class="info">ASSURANCES</h5>
                <p>Dans notre métier, nous sommes toujours à la recherche des meilleures solutions sur le marché pour répondre
                    à vos besoins.</p>
                <p>Nous couvrons un large éventail d'assurances.</p>
                <br>
                <button type="button" @click="newQuote('Je veux en savoir plus sur les assurances')" class="btn btn-info">@lang('client.btn.get_quote')</button>
            </div>
        </div>
    </div>
</section>
{{-- PROPERTY --}}
<section class="property">
    <div class="container">
        <div class="row">
            <div class="content col-md-7">
                <h5 class="primary">PROPRIÉTÉ
                </h5>
                <p>La gestion de la propriété est un élément essentiel dans le maintien de la capitale et de son augmentation,
                    nous fournissons ce serivce incluant Investement Risk Analisys. </p>
                <br>
                <p>
                    Nos services peuvent être décrits comme:</p>
                <br>
                <ul>
                    <li>Gestion de la propriété
                    </li>
                    <li>Analyse des propriétés spécifiques</li>
                    <li>Expertise en gestion</li>
                </ul>
                <br>
                <button type="button" @click="newQuote('Je veux en savoir plus sur Property Services')" class="btn btn-primary">@lang('client.btn.im_interested')</button>
            </div>
            <div class="col-md-4 offset-md-1 image">
                <img id="sir" class="svg" src="/images/property.svg" alt="who we are" />
            </div>
        </div>
    </div>
</section>
{{-- SERVICES --}}
<section class="services">
    <div class="container">
        <div class="row">
            <div class="col-md-6  image">
                <img id="sir" class="svg" src="/images/folder.svg" alt="who we are" />
            </div>
            <div class="offset-md-1 flex-sm-column-reverse col-md-5 content">
                <h5 class="info">SUPPORT D'ADMINISTRATION GLOBAL</h5>
                <p>Qu'il s'agisse d'une entreprise ou d'une personne privée, nous avons la solution parfaite pour faciliter
                    vos démarches administratives et réduire leurs coûts, tout en vous assurant d'être couvert de manière
                    optimale dans les assurances. Nous fournissons :
                </p>
                <br>
                <ul>
                    <li> Support administratif </li>
                    <li> Services liés à la fiscalité </li>
                    <li> Gestion de portefeuille d'assurance </li>
                    <li> Compte en ligne avec assistance </li>
                    <li> Conseil hypothécaire </li>
                </ul>
                <br>
                <button type="button" @click="newQuote('Je veux en savoir plus sur Support Administration Global')" class="btn btn-info">@lang('client.btn.let_me_know')</button>
            </div>
        </div>
    </div>
</section>
{{-- PARTNERS --}}
<section class="partners">
    <div class="container">
        <div class="row">
            <div class="content col-md-6">
                <h5 class="primary">PARTENAIRES
                    </h5>
                <p>Nous sélectionnons nos partenaires en fonction de la plus haute qualité des produits, des meilleurs prix disponibles, de la plus haute fiabilité
                    dans les affaires et la solidité financière la plus saine.</p>
                <br>
                <p>
                    Le statut de nos partenaires dans nos livres est régulièrement revu.</p>
                <br>
                <p>Nos clients et partenaires nous font confiance parce que nous associons le client approprié au meilleur partenaire.</p>
                <br>
              
            </div>
            <div class="col-md-5 offset-md-1 images">
                <div class="row">
                    <div class="col-md-6 logo"><img class="helsana" src="images/companies/helsana.png" alt="helsana"></div>
                    <div class="col-md-6 logo"><img class="zurich" src="images/companies/zurich.png" alt="zurich"></div>
                    <div class="col-md-6 logo "><img class="axa" src="images/companies/axa.jpg" alt="axa"></div>
                    <div class="col-md-6 logo"><img class="generali" src="images/companies/generali.png" alt="generali"></div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- CONTACTS --}}
<section class="contact">
    <div class="container container2">
        <div class="row">
            <div id="map" class="col-lg-6 map">
            </div>
            <div class="col-lg-6  content">
                <div class="container">
                    <h5 class="info">CONTACT</h5>
                    <p>
                        <strong>Lakeviews SA</strong>
                        <br> RUE BLAVIGNAC,10<br> Carouge
                        <br> CH - 1227 Geneve<br>
                        <br>
                        <i class="fa fa-envelope mr-2" aria-hidden="true"></i>
                        <a href="mailto:info@lakeviews.ch">info@lakeviews.ch</a>
                        <br>
                        <i class="fa fa-phone mr-2" aria-hidden="true"></i>
                        <a href="tel:+41227722211"> +41 22 772 2211</a>
                        <br>
                        <i class="fa fa-fax mr-2" aria-hidden="true"></i>
                        <a href="tel:+41227722211"> +41 22 772 2211</a>
                    </p>
                    </br>
                   @include('partials.contact')
                </div>
            </div>
        </div>
</section>