{{-- WHO WE ARE --}}
<section class="who-we-are">
    <div class="container">
        <div class="row">
            <div class="content col-md-7">
                <h5 class="primary">WHO WE ARE</h5>
                <p>We are an independent advisor in Insurances, Wealth management and a real estate transaction facilitator.
                    We also provide strong administrative support in different businesses. </p>
                <br>
                <p>
                    Our mission is to assist you and advise you to achieve your goals in all our areas of expertise.</p>
                <br>
                <p>
                    Only thanks to our independent situation, do we have the privilege to absolutely serve our clients in their best interest.
                </p>
            </div>
            <div class="col-md-4 offset-md-1 image">
                <img id="sir" class="svg" src="/images/who-we-are.svg" alt="who we are" />
            </div>
        </div>
    </div>
</section>
{{-- INSURANCES --}}
<section class="insurances">
    <div class="container">
        <div class="row">
            <div class="col-md-6 images">
                <ul class="products">
                    <li @click="newQuote('I want to know more about the insurance Health')">
                        <img src="images/insurances/health.svg" alt="health insurance">
                        <div class="title">
                            Health
                        </div>
                    </li>
                    <li @click="newQuote('I want to know more about the insurance Life')">
                        <img src="images/insurances/life.svg" alt="life insurance">
                        <div class="title">
                            life
                        </div>
                    </li>
                    <li @click="newQuote('I want to know more about the insurance Domestic')">
                        <img src="images/insurances/thing.svg" alt="thing insurance">
                        <div class="title">
                            Domestic
                        </div>
                    </li>
                    <li @click="newQuote('I want to know more about the insurance Vehicle')">
                        <img src="images/insurances/car.svg" alt="car insurance">
                        <div class="title">
                            Vehicle
                        </div>
                    </li>
                    <li @click="newQuote('I want to know more about the insurance Animal')">
                        <img src="images/insurances/animal.svg" alt="animal insurance">
                        <div class="title">
                            Animal
                        </div>
                    </li>
                    <li @click="newQuote('I want to know more about the insurance lawyer')">
                        <img src="images/insurances/lawyer.svg" alt="lawyer insurance">
                        <div class="title">
                            Legal
                        </div>
                    </li>
                    <li @click="newQuote('I want to know more about the insurance Civil')">
                        <img src="images/insurances/civil.svg" alt="civil insurance">
                        <div class="title">
                            Civil
                        </div>
                    </li>
                    <li @click="newQuote('I want to know more about the insurance Border')">
                        <img src="images/insurances/border.svg" alt="border insurance">
                        <div class="title">
                            Border
                        </div>
                    </li>
                    <li @click="newQuote('I want to know more about the insurance Professional')">
                        <img src="images/insurances/prossional.svg" alt="prossional insurance">
                        <div class="title">
                            Professional
                        </div>
                    </li>
                </ul>
            </div>
            <div class="offset-lg-1 col-lg-5 col-md-6 content">
                <h5 class="info">INSURANCES</h5>
                <p>In our business, we are always looking for the best solutions in the market to meet your needs.</p>
                <p>We cover a wide range of insurances.</p>
                <br>
                <button type="button" @click="newQuote('I want to know more about the insurances')" class="btn btn-info">GET A QUOTE</button>
            </div>
        </div>
    </div>
</section>
{{-- PROPERTY --}}
<section class="property">
    <div class="container">
        <div class="row">
            <div class="content col-md-7">
                <h5 class="primary">PROPERTY</h5>
                <p>Property Management is an essential element in the maintenance of the capital and its increase, we provide
                    this serivce including Investement Risk Analisys. </p>
                <br>
                <p>
                    Ours services can described as:</p>
                <br>
                <ul>
                    <li>Property Management</li>
                    <li>Analysis of Specific Properties</li>
                    <li>Management Expertise</li>
                </ul>
                <br>
                <button type="button" @click="newQuote('I want to know more about the Property Services')" class="btn btn-primary">@lang('client.btn.im_interested')</button>
            </div>
            <div class="col-md-4 offset-md-1 image">
                <img id="sir" class="svg" src="/images/property.svg" alt="who we are" />
            </div>
        </div>
    </div>
</section>
{{-- SERVICES --}}
<section class="services">
    <div class="container">
        <div class="row">
            <div class="col-md-6  image">
                <img id="sir" class="svg" src="/images/folder.svg" alt="who we are" />
            </div>
            <div class="offset-md-1 flex-sm-column-reverse col-md-5 content">
                <h5 class="info">GLOBAL ADMINISTRATION SUPPORT</h5>
                <p>Wheter you are a company or a private person, we have the perfect solution to facilitate your administrative
                    processes and to bring down their costs, while making sure of being optimaly covered in insurances. We
                    provide :.</p>
                <br>
                <ul>
                    <li>Administrative support</li>
                    <li>Tax related services</li>
                    <li>Insurance portfolio management</li>
                    <li>Online account with assistance</li>
                    <li>Mortgage advisory</li>
                </ul>
                <br>
                <button type="button" @click="newQuote('I want to know more about the Global Administration Support')" class="btn btn-info">@lang('client.btn.let_me_know')</button>
            </div>
        </div>
    </div>
</section>
{{-- PARTNERS --}}
<section class="partners">
    <div class="container">
        <div class="row">
            <div class="content col-md-6">
                <h5 class="primary">PARTNERS</h5>
                <p>We select our partners according to highest quality of products, best prices available, highest reliability
                    in business and soundest financial solidity. </p>
                <br>
                <p>
                    Our partners status in our books are reviewed on a regular basis.</p>
                <br>
                <p>We trust our partners because of who they are.</p>
                <br>
                <p>Our Clients and Partners trust us because we match the appropriate client with the best Partner.</p>
            </div>
            <div class="col-md-5 offset-md-1 images">
                <div class="row">
                    <div class="col-md-6 logo"><img class="helsana" src="images/companies/helsana.png" alt="helsana"></div>
                    <div class="col-md-6 logo"><img class="zurich" src="images/companies/zurich.png" alt="zurich"></div>
                    <div class="col-md-6 logo "><img class="axa" src="images/companies/axa.jpg" alt="axa"></div>
                    <div class="col-md-6 logo"><img class="generali" src="images/companies/generali.png" alt="generali"></div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- CONTACTS --}}
<section class="contact">
    <div class="container container2">
        <div class="row">
            <div id="map" class="col-lg-6 map">
            </div>
            <div class="col-lg-6  content">
                <div class="container">
                    <h5 class="info">CONTACT</h5>
                    <p>
                        <strong>Lakeviews SA</strong>
                        <br> RUE BLAVIGNAC,10<br> Carouge
                        <br> CH - 1227 Geneve<br>
                        <br>
                        <i class="fa fa-envelope mr-2" aria-hidden="true"></i>
                        <a href="mailto:info@lakeviews.ch">info@lakeviews.ch</a>
                        <br>
                        <i class="fa fa-phone mr-2" aria-hidden="true"></i>
                        <a href="tel:+41227722211"> +41 22 772 2211</a>
                        <br>
                        <i class="fa fa-fax mr-2" aria-hidden="true"></i>
                        <a href="tel:+41227722211"> +41 22 772 2211</a>
                    </p>
                    </br>
                    @include('partials.contact')
                </div>
            </div>
        </div>
</section>