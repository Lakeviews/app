<footer>
    <div class="container">
        <div class="row">
            {{-- NAVIATION --}}
            <div class="col-md-6 col-lg-3 footer-item">
                <h5 class="primary">@lang('client.navigation')</h5>
                <ul>
                    <li :class="{'active' : navItem.active }" v-for="(navItem, index) in navItens">
                        <a class="nav-link" @click="scrollNav(navItem, index, 1000)">@{{ navItem.name }}</a>
                    </li>
                </ul>
            </div>
            {{-- NEWSLETTER --}}
            <div class="col-md-6 col-lg-4 footer-item newsletter">
                {{--
                <h5>NEWSLETTER</h5>
                <div class="input-group">
                    <input type="email" class="form-control" placeholder="Enter you email">
                    <span class="input-group-btn">
                        <button class="btn btn-info btn-sm" type="button">SUBSCRIBE</button>
                    </span>
                </div> --}}
            </div>
            {{-- SOCIAL --}}
            <div class="col-md-6 col-lg-3 offset-lg-1 footer-item social">
                <h5 class="primary">SOCIAL</h5>
                <ul>
                    <li><i class="fa fa-linkedin-square" aria-hidden="true"></i></li>
                    <li><i class="fa fa-twitter-square" aria-hidden="true"></i></li>
                    <li><i class="fa fa-facebook-square" aria-hidden="true"></i></li>
                </ul>
            </div>
            <section class="copyright">
                <span class="green-txt">@lang('client.all_right_reserved')  </span>LAKEVIEWS <span class="green-txt">&#9400;&nbsp;2017 </span>
            </section>
        </div>
    </div>
</footer>