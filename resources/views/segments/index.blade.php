<segments inline-template>
    <section>
        <div class="slide-2x" :class="slide">
            {{-- SLIDE ONE --}}
            <div class="container-fluid slide-1">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="nav nav-tabs" id="primayTabs" role="tablist">
                            <li @click="tab=0" class="nav-item">
                                <a @click="tab=0" class="nav-link active text-uppercase" id="calls-tab" data-toggle="tab" href="#calls" role="tab" aria-controls="calls-tab"
                                    aria-selected="true">@lang('backoffice.my_calls')</a>
                            </li>
                            <li @click="tab=1" class="nav-item">
                                <a @click="tab=1" class="nav-link text-uppercase" id="segments-tab" data-toggle="tab" href="#segments" role="tab" aria-controls="segments-tab"
                                    aria-selected="false">@lang('backoffice.segments')</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="primayTabsContent">
                            <div class="tab-pane fade show active" id="calls" role="tabpanel" aria-labelledby="home-tab">
                                @include('segments.calls')
                            </div>
                            <div class="tab-pane fade" id="segments" role="tabpanel" aria-labelledby="profile-tab">
                                @include('segments.table')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- SLIDE TWO --}}
            <div class="container-fluid slide-2">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="nav nav-tabs" id="segmentTabs" role="tablist">
                            <li @click="info=1"  class="nav-item">
                                <a  class="nav-link active" id="client-segment-tab" data-toggle="tab" href="#segment" role="tab" aria-controls="calls-tab"
                                    aria-selected="true">@lang('backoffice.client_segment')</a>
                            </li>
                            <li @click="info=0"  class="nav-item">
                                <a  class="nav-link" id="client-segment-call-list" data-toggle="tab" href="#list" role="tab" aria-controls="segments-tab"
                                    aria-selected="false">@lang('backoffice.my_calls')</a>
                            </li>
                        </ul><button type="button" :disabled="table.model.loading" @click="back()" class="btn btn-primary btn-back-abs">BACK</button>
                        <div class="tab-content" id="primayTabsContent">
                            <div class="tab-pane fade show active" id="segment" role="tabpanel" aria-labelledby="home-tab">
                                 @include('segments.info')
                            </div>
                            <div class="tab-pane fade" id="list" role="tabpanel" aria-labelledby="profile-tab">
                                <contact-list :entity="formSegment"></contact-list>
                            </div>
                        </div>
                    </div>
                </div>
                <div v-if="info== 0" class="row">
                    <div class="col-md-12">
                        <hr class="mt-5">
                        <button :disabled="table.model.loading || !isAdmin" type="submit" @click="save()" class="btn btn-primary float-right text-uppercase">
								<i v-if="table.model.loading" class="fa fa-gear faa-spin animated"></i>
								@lang('backoffice.save')
							</button>
                        <button :disabled="table.model.loading" @click="back()" type="buttom" class="btn btn-danger float-right mr-2 text-uppercase">
                            @lang('backoffice.cancel')
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</segments>