<div class="row" v-if="table.paginate != null">
    <div class="col-md-12">
        <button v-show="tab == 1" type="button" @click="edit(table.model,true, false)" class="btn btn-primary float-right  text-uppercase mt--3">
                        
                        <i v-if="table.model.loading" class="fa fa-gear faa-spin animated"></i>
                        <i class="fa fa-plus mr-1"></i>
                        @lang('backoffice.create_new')
                    </button>
        <button @click="processDelete()" type="button" v-if="trash.length > 0" class="btn btn-danger mt--3 mr-9-05 float-right">
                        <i class="fa fa-trash"></i>
                    </button>
    </div>
    <div class="col-md-12">
        <table class="table table-striped table-hover">
            <thead>
                <tr class="header">
                    @if(auth()->user()->isAdmin())
                    <td colspan="3">
                        <h3 class="text-uppercase">@lang('backoffice.segments')</h3>
                    </td>
                    @else
                    <td colspan="2">
                        <h3 class="text-uppercase">@lang('backoffice.segments')</h3>
                    </td>
                    @endif
                    <td colspan="1">
                        @if(\Route::currentRouteName()!="client.show")
                        <fieldset class="form-group">
                            <select @change="changeClient($event)" class="selectpicker clients" data-live-search="true">
                                                    <option value="" selected>@lang('backoffice.please_select_a_client')</option>
                                                    @foreach($clients as $client)
                                                        <option value="{{ $client->id }}">{{ $client->name }}</option>
                                                    @endforeach
                                                </select>
                        </fieldset>
                        @endif
                    </td>
                    <td colspan="1">
                        @if(isset($segments))
                        <fieldset class="form-group">
                            <select @change="changeSegment($event)" class="selectpicker segments" data-live-search="true">
                                                <option value="" selected>@lang('backoffice.please_select_a_segment')</option>
                                                @foreach($segments as $segment)
                                                    <option value="{{ $segment->id }}">{{ $segment->name }}</option>
                                                @endforeach
                                            </select>
                        </fieldset>
                        @endif
                    </td>
                    <td colspan="2">
                        <div class="input-group">
                            <input type="text" v-model="table.params.search" @keyup="table.bind()" class="form-control" placeholder="@lang('backoffice.search_here')">
                            <span class="input-group-addon">
                                            <i class="fa fa-search"></i>
                                        </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    @if(auth()->user()->isAdmin())
                    <th class="w3" scope="col">
                        <input @click="selectetAll($event.target.checked)" type="checkbox">
                    </th>
                    @endif
                    <th class="w4" @click="table.setSort('id')" :class="table.sorts['id']" scope="col">@lang('backoffice.id')</th>
                    <th @click="table.setSort('client_id')" :class="table.sorts['client_id']" scope="col">@lang('backoffice.client')</th>
                    <th @click="table.setSort('segment_type_id')" :class="table.sorts['segment_type_id']" scope="col">@lang('backoffice.segment')</th>
                    <th @click="table.setSort('contact_name')" :class="table.sorts['contact_name']" scope="col">@lang('backoffice.name')</th>
                    <th @click="table.setSort('contact_role')" :class="table.sorts['contact_role']" scope="col">@lang('backoffice.role')</th>
                    <th class="actions" scope="col">@lang('backoffice.action')</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(row, index) in table.paginate.data">
                    @if(auth()->user()->isAdmin())
                    <td scope="row"><input v-model="row.selected" type="checkbox"></td>
                    @endif
                    <td scope="col" v-text="row.id"></td>
                    <td scope="col" v-text="row.client.name"></td>
                    <td scope="col" v-text="row.segment.name"></td>
                    <td scope="col" v-text="row.contact_name"></td>
                    <td scope="col" v-text="row.contact_role"></td>
                    <td class="tac" scope="col">
                        <button @click="edit(row, true, false)" type="button" class="btn btn-sm btn-primary">
                                        <i v-if="row.loading" class="fa fa-gear faa-spin animated"></i>
                                        <i class="fa fa-edit"></i>
                                    </button> @if(auth()->user()->isAdmin())
                        <button @click="del(index)" type="button" class="btn btn-sm btn-danger	">
                            <i class="fa fa-trash"></i>
                        </button> @endif
                    </td>
                </tr>
            </tbody>
        </table>
        <nav class="float-right">
            <ul class="pagination">
                <li class="page-item" :class="{ 'disabled' : table.paginate.current_page == 1}">
                    <a class="page-link" @click="table.first()" tabindex="-1">
                                    <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                                </a>
                </li>
                <li class="page-item" :class="{ 'disabled' : table.paginate.current_page == 1}">
                    <a class="page-link" @click="table.prev()" tabindex="-1">
                                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                                </a>
                </li>
                <li class="page-item" :class="{'active': table.paginate.current_page == page}" v-for="page in table.pages()"><a class="page-link" @click="table.goto(page)" v-text="page" tabindex="-1"></a></li>
                <li v-if="table.paginate.last_page > 2" class="page-item" :class="{'active': table.paginate.current_page == table.paginate.last_page }">
                    <a class="page-link" @click="table.last()" v-text=" table.paginate.last_page" tabindex="-1">
                                </a>
                </li>
                <li class="page-item" :class="{ 'disabled' : table.paginate.current_page == table.paginate.last_page }">
                    <a class="page-link" @click="table.next()" tabindex="-1">
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </a>
                </li>
                <li class="page-item" :class="{ 'disabled' : table.paginate.current_page == table.paginate.last_page }">
                    <a @click="table.last()" class="page-link" tabindex="-1">
                                    <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                </a>
                </li>
            </ul>
        </nav>
    </div>
</div>