<div class="row" v-if="tableCalls.paginate != null">
    <div class="col-md-12">
        <table class="table table-striped table-hover">
            <thead>
                <tr class="header">
                    <td colspan="3">
                        @if(isset($users))
                        <fieldset class="form-group">
                            <select @change="changeContactedBy($event)" class="selectpicker users" data-live-search="true">
                                                        <option value="" selected>@lang('backoffice.please_select_responsable')</option>
                                                        @foreach($users as $user)
                                                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                                                        @endforeach
                                                    </select>
                        </fieldset>
                        @endif
                    </td>
                  
                    <td colspan="1">
                        @if(isset($clients))
                        <fieldset class="form-group">
                            <select @change="changeClientCalls($event)" class="selectpicker clients" data-live-search="true">
                                                        <option value="" selected>@lang('backoffice.please_select_a_client')</option>
                                                        @foreach($clients as $client)
                                                            <option value="{{ $client->id }}">{{ $client->name }}</option>
                                                        @endforeach
                                                    </select>
                        </fieldset>
                        @endif
                    </td>
                    <td colspan="2">
                        @if(isset($segments))
                        <fieldset class="form-group">
                            <select @change="changeSegmentCalls($event)" class="selectpicker segments" data-live-search="true">
                                                    <option value="" selected>@lang('backoffice.please_select_a_segment')</option>
                                                    @foreach($segments as $segment)
                                                        <option value="{{ $segment->id }}">{{ $segment->name }}</option>
                                                    @endforeach
                                                </select>
                        </fieldset>
                        @endif
                    </td>
                    <td colspan="2">
                        <div class="input-group">
                            <input type="text" v-model="tableCalls.params.search" @keyup="tableCalls.bind()" class="form-control" placeholder="@lang('backoffice.search_here')">
                            <span class="input-group-addon">
                                                <i class="fa fa-search"></i>
                                            </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="w-10" @click="tableCalls.setSort('user_id')" :class="tableCalls.sorts['user_id']" scope="col">@lang('backoffice.responsable')</th>
                    <th class="w-10" @click="tableCalls.setSort('client_id')" :class="tableCalls.sorts['client_id']" scope="col">@lang('backoffice.client')</th>
                    <th class="w-10" @click="tableCalls.setSort('segment_type_id')" :class="tableCalls.sorts['segment_type_id']" scope="col">@lang('backoffice.segment')</th>
                    <th class="w-15" @click="tableCalls.setSort('name')" :class="tableCalls.sorts['name']" scope="col">@lang('backoffice.who')</th>
                    <th class="w-15" @click="tableCalls.setSort('contact_postcode')" :class="tableCalls.sorts['contact_postcode']" scope="col">@lang('backoffice.postcode')</th>
                    <th class="w-15" @click="tableCalls.setSort('contact_later_name')" :class="tableCalls.sorts['contact_later_name']" scope="col">@lang('backoffice.person')</th>
                    {{--
                    <th @click="tableCalls.setSort('contact_later_role')" :class="tableCalls.sorts['contact_later_role']" scope="col">ROLE</th> --}}
                    <th class="w-25" @click="tableCalls.setSort('contact_later_details')" :class="tableCalls.sorts['contact_later_details']"
                        scope="col">@lang('backoffice.contact')</th>
                    <th class="w-10" @click="tableCalls.setSort('contact_later_date')" :class="tableCalls.sorts['contact_later_date']" scope="col">@lang('backoffice.date')</th>
                    <th class="actions w-10" scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(row, index) in tableCalls.paginate.data">
                    <td scope="col" v-text="row.user.name"></td>
                    <td scope="col" v-text="row.segment.client.name"></td>
                    <td scope="col" v-text="row.segment.segment.name"></td>
                    <td scope="col" v-text="row.name"></td>
                    <td scope="col" v-text="row.contact_postcode"></td>
                    <td scope="col" v-text="row.contact_later_name"></td>
                    <td scope="col" v-text="row.contact_later_details"></td>
                    <td scope="col" v-text="formatDateTime(row.contact_later_date)"></td>
                    <td class="tac" scope="col">
                        <button @click="callNow(row)" type="button" class="btn btn-sm btn-primary">
                                            <i v-if="row.loading" class="fa fa-gears faa-spin animated"></i>
                                            <i v-if="row.type==1" class="fa fa-phone"></i>
                                            <i v-if="row.type==2" class="fa fa-envelope"></i>
                                        </button>
                    </td>
                </tr>
            </tbody>
        </table>
        <nav class="float-right">
            <ul class="pagination">
                <li class="page-item" :class="{ 'disabled' : tableCalls.paginate.current_page == 1}">
                    <a class="page-link" @click="tableCalls.first()" tabindex="-1">
                                        <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                                    </a>
                </li>
                <li class="page-item" :class="{ 'disabled' : tableCalls.paginate.current_page == 1}">
                    <a class="page-link" @click="tableCalls.prev()" tabindex="-1">
                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                    </a>
                </li>
                <li class="page-item" :class="{'active': tableCalls.paginate.current_page == page}" v-for="page in tableCalls.pages()"><a class="page-link" @click="tableCalls.goto(page)" v-text="page" tabindex="-1"></a></li>
                <li v-if="tableCalls.paginate.last_page > 2" class="page-item" :class="{'active': tableCalls.paginate.current_page == tableCalls.paginate.last_page }">
                    <a class="page-link" @click="tableCalls.last()" v-text=" tableCalls.paginate.last_page" tabindex="-1">
                                    </a>
                </li>
                <li class="page-item" :class="{ 'disabled' : tableCalls.paginate.current_page == tableCalls.paginate.last_page }">
                    <a class="page-link" @click="tableCalls.next()" tabindex="-1">
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </a>
                </li>
                <li class="page-item" :class="{ 'disabled' : tableCalls.paginate.current_page == tableCalls.paginate.last_page }">
                    <a @click="tableCalls.last()" class="page-link" tabindex="-1">
                                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div>