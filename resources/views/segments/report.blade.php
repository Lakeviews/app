@extends('report') 
@section('content')
<table class="table">
    <thead>
        <tr>
            <th colspan="4" class="text-uppercase">
                @lang('backoffice.client_contact_log') <strong>{{ $data['segment']->Client->name }}</strong> SEGMENT <strong>{{ $data['segment']->Segment->name }}</strong>
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach($data['contacts'] as $contact)
        <tr style="background-color: #ddd; margin-top:20px;">
            <td colspan="2" scope="row" class="text-uppercase">
                @if($contact->type == 1)
                <strong>@lang('backoffice.call_to') </strong> @else
                <strong>@lang('backoffice.message_to') </strong> @endif {{ $contact->name.' -'.$contact->contact_name }}
                <strong>&nbsp;BY&nbsp;</strong> {{ $contact->user->name }}
            </td>
            <td>

                @if($contact->status == 1)
                <span class="badge badge-info float-right">@lang('backoffice.complete')</span> @endif @if($contact->status
                == 2)
                <span class="badge badge-warning float-right text-uppercase">@lang('backoffice.unavailable')</span> @endif
                @if($contact->status == 3)
                <span class="badge badge-primary float-right text-uppercase">@lang('backoffice.contact_later')</span> @endif
                @if($contact->status==4)
                <span class="badge badge-success mr-2 float-right text-uppercase">@lang('backoffice.successfull')</span>                @endif @if($contact->status==5)
                <span class="badge badge-danger mr-2 float-right text-uppercase">@lang('backoffice.unsuccessful')</span>                @endif @if($contact->contact_recall_status==1)
                <span class="badge badge-secondary mr-2 float-right text-uppercase">@lang('backoffice.done')</span> @endif
            </td>
            <td>{{ \Carbon\Carbon::parse($contact->updated_at)->formatLocalized('%d %B %Y - %H:%M') }}
            </td>
        </tr>
        <tr>
            <td scope="row" colspan="4">
                <span class="badge badge-primary badge-hr text-uppercase">@lang('backoffice.info_contact') </span>
            </td>
        </tr>
        <tr>
            <td scope="row" colspan="2" class="text-uppercase">
                <strong> @lang('backoffice.info_contact')</strong>
            </td>
            <td colspan="2" class="text-uppercase">
                <strong>@lang('backoffice.person_name')</strong>
            </td>
        </tr>
        <tr>
            <td scope="row" colspan="2">
                {{ $contact->name }}
            </td>
            <td colspan="2">
                {{ $contact->contact_name }}
            </td>
        </tr>
        <tr>
            <td scope="row" colspan="2" class="text-uppercase">
                <strong>@lang('backoffice.person_role')</strong>
            </td>
            <td colspan="2" class="text-uppercase">
                <strong>@lang('backoffice.person_contact')</strong>
            </td>
        </tr>
        <tr>
            <td scope="row" colspan="2">
                {{ $contact->contact_role }}
            </td>
            <td colspan="2">
                {{ $contact->contact_details }}
            </td>
        </tr>
        <tr>
            <td scope="row" class="comments" colspan="4">
                <strong>@lang('backoffice.comments')</strong>
            </td>
        </tr>
        <tr>
            <td scope="row" colspan="4">
                {{ $contact->comments }}
            </td>
        </tr>
        @if($contact->status == 3)
        <tr>
            <td scope="row" colspan="4">
                <span class="badge badge-primary badge-hr text-uppercase">@lang('backoffice.person_call_later_info')</span>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="text-uppercase"> <strong>@lang('backoffice.name')</strong></td>
            <td colspan="2" class="text-uppercase"> <strong>@lang('backoffice.role')</strong></td>
        </tr>
        <tr>
            <td colspan="2">{{ $contact->contact_later_name }}</td>
            <td colspan="2">{{ $contact->contact_later_role }}</td>
        </tr>
        <tr>
            <td colspan="2" class="text-uppercase"> <strong>@lang('backoffice.details')</strong></td>
            <td colspan="2" class="text-uppercase"> <strong>@lang('backoffice.date')</strong></td>
        </tr>
        <tr>
            <td colspan="2">{{ $contact->contact_later_details }}</td>
            <td colspan="2">
                {{ \Carbon\Carbon::parse($contact->contact_later_date)->formatLocalized('%d %B %Y - %H:%M') }}
            </td>
        </tr>
        @endif
        <tr>
            <td colspan="4">
            </td>
        </tr>
        <tr>
            <td colspan="4">
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection