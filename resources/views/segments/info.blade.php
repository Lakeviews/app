<div v-if="formSegment != null"  class="card">
    <div class="card-header">
        INFO
    </div>
    <div class="card-body">
        <form class="p-3" @keypress="formSegment.errors.clear($event.target.name)" @keypress.enter="save()">
            <div class="row">
                <div class="col-md-12">
                    <fieldset class="form-group" :class="{ 'is-invalid' : formSegment.errors.has('client_id') }">
                        <label class="w-100" for="active">Client</label>
                        <select  :disabled="!isAdmin" v-model="formSegment.client_id" class="selectpicker clients-form" data-live-search="true">
                                    <option value="" selected>@lang('backoffice.please_select_a_client')</option>
                                    @foreach($clients as $client)
                                        <option value="{{ $client->id }}">{{ $client->name }}</option>
                                    @endforeach
                                </select>
                        <div v-if="formSegment.errors.has('client_id')" class="invalid-feedback" v-text="formSegment.errors.get('client_id')"></div>
                    </fieldset>
                </div>
                <div class="col-md-12">
                    <fieldset class="form-group" :class="{ 'is-invalid' : formSegment.errors.has('segment_type_id') }">
                        <label class="w-100" for="active">@lang('backoffice.sector')</label>
                        <select  :disabled="!isAdmin" v-model="formSegment.segment_type_id" class="selectpicker segments-form" data-live-search="true">
                            <option value="" selected>@lang('backoffice.please_select_a_segment')</option>
                            @foreach($segments as $segment)
                                <option value="{{ $segment->id }}">{{ $segment->name }}</option>
                            @endforeach
                        </select>
                        <div v-if="formSegment.errors.has('segment_type_id')" class="invalid-feedback" v-text="formSegment.errors.get('segment_type_id')"></div>
                    </fieldset>
                </div>
                <div class="col-lg-6">
                    <fieldset class="form-group">
                        <label for="name">@lang('backoffice.person_name')</label>
                        <input :disabled="!isAdmin" type="text" v-model="formSegment.contact_name" class="form-control" name="name" placeholder="Person Name" :class="{ 'is-invalid' : formSegment.errors.has('contact_name') }">
                        <div v-if="formSegment.errors.has('contact_name')" class="invalid-feedback" v-text="formSegment.errors.get('contact_name')"></div>
                    </fieldset>
                </div>
                <div class="col-lg-6">
                    <fieldset class="form-group">
                        <label for="name">@lang('backoffice.person_role')</label>
                        <input :disabled="!isAdmin" type="text" v-model="formSegment.contact_role" class="form-control" name="name" placeholder="Person Role" :class="{ 'is-invalid' : formSegment.errors.has('contact_role') }">
                        <div v-if="formSegment.errors.has('contact_role')" class="invalid-feedback" v-text="formSegment.errors.get('contact_role')"></div>
                    </fieldset>
                </div>
                <div class="col-lg-12">
                    <fieldset class="form-group">
                        <label for="name">@lang('backoffice.person_contact')</label>
                        <input :disabled="!isAdmin" type="text" v-model="formSegment.contact_details" class="form-control" name="name" placeholder="Person Number" :class="{ 'is-invalid' : formSegment.errors.has('contact_details') }">
                        <div v-if="formSegment.errors.has('contact_details')" class="invalid-feedback" v-text="formSegment.errors.get('contact_details')"></div>
                    </fieldset>
                </div>

                <div v-show="formSegment.id > 0" class="col-md-12">
                    <fieldset class="form-group">
                        <upload-control :canUpload="isAdmin" :canDelete="isAdmin" :description="true" :entity="'segment/' + formSegment.id" :ext="['.jpg','.jpeg','.png','.doc', '.docx', '.xls', '.xlsx', '.ppt', 'pptx', '.pdf', '.zip']" :size="4096" event="update-segment-files" update='update-child-segment-files'></upload-control>
                    </fieldset>
                </div>

            </div>
        </form>
    </div>
</div>