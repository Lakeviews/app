<products inline-template>
	<section>
		<div class="slide-2x" :class="slide">
			{{-- SLIDE ONE --}}
			<div class="container-fluid slide-1">
				<div class="row">
					<div class="col-md-6">
						<h6 class="primary">PRODUCTS</h6>
					</div>
					<div class="col-md-6">
						<button type="button" @click="edit(table.model,true)" class="btn btn-primary float-right pot-05">
							
							<i v-if="table.model.loading" class="fa fa-gear faa-spin animated"></i>
							<i class="fa fa-plus mr-1"></i>
							CREATE NEW
						</button>
						<button @click="processDelete()" type="button" v-if="trash.length > 0" class="btn btn-danger mr-2 pot-05 float-right">
							<i class="fa fa-trash"></i>
						</button>
					</div>
				</div>
				<div class="row" v-if="table.paginate != null">
					<div class="col-md-12">

						<table class="table table-striped table-hover">
							<thead>
								<tr class="header">
									<td colspan="3">
										<h3>PRODUCTS MANAGEMENT </h3>
									</td>
									<td colspan="2"></td>
									<td colspan="2">
										<div class="input-group">
											<input type="text" v-model="table.params.search" @keyup="table.bind()" class="form-control" placeholder="Search here">
											<span class="input-group-addon">
												<i class="fa fa-search"></i>
											</span>
										</div>
									</td>
								</tr>
								<tr>
									<th class="w3" scope="col">
										<input @click="selectetAll($event.target.checked)" type="checkbox">
									</th>
									<th class="w4" @click="table.setSort('id')" :class="table.sorts['id']" scope="col">ID</th>
									<th @click="table.setSort('name')" :class="table.sorts['name']" scope="col">Name</th>
									<th @click="table.setSort('description')" :class="table.sorts['email']" scope="col">Description</th>
									<th @click="table.setSort('type')" :class="table.sorts['type']" scope="col">Permission</th>
									<th @click="table.setSort('active')" :class="table.sorts['active']" scope="col">Status</th>
									<th class="actions" scope="col">Action</th>
								</tr>
							</thead>
							<tbody>
								<tr v-for="(row, index) in table.paginate.data">
									<td scope="row"><input :disabled="row.name=='GENERIC TICKET'" v-model="row.selected" type="checkbox"></td>
									<td scope="col" v-text="row.id"></td>
									<td scope="col" v-text="row.name"></td>
									<td scope="col" v-text="row.description"></td>
									<td scope="col">
										<span v-if="row.type ==1" class="badge badge-primary">INSURANCE</span>
										<span v-if="row.type ==2" class="badge badge-warning">GENERIC</span>
									</td>
									<td scope="col">
										<span v-if="row.active" class="badge badge-success">ON</span>
										<span v-if="!row.active" class="badge badge-danger">OFF</span>
									</td>
									<td class="tac" scope="col">
										<button :disabled="row.name=='GENERIC TICKET'" @click="edit(row, true)" type="button" class="btn btn-sm btn-primary">
											<i v-if="row.loading" class="fa fa-gear faa-spin animated"></i>
											<i class="fa fa-edit"></i>
										</button>
										<button :disabled="row.name=='GENERIC TICKET'" @click="del(index)" type="button" class="btn btn-sm btn-danger	">
											<i class="fa fa-trash"></i>
										</button>
									</td>
								</tr>
							</tbody>
						</table>
						<nav class="float-right">
							<ul class="pagination">
								<li class="page-item" :class="{ 'disabled' : table.paginate.current_page == 1}">
									<a class="page-link" @click="table.first()" tabindex="-1">
										<i class="fa fa-angle-double-left" aria-hidden="true"></i>
									</a>
								</li>
								<li class="page-item" :class="{ 'disabled' : table.paginate.current_page == 1}">
									<a class="page-link" @click="table.prev()" tabindex="-1">
										<i class="fa fa-angle-left" aria-hidden="true"></i>
									</a>
								</li>
								<li class="page-item" :class="{'active': table.paginate.current_page == page}" v-for="page in table.pages()"><a class="page-link" @click="table.goto(page)" v-text="page" tabindex="-1"></a></li>
								<li v-if="table.paginate.last_page > 2" class="page-item" :class="{'active': table.paginate.current_page == table.paginate.last_page }">
									<a class="page-link" @click="table.last()" v-text=" table.paginate.last_page" tabindex="-1">
									</a>
								</li>
								<li class="page-item" :class="{ 'disabled' : table.paginate.current_page == table.paginate.last_page }">
									<a class="page-link" @click="table.next()" tabindex="-1">
										<i class="fa fa-angle-right" aria-hidden="true"></i>
									</a>
								</li>
								<li class="page-item" :class="{ 'disabled' : table.paginate.current_page == table.paginate.last_page }">
									<a @click="table.last()" class="page-link" tabindex="-1">
										<i class="fa fa-angle-double-right" aria-hidden="true"></i>
									</a>
								</li>
							</ul>
						</nav>
					</div>
				</div>
			</div>

			{{-- SLIDE TWO --}}
			<div v-if="formProduct != null" class="container-fluid slide-2">
				<div class="row lb">
					<div class="col-md-6">
						<h6 class="primary"><span v-text="formProduct.id != undefined ? 'EDITING' : 'NEW'"></span> PRODUCT</h6>
						<h2 class="pt-1 pb-1"><span v-text="formProduct.name"></span>&nbsp; </h2>
					</div>
					<div class="col-md-6">
						<button type="button" :disabled="table.model.loading" @click="back()" class="btn btn-primary float-right">BACK</button>
					</div>
					<div class="col-md-12">
						<hr class="mt--05">
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								INFO
							</div>
							<div class="card-body">
								<form class="p-3" @keypress="formProduct.errors.clear($event.target.name)" @keypress.enter="save()">
									<div class="row">
										<div class="col-xl-6">

											<ul class="nav nav-pills mb-3" id="meta-tab" role="tablist">
												<li class="nav-item" v-for="(meta, index) in formProduct.meta">
													<a class="nav-link" :id="'meta' + index" v-text="meta.lang" :class="{ 'active': index == 0 }" data-toggle="pill" :href="'#tab-' + index"
													 role="tab" aria-controls="'#tab-' + index" aria-selected="true"></a>
												</li>
											</ul>
											<div class="tab-content">
												<div v-for="(meta, index) in formProduct.meta" class="tab-pane fade" :class="{ 'show active': index == 0 }" :id="'tab-' + index"
												 role="tabpanel" :aria-labelledby="'tab-' + index">
													<fieldset class="form-group">
														<label for="name">Name</label>
														<input type="text" v-model="meta.name" class="form-control" name="name" :placeholder="'Enter the name in ' + meta.lang" :class="{ 'is-invalid' : formProduct.errors.has('name') && index == 0 }">
														
														<div v-if="formProduct.errors.has('name') && index == 0" class="invalid-feedback" v-text="formProduct.errors.get('name')"></div>
													</fieldset>

													<fieldset class="form-group">
														<label for="description">Description</label>
														<textarea rows="3" maxlength="300" v-model="meta.description" class="form-control" name="description" :placeholder="'Enter a description in ' + meta.lang "></textarea>
													</fieldset>
												</div>
											</div>
										</div>


										<div class="col-xl-6">
											<fieldset class="form-group">
												<label for="companies">Company</label>
												<select id="companies" onchange="formProduct.company_id = this.value" class="selectpicker" data-live-search="true">
													<option value="" selected>Please select or None</option>
													@foreach($companies as $company)
													<option value="{{ $company->id }}">{{ $company->name }}</option>
													@endforeach
												</select>
											</fieldset>
											<div class="row no-gutters">
												<div class="col-xl-4">
													<fieldset class="form-group">
														<label class="w-100" for="active">Active</label>
														<div class="btn-group" data-toggle="buttons">
															<label @click="formProduct.active = 1" class="btn btn-default" :class="{ 'active' : formProduct.active == 1}">
																ACTIVE
															</label>
															<label @click="formProduct.active = 0" class="btn btn-default" :class="{ 'active' : formProduct.active == 0}">
																INACTIVE
															</label>
														</div>
													</fieldset>
												</div>
												<div class="col-xl-4">
													<fieldset class="form-group">
														<label class="w-100" for="type">Type</label>
														<div class="btn-group" data-toggle="buttons">
															<label @click="formProduct.type = 1" class="btn btn-default" :class="{ 'active' : formProduct.type == 1}">
																INSURANCE
															</label>
															<label @click="formProduct.type = 2" class="btn btn-default" :class="{ 'active' : formProduct.type == 2}">
																GENERIC
															</label>
														</div>
													</fieldset>
												</div>
												<div class="col-xl-4">
													<fieldset class="form-group">
														<label class="w-100" for="available_for_ticket">Available for Ticket</label>
														<div class="btn-group" data-toggle="buttons">
															<label @click="formProduct.available_for_ticket = 1" class="btn btn-default" :class="{ 'active' : formProduct.available_for_ticket == 1}">
																ACTIVE
															</label>
															<label @click="formProduct.available_for_ticket = 0" class="btn btn-default" :class="{ 'active' : formProduct.available_for_ticket == 0}">
																INACTIVE
															</label>
														</div>
													</fieldset>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>

				</div>
				<div>
					<div class="row">
						<div class="col-md-12">
							<hr class="mt-5">
							<button :disabled="table.model.loading" type="submit" @click="save()" class="btn btn-primary float-right">
								<i v-if="table.model.loading" class="fa fa-gear faa-spin animated"></i>
								SAVE
							</button>
							<button :disabled="table.model.loading" @click="back()" type="buttom" class="btn btn-danger float-right mr-2">CANCEL</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</products>