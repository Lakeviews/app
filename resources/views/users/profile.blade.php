@extends('layoutPage')

@section('content')
<profile inline-template :user="{{ auth()->user() }}">
	<div class="content-page">
		<div class="container wrapper">
			<div class="content-2x" :class="slide">
				<section class="slide-1">

					<div class="row">	
						<div class="content col-sm-12">
							<h5 class="info-ligth mb-0 pb-05">
								@lang('client.profile')
							</h5>
							<p>@lang('client.profile.title') </p>
						</div>
					</div>
					<div class="row profile">
						{{-- INFO --}}
						<div class="col-md-6 mb-3">	
							@include('users.info')
						</div>	

						<div class="col-md-6 mb-3">
							@include('users.details')
						</div>			
						
					</div>
				</section>

				<section class="slide-2">
					<div id="template"></div>
				</section>
			</div>
		</div>
	</div>
</profile>

@endsection