<insurances-documents inline-template>
	<div id="template">
		<div class="row">
			<div class="col-8">
				<h5 class="info-ligth mb-0 pb-05">MY INSURANCES & DOCUMENTS</h5>
			</div>
			<div class="col-4">
				<button type="button" @click="dashboard()" class="btn btn-info float-right top--05">
					<i class="fa fa-chevron-left mr-02" aria-hidden="true"></i>
					BACK
				</button>
			</div>
			<div class="col-12">
				<p class="white">Here you can consult your insurances, document processes like rent, watter bill etc... at bottom you will find a a resume
					with amounts to pay by period</p>
			</div>
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-3">
						<select id="years" v-model="year" @change="bind()" class="selectpicker mt-3 mb-3 float-left w-25">
							<option v-for="year in years" :value="year" selected v-text="year"></option>
						</select>
					</div>
					<div class="col-md-3 offset-md-6">
						<select id="payment_type" v-model="payment_type" class="selectpicker mt-3 mb-3 float-right w-25">
							<option value="0">ALL</option>
							<option value="1">MONTHLY</option>
							<option value="2">QUATERLY</option>
							<option value="3">HALF-YEAR</option>
							<option value="4">ANNUAL</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div id="products" role="tablist" aria-multiselectable="true">
					<div class="card" v-for="(type_insurance, index) in finalData.type_insurances">
						<div class="card-header" @click="openCollapse('#pro-' + index)" role="tab">
							<div class="row">
								<div class="col-6">
									<h5 class="mb-0 blue" v-text="insuranceName(type_insurance)">
									</h5>
								</div>
								<div class="col-6 tar">
									<div class="period">
										<span v-if="payment_type == 0" class="badge badge-secondary">ALL</span>
										<span v-if="payment_type ==1" class="badge badge-success">MONTHLY</span>
										<span v-if="payment_type ==2" class="badge badge-info">QUATERLY</span>
										<span v-if="payment_type ==3" class="badge badge-warning">HALF-YEAR</span>
										<span v-if="payment_type ==4" class="badge badge-success">ANNUAL</span>
										<p v-if="payment_type == 0" v-text="type_insurance.total_period + ' CHF'"></p>
										<p v-if="payment_type == 1" v-text="type_insurance.total_monthly + ' CHF'"></p>
										<p v-if="payment_type == 2" v-text="type_insurance.total_quaterly + ' CHF'"></p>
										<p v-if="payment_type == 3" v-text="type_insurance.total_half_year + ' CHF'"></p>
										<p v-if="payment_type == 4" v-text="type_insurance.total_annual + ' CHF'"></p>
									</div>
								</div>
							</div>
						</div>
						<div :id="'pro-' + index" class="collapse" role="tabpanel" :aria-labelledby="'pro-' + index" data-parent="#accordion">
							<div class="card-body">
								<div class="card" v-for="document in type_insurance.documents">
									<div class="card-header" @click="openCollapse('#doc-' + document.id)" role="tab">
										<div class="row">
											<div class="col-md-3">
												<h6 class="mb-0" v-text="document.name">
												</h6>

											</div>
											<div class="col-md-6">
												<p v-text="document.description"></p>
											</div>
											<div class="col-md-3">
												<span v-if="document.payment_type ==1" class="badge badge-success">MONTHLY</span>
												<span v-if="document.payment_type ==2" class="badge badge-info">QUATERLY</span>
												<span v-if="document.payment_type ==3" class="badge badge-warning">HALF-YEAR</span>
												<span v-if="document.payment_type ==4" class="badge badge-success">ANNUAL</span>
												<p class="float-right" v-text="document.amount + ' CHF'"></p>
											</div>
										</div>
									</div>
									<div :id="'doc-' + document.id" class="collapse" role="tabpanel" :aria-labelledby="'doc-' + document.id" data-parent="#accordion">
										<div class="card-body">
											<section>
												<div class="row">
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-4 font-weight-bold">
																	<span class="badge badge-primary badge-contract-info">ASSIGNED TO:</span>
																NAME</div>
															<div class="col-md-8" v-text="document.name"></div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-4 font-weight-bold">BIRTHDATE</div>
															<div class="col-md-8" v-text="document.birthdate"></div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-4 font-weight-bold">ADDRESS</div>
															<div class="col-md-8" v-text="formatAddress(document)"></div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-4 font-weight-bold">COUNTRY</div>
															<div class="col-md-8" v-text="  document.country"></div>
														</div>
													</div>
												</div>
											</section>

											<section>
												<div class="row">
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-4 font-weight-bold">
																<span class="badge badge-secondary badge-contract-info">CUSTOMER INFO:</span>
																NAME</div>
															<div class="col-md-8" v-text="document.customer.name"></div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-4 font-weight-bold">BIRTHDATE</div>
															<div class="col-md-8" v-text="document.customer.birthdate"></div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-4 font-weight-bold">ADDRESS</div>
															<div class="col-md-8" v-text="formatAddress(document.customer)"></div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-4 font-weight-bold">COUNTRY</div>
															<div class="col-md-8" v-text="  document.customer.country"></div>
														</div>
													</div>
												</div>
											</section>


											<section>
												<div class="row">
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-4 font-weight-bold">TYPE CONTRACT</div>
															<div class="col-md-8" v-text="type_insurance.name"></div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-4 font-weight-bold">COMPANY</div>
															<div v-if="document.company != null" class="col-md-8" v-text="document.company.name"></div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="row">
															<div v-if="document.type== 1" class="col-md-4 font-weight-bold">POLICY Nº</div>
															<div v-if="document.type!= 1" class="col-md-4 font-weight-bold">REFERENCE</div>
															<div class="col-md-8" v-text="formatAddress(document)"></div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-4 font-weight-bold">MANAGED</div>
															<div class="col-md-8">
																<span v-if="document.managed ==1" class="badge badge-primary">YES</span>
																<span v-if="document.managed ==0" class="badge badge-danger">NO</span>
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-4 font-weight-bold">START DATE</div>
															<div class="col-md-8" v-text="document.date_start"></div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-4 font-weight-bold">END DATE</div>
															<div class="col-md-8" v-text="document.date_end"></div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-4 font-weight-bold">PRIME</div>
															<div class="col-md-8">
																@{{ document.amount + ' CHF' }}
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-4 font-weight-bold">PERIOD</div>
															<div class="col-md-8">
																<span v-if="document.payment_type ==1" class="badge badge-success">MONTHLY</span>
																<span v-if="document.payment_type ==2" class="badge badge-info">QUATERLY</span>
																<span v-if="document.payment_type ==3" class="badge badge-warning">HALF-YEAR</span>
																<span v-if="document.payment_type ==4" class="badge badge-success">ANNUAL</span>
															</div>
														</div>
													</div>
												</div>
											</section>
											<section>
												<div class="row">
													<div class="col-md-12">
														<div class="row">
															<div class="col-md-4 font-weight-bold">DOCUMENTS</div>
														</div>
													</div>
													<div class="col-md-12">
														<div class="row">
															<div class="col-md-6" v-for="file in document.files">
																{{-- <i class="fa fa-trash"></i> --}}
																<div class="list-group-item">
																	<span v-text="file.name"></span> <i class="fa fa-download"></i>
																</div>
															</div>
														</div>
													</div>
												</div>
											</section>
										</div>
									</div>
								</div>
							</div>
						</div>
					
					</div>
					<div class="card totals">
						<div class="card-header bg section-1">
							<div class="row">
								<div class="col-md-6">
									<h5 class="mb-0 white">
										TOTALS
									</h5>
								</div>
								<div class="col-md-3 tar">
									<p>PERIODICAL</p>
								</div>
								<div class="col-md-3 tar">
									<p>ANNUAL</p>
								</div>
							</div>
						</div>
						<div class="card-body">
							<div class="row no-gutters">
								<div class="col-md-6 bg info-ligth-3 p-2">
									TOTAL MONTHLY
								</div>
								<div class="col-md-3 p-2 tar" v-text="finalData.total_monthly + ' CHF'">
								</div>
								<div class="col-md-3 p-2 tar" v-text="total_monthly_year + ' CHF'">
								</div>
								<div class="col-md-6  bg info-ligth-3 p-2">
									TOTAL QUATERLY
								</div>
								<div class="col-md-3 p-2 tar" v-text="finalData.total_quaterly + ' CHF'">
								</div>
								<div class="col-md-3 p-2 tar" v-text="total_quaterly_year + ' CHF'">
								</div>
								<div class="col-md-6 bg info-ligth-3 p-2">
									TOTAL HALF-YEAR
								</div>
								<div class="col-md-3 p-2 tar" v-text="finalData.total_half_year + ' CHF'">
								</div>
								<div class="col-md-3 p-2 tar" v-text="total_half_year_year + ' CHF'">
								</div>
								<div class="col-md-6 bg info-ligth-3 p-2">
									TOTAL ANNUAL
								</div>
								<div class="col-md-6 p-2 tar" v-text="finalData.total_annual + ' CHF'">
								</div>
								<div class="col-md-6 bg section-1 p-2">
									TOTALS
								</div>
								<div class="col-md-6 p-2 bg info-ligth-3 tar">
									<strong v-text="finalData.total_period + ' CHF'"></strong>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</insurances-documents>