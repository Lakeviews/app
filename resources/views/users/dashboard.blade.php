@extends('layoutPage') 
@section('content')
<customer-dashboard inline-template :ticket-id="{{ isset($ticketId) ? $ticketId : 0 }}">
	<div class="content-page">
		<div class="container wrapper">
			<div class="content-2x" :class="slide">
				<section class="slide-1 dashboard">
					<div class="row">
						<div class="content col-sm-12">
							<h5 class="info-ligth mb-0 pb-05">DASHBOARD</h5>
							<p>@lang('client.customer.dashboard.subtitle') </p>
						</div>
					</div>
					<div class="row no-gutters">
						<div class="col-md-6 col-lg-3">
							<div class="card">
								<div class="card-body">
									<img class="card-img-top" src="images/dashboard/insurance.svg" alt="Insurance">
									<h6 class="card-title tac">@lang('client.customer.dashboard.insurances.title')</h6>
									<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
									<a href="#" @click="goto(0)" class="btn btn-info w-100">
										<i v-if="menu[0].loading" class="fa fa-gear faa-spin animated"></i>
										@lang('client.customer.dashboard.insurances.btn')
									</a>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-lg-3">
							<div class="card">
								<div class="card-body">
									<img class="card-img-top" src="images/dashboard/relocation.svg" alt="Insurance">
									<h6 class="card-title tac">@lang('client.customer.dashboard.relocation.title')</h6>
									<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
									<a href="#" @click="goto(1)" class="btn btn-info w-100">
										<i v-if="menu[1].loading" class="fa fa-gear faa-spin animated"></i>
										@lang('client.customer.dashboard.relocation.btn')</a>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-lg-3">
							<div class="card">
								<div class="card-body">
									<img class="card-img-top" src="images/dashboard/files.svg" alt="Insurance">
									<h6 class="card-title tac">@lang('client.customer.dashboard.storage.title')</h6>
									<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
									<a href="#" @click="goto(2)" class="btn btn-info w-100">
										<i v-if="menu[2].loading" class="fa fa-gear faa-spin animated"></i>
										@lang('client.customer.dashboard.storage.btn')</a>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-lg-3">
							<div class="card">
								<div class="card-body">
									<img class="card-img-top" src="images/dashboard/ticket.svg" alt="Insurance">
									<h6 class="card-title tac">@lang('client.customer.dashboard.tickets.title')</h6>
									<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
									<a href="#" @click="goto(3)" class="btn btn-info w-100">
										<i v-if="menu[3].loading" class="fa fa-gear faa-spin animated"></i>
										@lang('client.customer.dashboard.tickets.btn')</a>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="slide-2">
					<div id="template"></div>
				</section>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="container btn-actions">
					<a role="button" href="/profile" class="btn btn-primary btn-fa">
						<i class="fa fa-user"></i>
							@lang('client.btn.profile')</a>
					<button @click="newEmail('Any Question')" type="button" class="btn btn-info btn-fa">
						<i class="fa fa-envelope"></i>
						@lang('client.btn.send_email')</button>
					<button @click="showTicket()" type="button" v-if="currentMenuIndex != 3" class="btn btn-danger btn-fa">
								<i class="fa fa-comments-o" ></i>
								@lang('client.btn.create_ticket')</button>
				</div>
			</div>
		</div>
		
		<div class="row" v-show="showTicketForm">
			<div class="col">
				<div class="container">
					@include('tickets.ticket-chat')
				</div>
			</div>
		</div>
	</div>
</customer-dashboard>
@endsection