<storage inline-template :user="{{ auth()->user() }}">
	<div id="template">
		<div class="row">
			<div class="col-9">
				<h5 class="info-ligth mb-0 pb-05">STORAGE</h5>
				<p class="white">Here you upload your more important documents	.</p>
			</div>
			<div class="col-3">
				<button type="button" @click="dashboard()" class="btn btn-info float-right top--05">
					<i class="fa fa-chevron-left mr-02" aria-hidden="true"></i>
					BACK
				</button>
				
			</div>
			{{-- STORAGE --}}
			<div class="col-md-12">
				<div class="card primary">
					<div class="card-header">FILES</div>
					<div class="card-body">

						<form class="p-3">
							<div class="row">
								<div class="col-md-12">
									<fieldset class="form-group">
										<upload-control :description="true" :entity="'users/' + formUser.id" :ext="['.doc', '.docx', '.xls', '.xlsx', '.ppt', 'pptx', '.pdf', '.zip']" :size="4096" event="upda	te-user-files" update='update-child-user-files'></upload-control>
									</fieldset>
								</div>
								<div class="col-md-12 no-spaces">
									<button :disabled="loading" type="button" @click="save()" class="btn btn-primary float-right">
										<i v-if="loading" class="fa fa-gear faa-spin animated"></i>
										SAVE
									</button>	
									<button :disabled="loading" @click.prevent="dashboard()" type="buttom" class="btn btn-danger float-right mr-2">CANCEL</button>	
								</div>
							</div>
						</form>

					</div>
				</div>
			</div>

		</div>
	</div>
</storage>