	<div class="card">
		<div class="card-header bg section-1 uppercase">
				@lang('client.details') 
		</div>
		<div class="card-body">
			<form class="p-3" @submit.prevent="save()">
				<div class="row">
					<div class="col-md-4">
						<fieldset class="form-group">
							<label class="w-100" for="active">@lang('client.gender')</label>
							<div class="btn-group" data-toggle="buttons">
								<label   @click="formUser.gender = 1" class="btn btn-primary" :class="{ 'active' : formUser.gender == 1}">
										@lang('client.male')
								</label>
								<label @click="formUser.gender = 0" class="btn btn-danger" :class="{ 'active' : formUser.gender == 0}">
										@lang('client.female')
								</label>
							
							</div>
						</fieldset>
					</div>

					<div class="col-md-7 offset-md-1">	
						<fieldset class="form-group">
							<label for="name">@lang('client.birthdate')</label>

							<div class='input-group date' id='birthdate'>
								<input type='text' v-model="formUser.birthdate"  class="form-control" :class="{ 'is-invalid' : formUser.errors.has('birthdate') }"/>
								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
							</div>

							<div v-if="formUser.errors.has('birthdate')"  class="invalid-feedback" v-text="formUser.errors.get('birthdate')"></div>
						</fieldset>
					</div>

					<div class="col-md-6">	
						<fieldset class="form-group">
							<label for="mobile">@lang('client.mobile')</label>
							<input  type="text" name="mobile" class="form-control" v-model="formUser.mobile" />
						</fieldset>
					</div>

					<div class="col-md-6">	
						<fieldset class="form-group">
							<label for="phone">@lang('client.phone')</label>
							<input  type="text" name="phone" class="form-control" v-model="formUser.phone" />
						</fieldset>
					</div>


					<div class="col-md-12">	
						<fieldset class="form-group">
							<label for="address">@lang('client.address')</label>
							<input  type="text" name="address" class="form-control" v-model="formUser.address" />
						</fieldset>
					</div>

					<div class="col-md-3">	
						<fieldset class="form-group">
							<label for="address">@lang('client.town')</label>
							<input  type="text" name="town" class="form-control" v-model="formUser.town" />
						</fieldset>
					</div>

					<div class="col-md-3">	
						<fieldset class="form-group">
							<label for="postcode">@lang('client.postcode')</label>
							<input  type="text" name="postcode" class="form-control" v-model="formUser.postcode" />
						</fieldset>
					</div>

					<div class="col-md-6">	
						<fieldset class="form-group">	
							<label for="countries">@lang('client.country')	</label>	
							<select id="countries" @change="formUser.country = $event.target.selectedOptions[0].innerText" class="selectpicker"	 data-live-search="true">
								<option value="" selected disabled>@lang('client.select.text')</option>
								@include('partials.countries')
							</select>

						</fieldset>
					</div>

					@if(request()->path() == 'profile')
					<div class="col-md-12">
						<hr class="mt-3">
						<button :disabled="loading" type="button" @click="save()" class="btn btn-primary float-right">
							<i v-if="loading" class="fa fa-gear faa-spin animated"></i>
							@lang('client.btn.save') 
						</button>	
					</div>
					@endif

				</div>
			</form>
		</div>
	</div>