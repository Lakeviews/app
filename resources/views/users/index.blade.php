<users inline-template :roles="{{ auth()->user()->getRoles() }}">
	<section>
		<div class="slide-2x" :class="slide">
			{{-- SLIDE ONE --}}
			<div class="container-fluid slide-1">
				<div class="row">

					<div class="col-md-6">
						<h6 class="primary">USERS</h6>
						
					</div>
					<div class="col-md-6">

						<button type="button" @click="edit(table.model,true)" class="btn btn-primary float-right pot-05">
							
							<i v-if="table.model.loading" class="fa fa-gear faa-spin animated"></i>
							<i class="fa fa-plus mr-1"></i>
							CREATE NEW
						</button>
						<button @click="processDelete()"  type="button" v-if="trash.length > 0" class="btn btn-danger mr-2 pot-05 float-right">
							<i class="fa fa-trash"></i>
						</button>	
					</div>
				</div>
				<div class="row" v-if="table.paginate != null">
					<div class="col-md-12">


						<table class="table table-striped table-hover">
							<thead>
								<tr class="header">
									<td colspan="3"><h3>USERS MANAGEMENT </h3></td>
									<td colspan="2"></td>
									<td colspan="2">
										<div class="input-group">
											<input type="text" v-model="table.params.search" @keyup="table.bind()" class="form-control" placeholder="Search here">
											<span class="input-group-addon">
												<i class="fa fa-search"></i>
											</span>
										</div>
									</td>
								</tr>
								<tr>
									<th class="w3" scope="col">
										<input @click="selectetAll($event.target.checked)" type="checkbox">
									</th>
									<th  class="w4" @click="table.setSort('id')" :class="table.sorts['id']" scope="col">ID</th>
									<th  @click="table.setSort('name')" :class="table.sorts['name']" scope="col">Name</th>
									<th @click="table.setSort('email')" :class="table.sorts['email']" scope="col">Email</th>
									
									<th @click="table.setSort('type')" :class="table.sorts['type']" scope="col">Permission</th>
									<th @click="table.setSort('active')" :class="table.sorts['active']" scope="col">Status</th>
									<th class="actions" scope="col">Action</th>
								</tr>
							</thead>
							<tbody>	
								<tr v-for="(row, index) in table.paginate.data">
									<td scope="row"><input v-model="row.selected" type="checkbox"></td>
									<td scope="col" v-text="row.id"></td>
									<td scope="col" v-text="row.name"></td>
									<td scope="col" v-text="row.email"></td>
									
									<td scope="col">
										<span v-if="row.type ==1" class="badge badge-primary">ADMIN</span>
										<span v-if="row.type ==2" class="badge badge-success">SALES</span>
										<span v-if="row.type ==3" class="badge badge-warning">CUSTOMER</span>
									</td>	
									<td scope="col">
										<span v-if="row.active" class="badge badge-success">ON</span>
										<span v-if="!row.active" class="badge badge-danger">OFF</span>
									</td>	
									<td class="tac" scope="col">
										<button @click="edit(row, true)" type="button" class="btn btn-sm btn-primary">
											<i v-if="row.loading" class="fa fa-gear faa-spin animated"></i>
											<i class="fa fa-edit"></i>
										</button>
										<button @click="del(index)" type="button" class="btn btn-sm btn-danger	">
											<i class="fa fa-trash"></i>
										</button>
									</td>
								</tr>
							</tbody>
						</table>
						<nav class="float-right">
							<ul class="pagination">
								
								<li  class="page-item" :class="{ 'disabled' : table.paginate.current_page == 1}">
									<a class="page-link" @click="table.first()"  tabindex="-1">
										<i class="fa fa-angle-double-left" aria-hidden="true"></i>
									</a>
								</li>

								<li  class="page-item" :class="{ 'disabled' : table.paginate.current_page == 1}">
									<a class="page-link" @click="table.prev()"  tabindex="-1">
										<i class="fa fa-angle-left" aria-hidden="true"></i>
									</a>
								</li>

								<li class="page-item" :class="{'active': table.paginate.current_page == page}" v-for="page in table.pages()"  ><a class="page-link" @click="table.goto(page)"  v-text="page" tabindex="-1"></a></li>

								<li v-if="table.paginate.last_page > 2" class="page-item" :class="{'active': table.paginate.current_page == table.paginate.last_page }" >
									<a class="page-link" @click="table.last()" v-text=" table.paginate.last_page" tabindex="-1">
									</a>
								</li>

								<li  class="page-item" :class="{ 'disabled' : table.paginate.current_page == table.paginate.last_page }">
									<a class="page-link" @click="table.next()"  tabindex="-1">
										<i class="fa fa-angle-right" aria-hidden="true"></i>
									</a>
								</li>

								<li class="page-item" :class="{ 'disabled' : table.paginate.current_page == table.paginate.last_page }">
									<a  @click="table.last()" class="page-link"  tabindex="-1">
										<i class="fa fa-angle-double-right" aria-hidden="true"></i>
									</a>
								</li>
							</ul>
						</nav>

					</div>
				</div>
			</div>



			{{-- SLIDE TWO --}}
			<div v-if="formUser != null" class="container-fluid slide-2">

				<div class="row lb">

					<div class="col-md-6">
						<h6 class="primary"><span v-text="formUser.id != undefined ? 'EDITING' : 'NEW'"></span> USER</h6>
						<h2 class="pt-1 pb-1"><span v-text="formUser.name"></span>&nbsp; </h2>
					</div>
					<div class="col-md-6">
						<button type="button" :disabled="table.model.loading" @click="back()" class="btn btn-primary float-right">BACK</button>
					</div>
					<div class="col-md-12">
						<hr class="mt--05">	
					</div>


				</div>
				
				<div class="row">

					{{-- INFO --}}
					<div class="col-md-6 mb-3">
						@include('users.info')
					</div>

					{{-- DETAILS --}}
					<div class="col-md-6 mb-3">
						@include('users.details')
					</div>

					{{-- RELOCATION --}}
					<div class="col-md-6">
						<div class="card">
							<div class="card-header">RELOCATION</div>
							<div class="card-body">
								
								<form class="p-3">
									<div class="row">
										<div class="col-md-6">
											<fieldset class="form-group">
												<label class="w-100" for="active">Relocation</label>
												<div class="btn-group" data-toggle="buttons">
													<label   @click="formUser.relocation = 1" class="btn btn-default" :class="{ 'active' : formUser.relocation == 1}">
														ACTIVE
													</label>
													<label @click="formUser.relocation = 0" class="btn btn-default red" :class="{ 'active' : formUser.relocation == 0}">
														INACTIVE
													</label>
												</div>
											</fieldset>
										</div>

										<div v-show="formUser.relocation ==	 1" class="col-md-6">	
											<fieldset class="form-group" :class="{ 'has-danger' : formUser.errors.has('relocation_company_id') }">	
												<label for="companies">Company</label>	
												<select id="companies" onchange="formUser.relocation_company_id = this.value" class="selectpicker"	 data-live-search="true">
													<option value="" selected>Please select or None</option>
													@foreach($relCompanies as $company)
													<option value="{{ $company->id }}">{{ $company->name }}</option>
													@endforeach
												</select>
												<div v-if="formUser.errors.has('relocation_company_id')"  class="form-control-feedback" v-text="formUser.errors.get('relocation_company_id')"></div>
											</fieldset>
										</div>

										<div v-show="formUser.relocation == 1" class="col-md-12">
											<fieldset class="form-group">
												<label for="description">Comments</label>
												<textarea rows="3" maxlength="300"   v-model="formUser.relocation_comments" class="form-control" name="relocation_comments" placeholder="Enter a description"></textarea>
											</fieldset>
										</div>



										<div v-show="formUser.relocation == 1" class="col-md-12">
											<fieldset class="form-group">
												<upload-control :entity="'relocation/' + formUser.id" :ext="['.doc', '.docx', '.xls', '.xlsx', '.ppt', 'pptx', '.pdf','.jpg', '.jpeg','.png']" :size="4096" event="update-relocation-files" update='update-child-relocation-files'></upload-control>
											</fieldset>
										</div>
									</div>
								</form>
								
							</div>
						</div>
					</div>

					{{-- STORAGE --}}
					<div class="col-md-6">
						<div class="card">
							<div class="card-header">FILES</div>
							<div class="card-body">
								
								<form class="p-3">
									<div class="row">
										<div class="col-md-12">
											<fieldset class="form-group">
												<upload-control  :entity="'users/' + formUser.id" :ext="['.doc', '.docx', '.xls', '.xlsx', '.ppt', 'pptx', '.pdf','.jpg', '.jpeg','.png']" :size="4096" event="update-user-files" update='update-child-user-files'></upload-control>
											</fieldset>
										</div>
									</div>
								</form>
								
							</div>
						</div>
					</div>
				</div>
				<div>
					<div class="row">
						<div class="col-md-12">
							<hr class="mt-5">
							<button :disabled="table.model.loading" type="submit" @click="save()" class="btn btn-primary float-right">
								<i v-if="table.model.loading" class="fa fa-gear faa-spin animated"></i>
								SAVE
							</button>	
							<button :disabled="table.model.loading" @click="back()" type="buttom" class="btn btn-danger float-right mr-2">CANCEL</button>	
						</div>
					</div>
				</div>

			</div>
		</div>
	</section>
</users>
