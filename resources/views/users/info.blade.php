<div class="card">
	<div class="card-header bg section-1">
		INFO
	</div>
	<div class="card-body">
		<form class="p-3" @keypress="formUser.errors.clear($event.target.name)" @keypress.enter="save()">
			<fieldset class="form-group">
				<label for="name">@lang('client.form.name')</label>
				<input type="text" v-model="formUser.name" usautocomplete="off" class="form-control" name="name" placeholder="@lang('client.form.name.placeholder')"
				 :class="{ 'is-invalid' : formUser.errors.has('name') }">
				<div v-if="formUser.errors.has('name')" class="invalid-feedback" v-text="formUser.errors.get('name')"></div>
			</fieldset>
			<fieldset class="form-group">
				<label for="email">@lang('client.form.email')</label>
				<input type="text" v-model="formUser.email" class="form-control" autocomplete="off" name="email" placeholder="@lang('client.form.email.placeholder')"
				 :class="{ 'is-invalid' : formUser.errors.has('email') }">
				<div v-if="formUser.errors.has('email')" class="invalid-feedback" v-text="formUser.errors.get('email')"></div>
			</fieldset>
			<fieldset class="form-group" :class="{ 'is-invalid' : formUser.errors.has('password') }">
				<label for="password">@lang('client.form.password')</label>

				<div class="input-group" >
					<input :disabled="passwordDisabled" type="text" v-model="formUser.password" class="form-control" autocomplete="nope" maxlength="10"
					 name="password" placeholder="@lang('client.form.password.placeholder')" :class="{ 'is-invalid' : formUser.errors.has('password') }">
					<span v-if="formUser.id > 0"  class="input-group-addon primary pointer" @click="changePassword()" v-text="passwordDisabled ? 'Click to change' : 'Save new Password'">Click to change</span>
					<span  v-if="formUser.id > 0 && !passwordDisabled" class="input-group-addon pointer danger" @click="cancelPasswordChange()">Cancel</span>
					
				</div>
				<div v-if="formUser.errors.has('password')" class="invalid-feedback" v-text="formUser.errors.get('password')"></div>
			</fieldset>
			<fieldset class="form-group">
				<label for="email">Api Token</label>
				<input disabled="disabled" type="text" v-model="formUser.api_token" autocomplete="off" class="form-control" name="api_token">
			</fieldset>
			<div class="row">
				<div class="col-md-5">
					<fieldset class="form-group">
						<label class="w-100" for="active">Active</label>
						<div class="btn-group" data-toggle="buttons">
							@if(auth()->user()->isBackOffice())
							<button type="button" @click="formUser.active = 1" class="btn btn-default" :class="{ 'active' : formUser.active == 1}">
								ACTIVE
							</button>
							<button type="button" @click="formUser.active = 0" class="btn btn-default red" :class="{ 'active' : formUser.active == 0}">
								INACTIVE
							</button> @else
							<button type="button" disabled="disabled" @click="formUser.active = 1" class="btn btn-default" :class="{ 'active' : formUser.active == 1}">
								ACTIVE
							</button> @endif
						</div>
					</fieldset>
				</div>
				<div class="col-md-7">
					<fieldset class="form-group">
						<label class="w-100" for="type">Permission</label>
						<div class="btn-group btn-roles" data-toggle="buttons">
							@if(\Route::currentRouteName() =="profile") @foreach(auth()->user()->getRoles() as $role)
							<span class="badge badge-primary uppercase mr-2"> {{ $role }}</span> @endforeach @else
							<button type="button" @click="addRole(1)" class="btn btn-primary focus active" :class="[ hasRole(1,formUser.roles) ? 'on' : 'off']">
								ADMIN
							</button>
							<button type="button" @click="addRole(2)" class="btn btn-success  focus active" :class="[ hasRole(2,formUser.roles) ? 'on' : 'off']">
								SALES
							</button>
							<button type="button" @click="addRole(4)" class="btn btn-info  focus active" :class="[ hasRole(4,formUser.roles) ? 'on' : 'off']">
								CALL CENTER
							</button>
							<button type="button" @click="addRole(3)" class="btn btn-warning  focus active" :class="[ hasRole(3,formUser.roles) ? 'on' : 'off']">
								CUSTOMER
							</button> @endif
						</div>
					</fieldset>
				</div>
				@if(request()->path() == 'profile')
				<div class="col-md-12">
					<hr class="mt-3">
					<button :disabled="loading" type="button" @click="save()" class="btn btn-primary float-right">
							<i v-if="loading" class="fa fa-gear faa-spin animated"></i>
							@lang('client.btn.save') 
						</button>
				</div>
				@endif
			</div>
		</form>
	</div>
</div>