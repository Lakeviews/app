<relocation :user="{{ auth()->user() }}" inline-template>
	<div id="template">
		<div class="row">
			<div class="col-9">
				<h5 class="info-ligth mb-0 pb-05">RELOCATION</h5>
				<p class="white">Here you can consult your relocation status.</p>
			</div>
			<div class="col-3">
				<button type="button" @click="dashboard()" class="btn btn-info float-right top--05">
					<i class="fa fa-chevron-left mr-02" aria-hidden="true"></i>
					BACK
				</button>
			</div>
			<div class="col-md-6">
				<div class="card primary">
					<div class="card-header">RELOCATION</div>
					<div class="card-body">
						<h6 v-if="!formUser.relocation">Please submit a ticket in the dashboard to enable this service</h6>
						<form class="p-3" v-if="formUser.relocation">
							<div class="row">
							
								<div v-show="formUser.relocation ==	 1" class="col-md-12">	
									<fieldset class="form-group" :class="{ 'has-danger' : formUser.errors.has('relocation_company_id') }">	
										<label for="companies">Company</label>	
										<select disabled="disabled" id="companies" @change="formUser.relocation_company_id = $event.target.value" class="selectpicker"	 data-live-search="true">
											<option value="" selected>Please select or None</option>
											@foreach($relCompanies as $company)
											<option value="{{ $company->id }}">{{ $company->name }}</option>
											@endforeach
										</select>
										<div v-if="formUser.errors.has('relocation_company_id')"  class="form-control-feedback" v-text="formUser.errors.get('relocation_company_id')"></div>
									</fieldset>
								</div>

								<div v-show="formUser.relocation == 1" class="col-md-12">
									<fieldset class="form-group">
										<label for="description">Comments</label>
										<textarea disabled="true" rows="3" maxlength="300"   v-model="formUser.relocation_comments" class="form-control" name="relocation_comments" placeholder="Enter a description"></textarea>
									</fieldset>
								</div>
							</div>

							<div class="col-md-12 no-spaces">

								<button :disabled="loading" type="button" @click="save()" class="btn btn-primary float-right">
									<i v-if="loading" class="fa fa-gear faa-spin animated"></i>
									SAVE
								</button>	

								<button :disabled="loading" @click.prevent="dashboard()" type="buttom" class="btn btn-danger float-right mr-2">CANCEL</button>	

							</div>
						</form>

					</div>
				</div>
			</div>

			<div v-show="formUser.relocation == 1"  class="col-md-6">

				<div class="card primary">
					<div class="card-header">FILES</div>
					<div class="card-body">
						<fieldset class="form-group">
							<upload-control :entity="'relocation/' + formUser.id" :ext="['.doc', '.docx', '.xls', '.xlsx', '.ppt', 'pptx', '.pdf']" :size="4096" event="update-relocation-files" update='update-child-relocation-files'></upload-control>
						</fieldset>
					</div>
				</div>
			</div>


		</div>
	</div>
</relocation>