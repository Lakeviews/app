<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<link rel="icon" type="image/vnd.microsoft.icon"
	href="/favicon.ico" />
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="csrf-token" content="{{ csrf_token()}}">
	<title>LAKEVIEWS - Insurance and Wealth Advisors</title>
	{{-- STYLES --}}
	<link rel="stylesheet" type="text/css" href="/css/app.css">
</head>
<body>

	{{-- LOADING PAGE SECTION --}}
	@include('partials/loader-2')

	{{-- APP --}}
	<div id="app">
		<app inline-template :user="{{ auth()->check() ? auth()->user() : 0  }}" :page="{{ isset(request()->page) ? request()->page : -1 }}" flash="{{ session()->has('flash') ?  session()->get('flash') : '' }}">
			<div class="app">

				<div class="navbar-section page">
					<div class="row no-gutters">
						<div class="col-xs-12 col-sm-12 col-lg-10 offset-lg-1 nav-wrapper">
							@include('partials.navbar')
						</div>	
					</div>		
				</div>
				@include('partials.main-modal')
				@include('partials.notifications')
				<!-- MAIN  -->
				<section class="main-page">
					@yield('content')
				</section>


				<!-- FOOTER --> 
				@include('partials.footer')
			</div>
		</app>
	</div>

</body>
{{-- SCRIPTS --}}
<script type="text/javascript" src="/js/app.js"></script>

</html>
