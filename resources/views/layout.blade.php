<!doctype html>
<html lang="{{ app()->getLocale() }}" languages='@json(config(' app.locales '))'>
<head>
	<link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico" />
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{ csrf_token()}}">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>LAKEVIEWS - Insurance and Wealth Advisors</title>
	{{-- STYLES --}}
	<link rel="stylesheet" type="text/css" href="/css/app.css">
</head>
<body>
	{{-- LOADING PAGE SECTION --}}
	@include('partials/loader-2') {{-- APP --}}
	<div id="app">
		<app inline-template :user="{{ auth()->check() ? auth()->user() : 0  }}" :page="{{ isset(request()->page) ? request()->page : -1 }}"
		 flash="{{ session()->has('flash') ?  session()->get('flash') : '' }}">
			<div class="app">
				<div class="navbar-section">
					<div class="row no-gutters">
						<div class="col-xs-12 col-sm-12 col-lg-12 offset-xl-1 col-xl-10 nav-wrapper">
	@include('partials.navbar')
						</div>
					</div>
				</div>
	@include('partials/main-modal')
	@include('partials/notifications') {{-- MAIN --}}
				<section class="main">
					<div class="container">
						<div class="content col-sm-10 offset-sm-2">
							<h1 class="info"><strong>@lang('client.main.quote')</strong></h1>
							<h3 class="info mt-2"><strong>@lang('client.main.sub_quote')</strong></h3>
							<a @click="scrollNav(navBar[0])" class="btn btn-lg btn-info mt-2" role="button">@lang('client.main.btn_more')</a>
							<a @click="scrollNav(navBar[1])" class="btn btn-lg btn-primary mt-2" role="button">@lang('client.main.btn_insurances')</a>
						</div>
					</div>
				</section>
				
				@include('partials.'.app()->getLocale().'.main')
				
				{{-- UPPER --}}
				<div @click="scrollToTop(1000)" class="upper" :class="{'show' : upper }">
					<span class="fa fa-angle-up"></span>
				</div>
				{{-- FOOTER --}}
				@include('partials.footer')
				</div>
			</div>
		</app>
</body>
{{-- SCRIPTS --}}
<script type="text/javascript" src="/js/app.js"></script>
<script type="text/javascript">
	function initMap()
	{
		window.busi.$emit('load-map');
	}
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6dsRsXoxQ3kdoDhE7Vh7GtV2fhmNl65k&callback=initMap"
 type="text/javascript"></script>
</html>