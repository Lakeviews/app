<objects inline-template>
    <section>
        <div class="slide-2x" :class="slide">
            {{-- SLIDE ONE --}}
            <div class="container-fluid slide-1">
                <div class="row">
                    <div class="col-md-6">
                        <h6 class="primary">OBJECTS</h6>
                    </div>
                    <div class="col-md-6">
                        <button type="button" @click="edit(table.model,true)" class="btn btn-primary float-right pot-05">
                                
                                <i v-if="table.model.loading" class="fa fa-gear faa-spin animated"></i>
                                <i class="fa fa-plus mr-1"></i>
                                CREATE NEW
                            </button>
                        <button @click="del(-1)" type="button" v-if="trash.length > 0" class="btn btn-danger mr-2 pot-05 float-right">
                                <i class="fa fa-trash"></i>
                            </button>
                    </div>
                </div>
                <div class="row" v-if="table.paginate != null">
                    <div class="col-md-12">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr class="header">
                                    <td colspan="3">
                                        <h3>OBJECTS MANAGEMENT </h3>
                                    </td>
                                    <td colspan="1">
                                        <fieldset class="form-group">
                                            <select @change="setCategory($event)" class="selectpicker categories" data-live-search="true">
                                                <option value="" selected>Please select a Category</option>
                                                @foreach($categories as $category)
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                @endforeach
                                            </select>
                                        </fieldset>
                                        </tb>
                                        <td colspan="3">
                                            <div class="input-group">
                                                <input type="text" v-model="table.params.search" @keyup="table.bind()" class="form-control" placeholder="Search here">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-search"></i>
                                                </span>
                                            </div>
                                        </td>
                                </tr>
                                <tr>
                                    <th class="w3" scope="col">
                                        <input @click="selectetAll($event.target.checked)" type="checkbox">
                                    </th>
                                    <th class="w4" @click="table.setSort('id')" :class="table.sorts['id']" scope="col">ID</th>
                                    <th class="w-20" @click="table.setSort('name')" :class="table.sorts['name']" scope="col">Name</th>
                                    <th class="w-20" @click="table.setSort('alias')" :class="table.sorts['alias']" scope="col">Alias</th>
                                    <th class="w-20" @click="table.setSort('category_id')" :class="table.sorts['category_id']" scope="col">Category</th>
                                    <th class="w-10" @click="table.setSort('active')" :class="table.sorts['active']" scope="col">Status</th>
                                    <th class="actions" scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(row, index) in table.paginate.data">
                                    <td scope="row"><input v-model="row.selected" type="checkbox"></td>
                                    <td scope="col" v-text="row.id"></td>
                                    <td scope="col" class="w-30" v-text="row.name"></td>
                                    <td scope="col" class="w-15" v-text="row.alias"></td>
                                    <td scope="col" class="w-15" v-text="row.category.name"></td>
                                    <td scope="col">
                                        <span v-if="row.active" class="badge badge-success">ON</span>
                                        <span v-if="!row.active" class="badge badge-danger">OFF</span>
                                    </td>
                                    <td class="tac" scope="col">
                                        <button @click="edit(row, true)" type="button" class="btn btn-sm btn-primary">
                                                <i v-if="row.loading" class="fa fa-gear faa-spin animated"></i>
                                                <i class="fa fa-edit"></i>
                                            </button>
                                        <button @click="del(row.id)" type="button" class="btn btn-sm btn-danger	">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <nav class="float-right">
                            <ul class="pagination">
                                <li class="page-item" :class="{ 'disabled' : table.paginate.current_page == 1}">
                                    <a class="page-link" @click="table.first()" tabindex="-1">
                                            <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                                        </a>
                                </li>
                                <li class="page-item" :class="{ 'disabled' : table.paginate.current_page == 1}">
                                    <a class="page-link" @click="table.prev()" tabindex="-1">
                                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                                        </a>
                                </li>
                                <li class="page-item" :class="{'active': table.paginate.current_page == page}" v-for="page in table.pages()"><a class="page-link" @click="table.goto(page)" v-text="page" tabindex="-1"></a></li>
                                <li v-if="table.paginate.last_page > 2" class="page-item" :class="{'active': table.paginate.current_page == table.paginate.last_page }">
                                    <a class="page-link" @click="table.last()" v-text=" table.paginate.last_page" tabindex="-1">
                                        </a>
                                </li>
                                <li class="page-item" :class="{ 'disabled' : table.paginate.current_page == table.paginate.last_page }">
                                    <a class="page-link" @click="table.next()" tabindex="-1">
                                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                                        </a>
                                </li>
                                <li class="page-item" :class="{ 'disabled' : table.paginate.current_page == table.paginate.last_page }">
                                    <a @click="table.last()" class="page-link" tabindex="-1">
                                            <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                        </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            {{-- SLIDE TWO --}}
            <div v-if="formObject != null" class="container-fluid slide-2">
                <div class="row lb">
                    <div class="col-md-6">
                        <h6 class="primary"><span v-text="formObject.id != undefined ? 'EDITING' : 'NEW'"></span> CATEGORY</h6>
                        <h2 class="pt-1 pb-1"><span v-text="formObject.name"></span>&nbsp; </h2>
                    </div>
                    <div class="col-md-6">
                        <button type="button" :disabled="table.model.loading" @click="back()" class="btn btn-primary float-right">BACK</button>
                    </div>
                    <div class="col-md-12">
                        <hr class="mt--05">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                INFO
                            </div>
                            <div class="card-body">
                                <form class="p-3" @keypress="formObject.errors.clear($event.target.name)" @keypress.enter="save()">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <fieldset class="form-group">
                                                <label for="name">Name</label>
                                                <input type="text" v-model="formObject.name" class="form-control" name="name" placeholder="type the name" :class="{ 'is-invalid' : formObject.errors.has('name') }">
                                                <div v-if="formObject.errors.has('name')" class="invalid-feedback" v-text="formObject.errors.get('name')"></div>
                                            </fieldset>
                                        </div>
                                        <div class="col-md-6">
                                            <fieldset class="form-group">
                                                <label for="email">Alias</label>
                                                <input type="text" v-model="formObject.alias" class="form-control" name="alias" placeholder="type the alias" :class="{ 'is-invalid' : formObject.errors.has('alias') }">
                                                <div v-if="formObject.errors.has('alias')" class="invalid-feedback" v-text="formObject.errors.get('alias')"></div>
                                            </fieldset>
                                        </div>
                                        <div class="col-md-6">
                                            <fieldset class="form-group" :class="{ 'is-invalid' : formObject.errors.has('category_id') }">
                                                <label class="w-100" for="active">Category</label>
                                                <select v-model="formObject.category_id" class="selectpicker categories" data-live-search="true">
                                                                <option value="" selected>Please select a Category</option>
                                                                @foreach($categories as $category)
                                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                                @endforeach
                                                            </select>
                                                <div v-if="formObject.errors.has('category_id')" class="invalid-feedback" v-text="formObject.errors.get('category_id')"></div>
                                            </fieldset>
                                        </div>
                                        <div class="col-md-6">
                                            <fieldset class="form-group">
                                                <label class="w-100" for="active">Active</label>
                                                <div class="btn-group" data-toggle="buttons">
                                                    <label @click="formObject.active = 1" class="btn btn-default" :class="{ 'active' : formObject.active == 1}">
                                                            ACTIVE
                                                        </label>
                                                    <label @click="formObject.active = 0" class="btn btn-default" :class="{ 'active' : formObject.active == 0}">
                                                            INACTIVE
                                                        </label>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <hr class="mt-5">
                        <button :disabled="table.model.loading" type="submit" @click="save()" class="btn btn-primary float-right">
                                    <i v-if="table.model.loading" class="fa fa-gear faa-spin animated"></i>
                                    SAVE
                                </button>
                        <button :disabled="table.model.loading" @click="back()" type="buttom" class="btn btn-danger float-right mr-2">CANCEL</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</objects>