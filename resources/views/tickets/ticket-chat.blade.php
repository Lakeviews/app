<ticket inline-template :user="{{ auth()->user() }}">
	<div v-if="formTicket != null" class="ticket">
		<div class="row lb">
			<div class="col-7">
				<h5 class="info-ligth mb-0 pb-05">TICKET CHAT | <span v-if="formTicket.id > 0">@{{ 'T-' + formTicket.id }}</span></h5>
				<p class="white">@lang('client.ticket.quote') </p>
			</div>
			<div class="col-5">
				<button v-if="workflow == 'tickets'" type="button" :disabled="saving" @click="back()" class="btn btn-info float-right top--05">
				<i class="fa fa-chevron-left mr-02" aria-hidden="true"></i>
				@lang('client.btn.back')
			</button>
				<button type="button" v-if="workflow === 'dashboard' && (formTicket.id != undefined && formTicket.id > 0)" :disabled="saving"
				 @click="goToTicket()" class="btn btn-info float-right top--05">
				 @lang('client.customer.dashboard.tickets.btn')
				 <i class="fa fa-chevron-right ml-02" aria-hidden="true"></i>
			</button>
			</div>
			<div class="row">
				{{-- SEND MESSAGE --}}
				<div class="col-md-6">
					<div class="card">
						<div class="card-header bg section-1">
							INFO
							<span v-if="formTicket.status == 1" class="badge badge-info float-right">@lang('client.open')</span>
							<span v-if="formTicket.status == 0" class="badge badge-success float-right">@lang('client.closed')</span>
						</div>
						<div class="card-body">
							<form class="p-3" @keypress="formTicket.errors.clear($event.target.name)" @keypress.enter="save(false)" @submit.prevent="save(false)">
								<div class="row no-gutters">
									<div v-show="formTicket.id != null" class="col-md-12">
										<fieldset v-if="userType =='adviser'" class="form-group">
											<label class="w-100" for="active">Status</label>
											<div class="btn-group" data-toggle="buttons">
												<button type="button" @click="formTicket.status = 1" class="btn btn-default red uppercase" :class="{ 'active' : formTicket.status == 1}">
													@lang('client.open')
										</button>
												<button type="button" :disabled="formTicket.status ==0" @click="formTicket.status = 0" class="btn btn-default green uppercase" :class="{ 'active' : formTicket.status == 0}">
													@lang('client.closed')
										</button>
											</div>
										</fieldset>
										@if(isset($customers))
										<fieldset class="form-group" :class="{ 'is-invalid' : formTicket.errors.has('customer_id') }">
											<label for="customers">@lang('client.customer')</label>
											<select id="customers" :disabled="formTicket.status ==0" v-model="formTicket.customer_id" class="selectpicker" data-live-search="true"
											 @change="formTicket.errors.clear('customer_id')">
										<option value="" selected>Please select or None</option>
									
										@foreach($customers as $customer)
										<option  value="{{ $customer->id }}">{{ $customer->name }}</option>
										@endforeach
									</select>
											<div v-if="formTicket.errors.has('customer_id')" class="invalid-feedback" v-text="formTicket.errors.get('customer_id')"></div>
										</fieldset>
										@endif
										<fieldset v-show="formTicket.id ==-1 && workflow != 'dashboard'" class="form-group" :class="{ 'is-invalid' : formTicket.errors.has('product_id') }">
											<label for="products">@lang('client.product')</label>
											<select id="products" :disabled="formTicket.status ==0" v-model="formTicket.product_id" class="selectpicker" data-live-search="true"
											 @change="formTicket.errors.clear('product_id')">
										<option value="" selected>@lang('client.select.text')</option>
										@foreach($products as $product)
										<option data-type="{{ $product->type }}" value="{{ $product->id }}">{{ $product->full_name }}</option>
										@endforeach
									</select>
											<div v-if="formTicket.errors.has('product_id')" class="invalid-feedback" v-text="formTicket.errors.get('product_id')"></div>
										</fieldset>
									</div>
									<div v-show="formTicket.id != null" class="col-md-12">
										<fieldset class="form-group">
											<label for="title">@lang('client.title')</label>
											<input :disabled="formTicket.status ==0" type="text" v-model="formTicket.title" class="form-control" name="name" placeholder="@lang('client.form.title.placeholder')"
											 :class="{ 'is-invalid' : formTicket.errors.has('title') }">
											<div v-if="formTicket.errors.has('title')" class="invalid-feedback" v-text="formTicket.errors.get('title')"></div>
										</fieldset>
									</div>
									<div v-if="formTicket != null" class="col-md-12">
										<fieldset class="form-group">
											<label for="title">@lang('client.message')</label>
											<textarea rows="3" :disabled="formTicket.status ==0" v-model="formTicket.message.text" class="form-control" name="message"
											 placeholder="@lang('client.form.message.placeholder')" :class="{ 'is-invalid' : formTicket.errors.has('message.text') }"></textarea>
											<div v-if="formTicket.errors.has('message.text')" class="invalid-feedback" v-text="formTicket.errors.get('message.text')">
											</div>
										</fieldset>
									</div>
									<div v-if="formTicket != null" class="col-md-12">
										<fieldset class="form-group">
											<label for="title">@lang('client.attach_file')</label>
											<upload-control :entity="'tickets/' + formTicket.id" :ext="['.doc', '.docx', '.xls', '.xlsx', '.ppt', 'pptx', '.pdf']" :size="4096"
											 event="update-ticket-files" update='update-child-ticket-files'></upload-control>
										</fieldset>
									</div>
								</div>
								<hr>
								<div class="col-md-12 no-spaces">
									<button :disabled="saving || (userType=='customer' && formTicket.status ==0)" type="submit" class="btn btn-primary float-right">
								<i v-if="saving" class="fa fa-gear faa-spin animated"></i>
								@lang('client.btn.send')
							</button>
									<button :disabled="saving" @click="back()" type="button" class="btn btn-danger float-right mr-2">@lang('client.btn.cancel')</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				{{-- CHAT --}}
				<div class="col-md-6">
					<div class="card primary">
						<div class="card-header">
							<span class="badge badge-info">QUESTION</span>
							<span v-if="formTicket.product != null" class="badge badge-default" v-text="formTicket.product.name"></span>
							<h5 class="title-p0-pt05" v-text="formTicket.title"></h5>
						</div>
						<div v-if="formTicket.id > 0" class="card-body chat">
							<ul class="list-group list-group-flush">
								<li @mouseover="changeStatus(message, index)" v-for="(message, index) in formTicket.log" class="list-group-item">
									<span v-if="message.type=='customer'" class="badge badge-primary">
								@{{formTicket.customer.name}} <span>says:</span>
									</span>
									<span v-if="message.type=='adviser'" class="badge badge-dark float-right">
								@{{message.posted_by_name}} <span>says:</span>
									</span>
									<span v-if="message.type!=userType && message.status != 0" class="badge badge-danger ml-2 mr-2" :class="{'float-right' : message.type=='adviser' }">NEW</span>
									<span v-if="message.type == userType" class="badge ml-2 mr-2" :class="{'float-right' : message.type=='adviser', 'badge-success': message.status == 0, 'badge-warning': message.status != 0 }"
									 v-text="(parseInt(message.status) != 0) ? 'NOT READED' : 'READ'"></span>
									<br>
									<div class="mt-1 text-dark" :class="{ 'tar' : message.type=='adviser' }" v-text="message.text"></div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</ticket>