<ticket-system inline-template :user ="{{ auth()->user() }}">
	<section>
		<div class="slide-2x" :class="slide">
			{{-- SLIDE ONE --}}
			<div class="container-fluid slide-1">
				<div class="row">

					<div class="col-md-6">
						<h6 class="primary">TICKETS</h6>
						
					</div>
					<div class="col-md-6">

						<button type="button" @click="edit(table.model,true)" class="btn btn-primary float-right pot-05">
							
							<i v-if="table.model.loading" class="fa fa-gear faa-spin animated"></i>
							<i class="fa fa-plus mr-1"></i>
							CREATE NEW
						</button>
						<button @click="processDelete()"  type="button" v-if="trash.length > 0" class="btn btn-danger mr-2 pot-05 float-right">
							<i class="fa fa-trash"></i>
						</button>	
					</div>
				</div>
				<div class="row" v-if="table.paginate != null">
					<div class="col-md-12">

						<table class="table table-striped table-hover">
							<thead>
								<tr class="header">
									<td colspan="3"></td>

									<td colspan="5">
										<div class="input-group">
											<input type="text" v-model="table.params.search" @keyup="table.bind()" class="form-control" placeholder="Search here">
											<span class="input-group-addon">
												<i class="fa fa-search"></i>
											</span>
										</div>
									</td>
								</tr>
								<tr>
									<th class="w3" scope="col">
										<input @click="selectetAll($event.target.checked)" type="checkbox">
									</th>
									<th  class="w4" @click="table.setSort('id')" :class="table.sorts['id']" scope="col">ID</th>
									<th  class="w-15" @click="table.setSort('customer_id')" :class="table.sorts['id']" scope="col">Name</th>
									<th class="w-25" @click="table.setSort('title')" :class="table.sorts['title']" scope="col">TITLE</th>
									<th class="w-15" @click="table.setSort('product_id')" :class="table.sorts['product_id']" scope="col">PRODUCT</th>
									<th class="w-15" @click="table.setSort('updated_at')" :class="table.sorts['updated_at']" scope="col">LAST UPDATE</th>
									<th class="w10" @click="table.setSort('status')" :class="table.sorts['status']" scope="col">Status</th>
									<th class="actions w10 tac" scope="col">Action</th>
								</tr>
							</thead>
							<tbody>	
								<tr v-for="(row, index) in table.paginate.data">
									<td scope="row"><input v-model="row.selected" type="checkbox"></td>
									<td scope="col" v-text="row.id"></td>
									<td scope="col" v-text="row.customer.name"></td>
									<td scope="col" v-text="row.title"></td>
									<td scope="col" v-text="row.product.name"></td>
									<td scope="col" v-text="row.updated_at"></td>

									<td scope="col">
										<span v-if="isNew(row.log)"  class="badge badge-danger">NEW</span>
										<span v-if="row.status == 1" class="badge badge-info">Open</span>
										<span v-if="row.status == 0" class="badge badge-success">Closed</span>
									</td>		

									<td class="tac" scope="col">
										<button @click="edit(row, true)" type="button" class="btn btn-sm btn-primary">
											<i v-if="row.loading" class="fa fa-gear faa-spin animated"></i>
											<i class="fa fa-edit"></i>
										</button>
										<button @click="del(index)" type="button" class="btn btn-sm btn-danger	">
											<i class="fa fa-trash"></i>
										</button>
									</td>
								</tr>
							</tbody>
						</table>
						<nav class="float-right">
							<ul class="pagination">
								
								<li  class="page-item" :class="{ 'disabled' : table.paginate.current_page == 1}">
									<a class="page-link" @click="table.first()"  tabindex="-1">
										<i class="fa fa-angle-double-left" aria-hidden="true"></i>
									</a>
								</li>

								<li  class="page-item" :class="{ 'disabled' : table.paginate.current_page == 1}">
									<a class="page-link" @click="table.prev()"  tabindex="-1">
										<i class="fa fa-angle-left" aria-hidden="true"></i>
									</a>
								</li>

								<li class="page-item" :class="{'active': table.paginate.current_page == page}" v-for="page in table.pages()"  ><a class="page-link" @click="table.goto(page)"  v-text="page" tabindex="-1"></a></li>

								<li v-if="table.paginate.last_page > 2" class="page-item" :class="{'active': table.paginate.current_page == table.paginate.last_page }" >
									<a class="page-link" @click="table.last()" v-text=" table.paginate.last_page" tabindex="-1">
									</a>
								</li>

								<li  class="page-item" :class="{ 'disabled' : table.paginate.current_page == table.paginate.last_page }">
									<a class="page-link" @click="table.next()"  tabindex="-1">
										<i class="fa fa-angle-right" aria-hidden="true"></i>
									</a>
								</li>

								<li class="page-item" :class="{ 'disabled' : table.paginate.current_page == table.paginate.last_page }">
									<a  @click="table.last()" class="page-link"  tabindex="-1">
										<i class="fa fa-angle-double-right" aria-hidden="true"></i>
									</a>
								</li>
							</ul>
						</nav>

					</div>
				</div>
			</div>



			{{-- SLIDE TWO --}}
			<div class="container-fluid slide-2">
				@include('tickets.ticket-chat')
			</div>
		</div>
	</section>
</ticket-system>
