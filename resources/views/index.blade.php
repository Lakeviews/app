@extends('layoutPage')

@section('content')
    <section class="who-we-are">
                <div class="container">
                    <div class="row">

                        <div class="content col-md-7">
                            <h5 class="info">WHO WE ARE</h5>
                            <p>We are an independent company active in the services and councils in Insurance and Patrimonial Management. </p>
                            <br>
                            <p>
                            Our mission is to assist you and advise you to achieve your goals in all our areas of expertise..</p>
                            <br>
                            <p>
                                We work with several companies. So we can choose which one is best for your situation. It is the privilege of being independent: to offer you the choice.
                            </p>


                            <blockquote class="blockquote">
                                <i>Lorem ipsum dolor sit amet. </i>&nbsp;&nbsp;&nbsp;<strong>Thomas Spycher&nbsp;&nbsp;|&nbsp;&nbsp;CEO</strong>
                            </blockquote>
                        </div>

                        <div class="col-md-4 offset-md-1 image">
                            <img id="sir" src="/images/who-we-are.svg" alt="who we are"/>
                        </div>
                    </div>
                </div>
            </section>
@endsection