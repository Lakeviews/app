<!doctype html>
<html>
<head>
	<link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico" />
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="csrf-token" content="{{ csrf_token()}}">
	<title>LAKEVIEWS - Insurance and Wealth Advisors</title>
</head>
<body>
	<style>
		@page {
			size: a4;
			margin: 15px;
			padding: 15px; // you can set margin and padding 0 
		}
		body {
			font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
			font-size: 12px;

		}
		.comments {
			background-color: #a5e1ff;
			padding: 15px 10px;
		}
		table {
			width: 100%;
		}
		.badge {
			display: inline-block;
			padding: 0.25em 0.4em;
			font-size: 75%;
			font-weight: 700;
			line-height: 1;
			text-align: center;
			white-space: nowrap;
			vertical-align: baseline;
			border-radius: 0.25rem;
		}
		.badge:empty {
			display: none;
		}
		.btn .badge {
			position: relative;
			top: -1px;
		}
		.badge-pill {
			padding-right: 0.6em;
			padding-left: 0.6em;
			border-radius: 10rem;
		}
		.badge-primary {
			color: #fff;
			background-color: #007bff;
		}
		.badge-primary[href]:focus,
		.badge-primary[href]:hover {
			color: #fff;
			text-decoration: none;
			background-color: #0062cc;
		}
		.badge-secondary {
			color: #fff;
			background-color: #868e96;
		}
		.badge-secondary[href]:focus,
		.badge-secondary[href]:hover {
			color: #fff;
			text-decoration: none;
			background-color: #6c757d;
		}
		.badge-success {
			color: #fff;
			background-color: #28a745;
		}
		.badge-success[href]:focus,
		.badge-success[href]:hover {
			color: #fff;
			text-decoration: none;
			background-color: #1e7e34;
		}
		.badge-info {
			color: #fff;
			background-color: #17a2b8;
		}
		.badge-info[href]:focus,
		.badge-info[href]:hover {
			color: #fff;
			text-decoration: none;
			background-color: #117a8b;
		}
		.badge-warning {
			color: #111;
			background-color: #ffc107;
		}
		.badge-warning[href]:focus,
		.badge-warning[href]:hover {
			color: #111;
			text-decoration: none;
			background-color: #d39e00;
		}
		.badge-danger {
			color: #fff;
			background-color: #dc3545;
		}
		.badge-danger[href]:focus,
		.badge-danger[href]:hover {
			color: #fff;
			text-decoration: none;
			background-color: #bd2130;
		}
		.badge-light {
			color: #111;
			background-color: #f8f9fa;
		}
		.badge-light[href]:focus,
		.badge-light[href]:hover {
			color: #111;
			text-decoration: none;
			background-color: #dae0e5;
		}
		.badge-dark {
			color: #fff;
			background-color: #343a40;
		}
		.badge-dark[href]:focus,
		.badge-dark[href]:hover {
			color: #fff;
			text-decoration: none;
			background-color: #1d2124;
		}
		table td {
			padding: 8px 10px;
		}
	</style>
	@yield('content')
</body>
<head>