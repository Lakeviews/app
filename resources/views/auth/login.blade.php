@extends('layoutPage') 
@section('content')
<auth inline-template>
	<div class="content-page">
		<div class="container auth-control">
			<div class="row">
				<div class="content offset-lg-3 col-lg-6 auth-control wrapper">
					<div class="content-2x content-card" :class="slide">
						<section class="slide-1">
							<div class="card primary">
								<div class="card-header">
									<h5 class="info-ligth-2	 no-margin">LOGIN</h5>
									<p class="card-text">
										@lang('client.login.title')
									</p>
								</div>
								<form @submit.prevent="" @keypress.enter="authenticate" @keypress="formLogin.errors.clear($event.target.name)">
									<div class="card-body">
										<fieldset class="form-group">
											<label>@lang('client.form.email')</label>
											<input type="email" name="email" v-model="formLogin.email" class="form-control" :class="{ 'is-invalid' : formLogin.errors.has('email') }"
											 placeholder="@lang('client.form.email.placeholder')">
											<div v-if="formLogin.errors.has('email')" class="invalid-feedback" v-text="formLogin.errors.get('email')"></div>
										</fieldset>
										<fieldset class="form-group">
											<label>@lang('client.form.password')</label>
											<input type="password" maxlength="10" name="password" v-model="formLogin.password" class="form-control" :class="{ 'is-invalid' : formLogin.errors.has('password') }"
											 placeholder="@lang('client.form.password.placeholder')">
											<div v-if="formLogin.errors.has('password')" class="invalid-feedback" v-text="formLogin.errors.get('password')"></div>
										</fieldset>
								</form>
								<form style="display:none" id="remember" action="/remember" method="post">
									{!! csrf_field() !!}
									<input type="hidden" name="route" value="route">
									<input name="email" v-model="formLogin.email">
									<input name="email" v-model="formLogin.password" type="password">
									<input value="Login" type="submit">
								</form>
								</div>
								<div class="card-body controls">
									<button :disabled="loading" @click="authenticate" type="submit" class="btn btn-primary float-right">
										<i v-if="loading" class="fa fa-gear faa-spin animated"></i>
										@lang('client.btn.authenticate')
									</button>
									<button @click="recovery()" type="button" class="btn btn-danger btn-password float-right mr-3">@lang('client.btn.password_recover')</button>
								</div>
							</div>
						</section>
						<section class="slide-2">
							<div class="card primary">
								<div class="card-header">
									<h5 class="info-ligth-2	 no-margin">@lang('client.btn.password_recover')</h5>
									<p class="card-text">@lang('client.password_recover.quote')</p>
								</div>
								<div class="card-body">
									<form @keypress.enter="recover()" @keypress="formRecover.errors.clear($event.target.name)" @submit.prevent="">
										<fieldset class="form-group">
											<label>@lang('client.form.email')</label>
											<input type="email" name="email" v-model="formRecover.email" class="form-control" :class="{ 'is-invalid' : formRecover.errors.has('email') }"
											 placeholder="@lang('client.form.email.placeholder')">
											<div v-if="formRecover.errors.has('email')" class="invalid-feedback" v-text="formRecover.errors.get('email')"></div>
										</fieldset>
									</form>
								</div>
								<div class="card-body controls">
									<button :disabled="loading" @click="recover()" type="button" class="btn btn-primary float-right">
										<i v-if="loading" class="fa fa-gear faa-spin animated"></i>
										@lang('client.btn.recover')</button>
									<button @click="back()" type="button" class="btn btn-danger btn-password float-right mr-3">@lang('client.btn.cancel')</button>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>
	</div>
</auth>
@endsection