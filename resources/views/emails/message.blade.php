@component('mail::message')
# {{ $message['title'] }}


<h3>NAME</h3>
<p>{{ $message['name'] }}</p>

<h3>EMAIL</h3>
<p>{{ $message['email'] }}</p>

<h3>PHONE</h3>
<p>{{ $message['phone'] }}</p>

<h3>MESSAGE</h3>
<p>{{ $message['message'] }}</p>


Thanks,<br>
{{ config('app.name') }}
@endcomponent
