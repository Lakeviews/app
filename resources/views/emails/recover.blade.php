@component('mail::message')
# {{ 'Hi, '.$user['name'] }}
<h2>You request a password change..</h2>
<p>To change your password click in link bellow:</p>
@component('mail::button', ['url' => env('APP_URL').'/recover/'.$user['api_token']])
CHANGE PASSWORD
@endcomponent
Thanks,<br>
{{ config('app.name') }}
@endcomponent
