@component('mail::message')
# Hi, you have new messages

Please click the link bellow to check the message

@component('mail::button',  ['url' => env('APP_URL').'/office/'.$user['api_token']])
GO TO ADMINISTRATION
@endcomponent

Thanks,
<br>
{{ config('app.name') }}
@endcomponent
