<div class="card">
    <div class="card-header">
        DETAILS
    </div>
    <div class="card-body">
        <form class="p-3" @keypress="formClient.errors.clear($event.target.name)" @keypress.enter="save()">
            <div class="row">
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <label for="latitude">Latitude</label>
                        <input type="text" name="latitude" class="form-control" v-model="formClient.latitude" />
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <label for="postcode">Longitude</label>
                        <input type="text" name="longitude" class="form-control" v-model="formClient.longitude" />
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <label for="mobile">Phone</label>
                        <input type="text" name="phone" class="form-control" v-model="formClient.phone" :class="{ 'is-invalid' : formClient.errors.has('phone') }"
                        />
                        <div v-if="formClient.errors.has('phone')" class="invalid-feedback" v-text="formClient.errors.get('phone')"></div>
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <label for="phone_2">Phone 2</label>
                        <input type="text" name="phone_2" class="form-control" v-model="formClient.phone_2" />
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <label for="fax">Fax</label>
                        <input type="text" name="fax" class="form-control" v-model="formClient.fax" />
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <label for="mobile">Mobile</label>
                        <input type="text" name="mobile" class="form-control" v-model="formClient.mobile" />
                    </fieldset>
                </div>
                <div class="col-md-12">
                    <fieldset class="form-group">
                        <label for="address">Address</label>
                        <input type="text" name="address" class="form-control" v-model="formClient.address" :class="{ 'is-invalid' : formClient.errors.has('address') }"
                        />
                        <div v-if="formClient.errors.has('address')" class="invalid-feedback" v-text="formClient.errors.get('address')"></div>
                    </fieldset>
                </div>
                <div class="col-md-3">
                    <fieldset class="form-group">
                        <label for="address">Town</label>
                        <input type="text" name="town" class="form-control" v-model="formClient.town" :class="{ 'is-invalid' : formClient.errors.has('town') }"
                        />
                        <div v-if="formClient.errors.has('town')" class="invalid-feedback" v-text="formClient.errors.get('town')"></div>
                    </fieldset>
                </div>
                <div class="col-md-3">
                    <fieldset class="form-group">
                        <label for="postcode">Postcode</label>
                        <input type="text" name="postcode" class="form-control" v-model="formClient.postcode" :class="{ 'is-invalid' : formClient.errors.has('postcode') }"
                        />
                        <div v-if="formClient.errors.has('postcode')" class="invalid-feedback" v-text="formClient.errors.get('postcode')"></div>
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset class="form-group" :class="{ 'is-invalid' : formClient.errors.has('country') }">
                        <label for="countries">Country  </label>
                        <select id="countries" name="country" @change="formClient.country = $event.target.selectedOptions[0].innerText" class="selectpicker"
                            data-live-search="true">
                            <option value="" selected disabled>Please select</option>
                            @include('partials.countries')
                        </select>
                        <div v-if="formClient.errors.has('country')" class="invalid-feedback" v-text="formClient.errors.get('country')"></div>
                    </fieldset>
                </div>
            </div>
        </form>
    </div>
</div>