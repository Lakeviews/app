<clients inline-template>
    <section>
        <div class="slide-2x" :class="slide">
            {{-- SLIDE ONE --}}
            <div class="container-fluid slide-1">
                <div class="row">
                    <div class="col-md-6">
                        <h6 class="primary">CLIENTS</h6>
                    </div>
                    <div class="col-md-6">
                        <button type="button" @click="edit(table.model,true)" class="btn btn-primary float-right pot-05">
							
							<i v-if="table.model.loading" class="fa fa-gear faa-spin animated"></i>
							<i class="fa fa-plus mr-1"></i>
							CREATE NEW
						</button>
                        <button @click="processDelete()" type="button" v-if="trash.length > 0" class="btn btn-danger mr-2 pot-05 float-right">
							<i class="fa fa-trash"></i>
						</button>
                    </div>
                </div>
                <div class="row" v-if="table.paginate != null">
                    <div class="col-md-12">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr class="header">
                                    <td colspan="3">
                                        <h3>CLIENTS MANAGEMENT </h3>
                                    </td>
                                    <td colspan="2">
                                        <fieldset class="form-group">
                                            <select @change="changeCompanyType($event)" class="selectpicker company_types" data-live-search="true">
                                                    <option value="" selected>Please select a Sector</option>
                                                    @foreach($CompanyTypes as $type)
                                                        <option value="{{ $type->id }}">{{ $type->name }}</option>
                                                    @endforeach
                                                </select>
                                        </fieldset>
                                    </td>
                                    <td colspan="2">
                                        <div class="input-group">
                                            <input type="text" v-model="table.params.search" @keyup="table.bind()" class="form-control" placeholder="Search here">
                                            <span class="input-group-addon">
												<i class="fa fa-search"></i>
											</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="w3" scope="col">
                                        <input @click="selectetAll($event.target.checked)" type="checkbox">
                                    </th>
                                    <th class="w4" @click="table.setSort('id')" :class="table.sorts['id']" scope="col">ID</th>
                                    <th @click="table.setSort('name')" :class="table.sorts['name']" scope="col">Name</th>
                                    <th @click="table.setSort('type')" :class="table.sorts['type']" scope="col">Type</th>
                                    <th @click="table.setSort('company')" :class="table.sorts['company']" scope="col">Sector</th>
                                    <th @click="table.setSort('active')" :class="table.sorts['active']" scope="col">Status</th>
                                    <th class="actions" scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(row, index) in table.paginate.data">
                                    <td scope="row"><input v-model="row.selected" type="checkbox"></td>
                                    <td scope="col" v-text="row.id"></td>
                                    <td scope="col" v-text="row.name"></td>
                                    <td scope="col">
                                        <span v-if="row.type==1" class="badge badge-primary">COMPANY</span>
                                        <span v-if="row.type==2" class="badge badge-secondary">PRIVATE</span>
                                    </td>
                                    <td scope="col" v-text="row.company.name"></td>
                                    <td scope="col">
                                        <span v-if="row.active" class="badge badge-success">ON</span>
                                        <span v-if="!row.active" class="badge badge-danger">OFF</span>
                                    </td>
                                    <td class="tac" scope="col">
                                        <button @click="edit(row, true)" type="button" class="btn btn-sm btn-primary">
											<i v-if="row.loading" class="fa fa-gear faa-spin animated"></i>
											<i class="fa fa-edit"></i>
                                        </button>
                                 
                                        <button @click="del(index)" type="button" class="btn btn-sm btn-danger">
											<i class="fa fa-trash"></i>
                                        </button>
                                        
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <nav class="float-right">
                            <ul class="pagination">
                                <li class="page-item" :class="{ 'disabled' : table.paginate.current_page == 1}">
                                    <a class="page-link" @click="table.first()" tabindex="-1">
										<i class="fa fa-angle-double-left" aria-hidden="true"></i>
									</a>
                                </li>
                                <li class="page-item" :class="{ 'disabled' : table.paginate.current_page == 1}">
                                    <a class="page-link" @click="table.prev()" tabindex="-1">
										<i class="fa fa-angle-left" aria-hidden="true"></i>
									</a>
                                </li>
                                <li class="page-item" :class="{'active': table.paginate.current_page == page}" v-for="page in table.pages()"><a class="page-link" @click="table.goto(page)" v-text="page" tabindex="-1"></a></li>
                                <li v-if="table.paginate.last_page > 2" class="page-item" :class="{'active': table.paginate.current_page == table.paginate.last_page }">
                                    <a class="page-link" @click="table.last()" v-text=" table.paginate.last_page" tabindex="-1">
									</a>
                                </li>
                                <li class="page-item" :class="{ 'disabled' : table.paginate.current_page == table.paginate.last_page }">
                                    <a class="page-link" @click="table.next()" tabindex="-1">
										<i class="fa fa-angle-right" aria-hidden="true"></i>
									</a>
                                </li>
                                <li class="page-item" :class="{ 'disabled' : table.paginate.current_page == table.paginate.last_page }">
                                    <a @click="table.last()" class="page-link" tabindex="-1">
										<i class="fa fa-angle-double-right" aria-hidden="true"></i>
									</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            {{-- SLIDE TWO --}}
            <div v-if="formClient != null" class="container-fluid slide-2">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="nav nav-tabs" id="primayTabs" role="tablist">
                            <li @click="tab=0" class="nav-item">
                                <a @click="tab=0" class="nav-link active" id="calls-tab" data-toggle="tab" href="#client" role="tab" aria-controls="calls-tab"
                                    aria-selected="true">CLIENT</a>
                            </li>
                            <li @click="tab=1" class="nav-item">
                                <a @click="tab=1" class="nav-link" id="segments-tab" data-toggle="tab" href="#segments" role="tab" aria-controls="segments-tab"
                                    aria-selected="false">SEGMENTS</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="client-data">
                            <div class="tab-pane fade show active" id="client" role="tabpanel" aria-labelledby="home-tab">
                                <div class="row">
                               
                                    <div class="col-md-12">
                                        <button type="button" :disabled="table.model.loading" @click="back()" class="btn btn-primary float-right mt--3">BACK</button>
                                    </div>
                               
                                    <div class="col-lg-6">
    @include('clients.info')
                                    </div>
                                    <div class="col-lg-6">
    @include('clients.details')
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <hr class="mt-5">
                                        <button :disabled="table.model.loading" type="submit" @click="save()" class="btn btn-primary float-right">
                                                                <i v-if="table.model.loading" class="fa fa-gear faa-spin animated"></i>
                                                                SAVE
                                                            </button>
                                        <button :disabled="table.model.loading" @click="back()" type="buttom" class="btn btn-danger float-right mr-2">CANCEL</button>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" style="overflow: hidden" id="segments" role="tabpanel" aria-labelledby="profile-tab">
                                <segments inline-template>
                                    <section>
                                        <div class="slide-2x full" :class="slide">
                                            {{-- SLIDE ONE --}}
                                            <div class="container-fluid slide-1">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <ul class="nav nav-tabs" id="primayTabs" role="tablist">
                                                            <li @click="tab=0" class="nav-item">
                                                                <a @click="tab=0" class="nav-link active" id="calls-tab" data-toggle="tab" href="#calls" role="tab" aria-controls="calls-tab"
                                                                    aria-selected="true">MY CALLS</a>
                                                            </li>
                                                            <li @click="tab=1" class="nav-item">
                                                                <a @click="tab=1" class="nav-link" id="segments-tab" data-toggle="tab" href="#segments2" role="tab" aria-controls="segments-tab"
                                                                    aria-selected="false">SEGEMENTS</a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content" id="primayTabsContent">
                                                            <div class="tab-pane fade show active" id="calls" role="tabpanel" aria-labelledby="home-tab">
    @include('segments.calls')
                                                            </div>
                                                            <div class="tab-pane fade" id="segments2" role="tabpanel" aria-labelledby="profile-tab">
    @include('segments.table')
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- SLIDE TWO --}}
                                            <div class="container-fluid slide-2">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <ul class="nav nav-tabs" id="segmentTabs" role="tablist">
                                                            <li @click="info=1" class="nav-item">
                                                                <a class="nav-link active" id="client-segment-tab" data-toggle="tab" href="#segment" role="tab" aria-controls="calls-tab"
                                                                    aria-selected="true">CLIENT SEGMENT</a>
                                                            </li>
                                                            <li @click="info=0" class="nav-item">
                                                                <a class="nav-link" id="client-segment-call-list" data-toggle="tab" href="#list" role="tab" aria-controls="segments-tab"
                                                                    aria-selected="false">CALL LIST</a>
                                                            </li>
                                                        </ul><button type="button" :disabled="table.model.loading" @click="back()"
                                                            class="btn btn-primary btn-back-abs">BACK</button>
                                                        <div class="tab-content" id="primayTabsContent">
                                                            <div class="tab-pane fade show active" id="segment" role="tabpanel" aria-labelledby="home-tab">
    @include('segments.info')
                                                            </div>
                                                            <div class="tab-pane fade" id="list" role="tabpanel" aria-labelledby="profile-tab">
                                                                <contact-list :entity="formSegment"></contact-list>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div v-if="info== 0" class="row">
                                                    <div class="col-md-12">
                                                        <hr class="mt-5">
                                                        <button :disabled="table.model.loading" type="submit" @click="save()" class="btn btn-primary float-right">
                                                                        <i v-if="table.model.loading" class="fa fa-gear faa-spin animated"></i>
                                                                        SAVE
                                                                    </button>
                                                        <button :disabled="table.model.loading" @click="back()" type="buttom" class="btn btn-danger float-right mr-2">CANCEL</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </segments>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</clients>