<div class="card">
    <div class="card-header">
        INFO
    </div>
    <div class="card-body">
        <form class="p-3" @keypress="formClient.errors.clear($event.target.name)" @keypress.enter="save()">
            <div class="row">
                <div class="col-lg-7">
                    <fieldset class="form-group">
                        <label for="name">Name</label>
                        <input type="text" v-model="formClient.name" class="form-control" name="name" placeholder="type the name" :class="{ 'is-invalid' : formClient.errors.has('name') }">
                        <div v-if="formClient.errors.has('name')" class="invalid-feedback" v-text="formClient.errors.get('name')"></div>
                    </fieldset>
                </div>
                <div class="col-lg-5">
                    <fieldset class="form-group">
                        <label for="name">Number</label>
                        <input type="text" v-model="formClient.number" class="form-control" name="name" placeholder="type the number" :class="{ 'is-invalid' : formClient.errors.has('number') }">
                        <div v-if="formClient.errors.has('number')" class="invalid-feedback" v-text="formClient.errors.get('number')"></div>
                    </fieldset>
                </div>
                <div class="col-md-12">
                    <fieldset class="form-group" :class="{ 'is-invalid' : formClient.errors.has('company_type_id') }">
                        <label class="w-100" for="active">Sector</label>
                        <select v-model="formClient.company_type_id" class="selectpicker company_types" data-live-search="true">
                            <option value="" selected>Please select a Sector</option>
                            @foreach($CompanyTypes as $type)
                                <option value="{{ $type->id }}">{{ $type->name }}</option>
                            @endforeach
                        </select>
                        <div v-if="formClient.errors.has('company_type_id')" class="invalid-feedback" v-text="formClient.errors.get('company_type_id')"></div>
                    </fieldset>
                </div>
                <div class="col-lg-6">
                    <fieldset class="form-group">
                        <label class="w-100" for="type">Type</label>
                        <div class="btn-group" data-toggle="buttons">
                            <label @click="formClient.type = 1" class="btn btn-default" :class="{ 'active' : formClient.type == 1}">
                                         COMPANY
                                        </label>
                            <label @click="formClient.type = 0" class="btn btn-default" :class="{ 'active' : formClient.type == 0}">
                                        PRIVATE
                                        </label>
                        </div>
                    </fieldset>
                </div>
                <div class="col-lg-6">
                    <fieldset class="form-group">
                        <label class="w-100" for="active">Active</label>
                        <div class="btn-group" data-toggle="buttons">
                            <label @click="formClient.active = 1" class="btn btn-default" :class="{ 'active' : formClient.active == 1}">
                                        ACTIVE
                                        </label>
                            <label @click="formClient.active = 0" class="btn btn-default" :class="{ 'active' : formClient.active == 0}">
                                        INACTIVE
                                        </label>
                        </div>
                    </fieldset>
                </div>
                <div class="col-md-12">
                    <hr>
                </div>
                <div class="col-lg-6">
                    <fieldset class="form-group">
                        <label for="name">Person To call</label>
                        <input type="text" v-model="formClient.contact_name" class="form-control" name="name" placeholder="Person Name" :class="{ 'is-invalid' : formClient.errors.has('contact_name') }">
                        <div v-if="formClient.errors.has('contact_name')" class="invalid-feedback" v-text="formClient.errors.get('contact_name')"></div>
                    </fieldset>
                </div>
                <div class="col-lg-6">
                    <fieldset class="form-group">
                        <label for="name">Person Role</label>
                        <input type="text" v-model="formClient.contact_role" class="form-control" name="name" placeholder="Person Role" :class="{ 'is-invalid' : formClient.errors.has('contact_role') }">
                        <div v-if="formClient.errors.has('contact_role')" class="invalid-feedback" v-text="formClient.errors.get('contact_role')"></div>
                    </fieldset>
                </div>
                <div class="col-lg-12">
                    <fieldset class="form-group">
                        <label for="name">Person Number</label>
                        <input type="text" v-model="formClient.contact_details" class="form-control" name="name" placeholder="Person Number" :class="{ 'is-invalid' : formClient.errors.has('contact_details') }">
                        <div v-if="formClient.errors.has('contact_details')" class="invalid-feedback" v-text="formClient.errors.get('contact_details')"></div>
                    </fieldset>
                </div>
            </div>
        </form>
    </div>
</div>