<!doctype html>
<html lang="{{ app()->getLocale() }}" languages='@json(config(' app.locales '))'>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" type="text/css" href="css/admin.css">
  <title>LAKEVIEWS - BACKOFFICE - Insurance and Wealth Advisors </title>
</head>
<body style="display: none;">
  <div id="app">
    <admin inline-template :my-roles=@json(auth()->user()->getRoles())>
        @include('admin.page')
    </admin>
  </div>
</body>
<script type="text/javascript" src="../plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="js/admin.js"></script>
</html>