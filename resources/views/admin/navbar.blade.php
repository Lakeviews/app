<ul id="navbar" :class="{ 'collapsed' : !navBar.status }" class="nav flex-column">
    <li class="nav-item first" style="height: 4rem;">
            @if(count(config('app.locales')) > 1)
    <div class="btn-group locales" role="group" aria-label="Languages">
        @foreach(config('app.locales') as $locale)
        <a href="{{ url('language/'.$locale) }}" role="button" class="btn btn-primary btn-sm {{ session()->get('locale') == $locale ? 'active' : '' }}">{{ $locale }}</a> @endForeach
    </div>
    @endif 
    
        <div class="expander">
            <a class="nav-link" @click="navBar.status =true"><i class="fa fa-bars" aria-hidden="true"></i></a>
        </div>
        <div class="collapaser">
            <a class="nav-link">
   <i class="fa fa-chevron-left" aria-hidden="true" @click="navBar.status =false"></i>    
   <span >
    <h5 class="title">LPANEL </h5>
    <span class="name" ><strong>Hi,</strong> {{ auth()->user() ->name}}</span>
  </span>
  <span class="comments" @click="openNotifications()">
    <span class="fa fa-comments"></span>
    <span class="badge badge-danger count" v-text="notys.length"></span>
  </span>
  <span class="logout" @click="logout()">
    <i class="fa fa-power-off"></i> 
  </span>
</a>
        </div>
    </li>
    {{-- BIND MENU VUE --}}
    <li class="nav-item" v-for="navItem in navItems">
        <a class="nav-link" @click="setNavItem(navItem)" :class="{'active' : navItem.active }" href="#">
<i class="fa"  :class="navItem.icon" aria-hidden="true"></i>
<span v-text="navItem.name"></span>
</a>
    </li>
    <li class="nav-item">
        <a @click="openNotifications()" class="nav-link">
<i class="fa fa-comments" aria-hidden="true"></i>
<span class="badge badge-danger badge-count" v-text="notys.length"></span>
<span v-text="notys.length + ' NEW MESSAGES'"></span>
</a>
    </li>
    <li class="nav-item">
        <a href="/" class="nav-link">
<i class="fa fa-home" aria-hidden="true"></i>
<span>WEBSITE</span>
</a>
    </li>
    <li class="nav-item">
        <a href="/logout" class="nav-link">
<i class="fa fa-power-off" aria-hidden="true"></i>
<span>LOGOUT</span>
</a>
    </li>
</ul>