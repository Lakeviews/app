<dashboard inline-template>
	
	<section>
		<div class="slide-2x" :class="slide">
			{{-- SLIDE ONE --}}
			<div class="container-fluid slide-1">
				<div class="row">

					<div class="col-md-9">
						<h6 class="primary">DASHBOARD</h6>

					</div>
					<div class="col-md-3">

							<select id="years" v-model="year" @change="bind()" class="selectpicker mt-3 mb-3 float-left w-25">
								<option v-for="year in years" :value="year" selected v-text="year"></option>
							</select>
						
						{{-- <button type="button" class="btn btn-primary float-right pot-05">

							<i class="fa fa-gear faa-spin animated"></i>
							<i class="fa fa-plus mr-1"></i>
							CREATE NEW
						</button> --}}
					</div>
					<div class="col-md-12">
						<hr>
					</div>
				</div>
				<div class="row">
					<div id="plot" class="col-md-12">



					</div>
				</div>
			</div>



			{{-- SLIDE TWO --}}
			<div class="container-fluid slide-2">



			</div>
		</div>
	</section>


</dashboard>
