<div class="app" :class="{ 'collapsed' : !navBar.status }">
    {{-- NOTIFICATIONS --}}
    @include('partials/notifications') {{-- LEFT NAVBAR --}}
    <section id="container">
        @include('admin.navbar')
        {{-- CONTENT --}}
        <div id="content" :class="{'fade-in' : !loader.enable , 'fade-out' : loader.enable }">
            <section id="section_content"></section>
        </div>
        <div v-show="loader.visible" class='loader-2' :class="{'fade-in' : loader.enable , 'fade-out' : !loader.enable }">
            <div class='circle'></div>
                <div class='circle'></div>
                <div class='circle'></div>
                <div class='circle'></div>
                <div class='circle'></div>
                <h1 class="quote" v-text="loader.message"></h1>
            </div>
        </div>
    </section>
</div>