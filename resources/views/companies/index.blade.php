<companies inline-template>
	<section>
		<div class="slide-2x" :class="slide">
			{{-- SLIDE ONE --}}
			<div class="container-fluid slide-1">
				<div class="row">

					<div class="col-md-6">
						<h6 class="primary">COMPANIES</h6>
						
					</div>
					<div class="col-md-6">

						<button type="button" @click="edit(table.model,true)" class="btn btn-primary float-right pot-05">
							
							<i v-if="table.model.loading" class="fa fa-gear faa-spin animated"></i>
							<i class="fa fa-plus mr-1"></i>
							CREATE NEW
						</button>
						<button @click="processDelete()"  type="button" v-if="trash.length > 0" class="btn btn-danger mr-2 pot-05 float-right">
							<i class="fa fa-trash"></i>
						</button>	
					</div>
				</div>
				<div class="row" v-if="table.paginate != null">
					<div class="col-md-12">


						<table class="table table-striped table-hover">
							<thead>
								<tr class="header">
									<td colspan="3"><h3>COMPANIES MANAGEMENT </h3></td>
									<td colspan="2"></td>
									<td colspan="2">
										<div class="input-group">
											<input type="text" v-model="table.params.search" @keyup="table.bind()" class="form-control" placeholder="Search here">
											<span class="input-group-addon">
												<i class="fa fa-search"></i>
											</span>
										</div>
									</td>
								</tr>
								<tr>
									<th class="w3" scope="col">
										<input @click="selectetAll($event.target.checked)" type="checkbox">
									</th>
									<th  class="w4" @click="table.setSort('id')" :class="table.sorts['id']" scope="col">ID</th>
									<th  @click="table.setSort('name')" :class="table.sorts['name']" scope="col">Name</th>
									<th @click="table.setSort('description')" :class="table.sorts['description']" scope="col">Email</th>
									<th @click="table.setSort('type')" :class="table.sorts['type']" scope="col">Permission</th>
									<th @click="table.setSort('active')" :class="table.sorts['active']" scope="col">Status</th>
									<th class="actions" scope="col">Action</th>
								</tr>
							</thead>
							<tbody>	
								<tr v-for="(row, index) in table.paginate.data">
									<td scope="row"><input v-model="row.selected" type="checkbox"></td>
									<td scope="col" v-text="row.id"></td>
									<td scope="col" v-text="row.name"></td>
									<td scope="col" v-text="row.description"></td>								
									<td scope="col">
										<span v-if="row.type == 1" class="badge badge-primary">INSURANCE</span>
										<span v-if="row.type == 2" class="badge badge-success">RELOCATION</span>
										<span v-if="row.type == 3" class="badge badge-warning">GENERIC</span>
									</td>		
									<td scope="col">
										<span v-if="row.active" class="badge badge-success">ON</span>
										<span v-if="!row.active" class="badge badge-danger">OFF</span>
									</td>	
									<td class="tac" scope="col">
										<button @click="edit(row, true)" type="button" class="btn btn-sm btn-primary">
											<i v-if="row.loading" class="fa fa-gear faa-spin animated"></i>
											<i class="fa fa-edit"></i>
										</button>
										<button @click="del(index)" type="button" class="btn btn-sm btn-danger	">
											<i class="fa fa-trash"></i>
										</button>
									</td>
								</tr>
							</tbody>
						</table>
						<nav class="float-right">
							<ul class="pagination">
								
								<li  class="page-item" :class="{ 'disabled' : table.paginate.current_page == 1}">
									<a class="page-link" @click="table.first()"  tabindex="-1">
										<i class="fa fa-angle-double-left" aria-hidden="true"></i>
									</a>
								</li>

								<li  class="page-item" :class="{ 'disabled' : table.paginate.current_page == 1}">
									<a class="page-link" @click="table.prev()"  tabindex="-1">
										<i class="fa fa-angle-left" aria-hidden="true"></i>
									</a>
								</li>

								<li class="page-item" :class="{'active': table.paginate.current_page == page}" v-for="page in table.pages()"  ><a class="page-link" @click="table.goto(page)"  v-text="page" tabindex="-1"></a></li>

								<li v-if="table.paginate.last_page > 2" class="page-item" :class="{'active': table.paginate.current_page == table.paginate.last_page }" >
									<a class="page-link" @click="table.last()" v-text=" table.paginate.last_page" tabindex="-1">
									</a>
								</li>

								<li  class="page-item" :class="{ 'disabled' : table.paginate.current_page == table.paginate.last_page }">
									<a class="page-link" @click="table.next()"  tabindex="-1">
										<i class="fa fa-angle-right" aria-hidden="true"></i>
									</a>
								</li>

								<li class="page-item" :class="{ 'disabled' : table.paginate.current_page == table.paginate.last_page }">
									<a  @click="table.last()" class="page-link"  tabindex="-1">
										<i class="fa fa-angle-double-right" aria-hidden="true"></i>
									</a>
								</li>
							</ul>
						</nav>

					</div>
				</div>
			</div>



			{{-- SLIDE TWO --}}
			<div v-if="formCompany != null" class="container-fluid slide-2">

				<div class="row lb">

					<div class="col-md-6">
						<h6 class="primary"><span v-text="formCompany.id != undefined ? 'EDITING' : 'NEW'"></span> COMPANY</h6>
						<h2 class="pt-1 pb-1"><span v-text="formCompany.name"></span>&nbsp; </h2>
					</div>
					<div class="col-md-6">
						<button type="button" :disabled="table.model.loading" @click="back()" class="btn btn-primary float-right">BACK</button>
					</div>
					<div class="col-md-12">
						<hr class="mt--05">	
					</div>


				</div>
				
				<div class="row">
					<div class="col-md-6">
						<div class="card">
							<div class="card-header">
								INFO
							</div>
							<div class="card-body">
								<form class="p-3" @keypress="formCompany.errors.clear($event.target.name)"  @keypress.enter="save()">

									<fieldset class="form-group">
										<label for="name">Name</label>
										<input type="text" v-model="formCompany.name" class="form-control"
										name="name" placeholder="type the name"
										:class="{ 'is-invalid' : formCompany.errors.has('name') }">
										<div v-if="formCompany.errors.has('name')"  class="invalid-feedback" v-text="formCompany.errors.get('name')"></div>

									</fieldset>

									<fieldset class="form-group">
										<label for="email">Email</label>
										<input type="text" v-model="formCompany.email" class="form-control"
										name="email" placeholder="type the email"
										:class="{ 'is-invalid' : formCompany.errors.has('email') }">
										<div v-if="formCompany.errors.has('email')"  class="invalid-feedback" v-text="formCompany.errors.get('email')"></div>
									</fieldset>

									<fieldset class="form-group">
										<label for="email">Description</label>
										<textarea rows="2" maxlength="300"  v-model="formCompany.description" class="form-control" name="description" placeholder="Enter a description"></textarea>

									</fieldset>
									

									<div class="row">
										<div class="col-md-5">
											<fieldset class="form-group">
												<label class="w-100" for="active">Active</label>
												<div class="btn-group" data-toggle="buttons">
													<label   @click="formCompany.active = 1" class="btn btn-default" :class="{ 'active' : formCompany.active == 1}">
														ACTIVE
													</label>
													<label @click="formCompany.active = 0" class="btn btn-default" :class="{ 'active' : formCompany.active == 0}">
														INACTIVE
													</label>
												</div>
											</fieldset>
										</div>
										<div class="col-md-7">
											<fieldset class="form-group">
												<label class="w-100" for="type">Permission</label>
												<div class="btn-group" data-toggle="buttons">
													<label   @click="formCompany.type = 1" class="btn btn-primary" :class="{ 'active' : formCompany.type == 1}">
														INSURANCE
													</label>
													<label @click="formCompany.type = 2" class="btn btn-success" :class="{ 'active' : formCompany.type == 2}">
														RELOCATION
													</label>
													<label @click="formCompany.type = 3" class="btn btn-warning" :class="{ 'active' : formCompany.type == 3}">
														GENERIC		
													</label>
												</div>
											</fieldset>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="card">
							<div class="card-header">
								DETAILS
							</div>
							<div class="card-body">
								<form class="p-3" @change="formCompany.errors.clear($event.target.name)" 
								@keypress="formCompany.errors.clear($event.target.name)"  @keypress.enter="save()">
									<div class="row">
										
										<div class="col-md-6">	
											<fieldset class="form-group" >
												<label for="latitude">Latitude</label>
												<input  type="text" name="latitude" class="form-control" v-model="formCompany.latitude" />
											</fieldset>
										</div>
										
										<div class="col-md-6">	
											<fieldset class="form-group" >
												<label for="postcode">Longitude</label>
												<input  type="text" name="longitude" class="form-control" v-model="formCompany.longitude" />
											</fieldset>
										</div>

										<div class="col-md-6">	
											<fieldset class="form-group">
												<label for="mobile">Phone</label>
												<input  type="text" name="phone" class="form-control" v-model="formCompany.phone"
												:class="{ 'is-invalid' : formCompany.errors.has('phone') }" />
												<div v-if="formCompany.errors.has('phone')"  class="invalid-feedback" v-text="formCompany.errors.get('phone')"></div>
											</fieldset>

										</div>

										<div class="col-md-6">	
											<fieldset class="form-group">	
												<label for="phone_2">Phone 2</label>
												<input  type="text" name="phone_2" class="form-control" v-model="formCompany.phone_2" />
											</fieldset>
										</div>

										<div class="col-md-6">	
											<fieldset class="form-group">
												<label for="fax">Fax</label>
												<input  type="text" name="fax" class="form-control" v-model="formCompany.fax" />
											</fieldset>

										</div>

										<div class="col-md-6">	
											<fieldset class="form-group">
												<label for="mobile">Mobile</label>
												<input  type="text" name="mobile" class="form-control" v-model="formCompany.mobile" />
											</fieldset>
										</div>


										<div class="col-md-12">	
											<fieldset class="form-group">
												<label for="address">Address</label>
												<input  type="text" name="address" class="form-control" v-model="formCompany.address" 
												:class="{ 'is-invalid' : formCompany.errors.has('address') }"/>
												<div v-if="formCompany.errors.has('address')"  class="invalid-feedback" v-text="formCompany.errors.get('address')"></div>
											</fieldset>

										</div>

										<div class="col-md-3">	
											<fieldset class="form-group">
												<label for="address">Town</label>
												<input  type="text" name="town" class="form-control" v-model="formCompany.town"
												:class="{ 'is-invalid' : formCompany.errors.has('town') }" />
												<div v-if="formCompany.errors.has('town')"  class="invalid-feedback" v-text="formCompany.errors.get('town')"></div>
											</fieldset>
										</div>

										<div class="col-md-3">	
											<fieldset class="form-group">
												<label for="postcode">Postcode</label>
												<input  type="text" name="postcode" class="form-control" v-model="formCompany.postcode"
												:class="{ 'is-invalid' : formCompany.errors.has('postcode') }" />
												<div v-if="formCompany.errors.has('postcode')"  class="invalid-feedback" v-text="formCompany.errors.get('postcode')"></div>
											</fieldset>
										</div>

										<div class="col-md-6">	
											<fieldset class="form-group"  :class="{ 'is-invalid' : formCompany.errors.has('country') }">	
												<label for="countries">Country	</label>	
												<select id="countries" name="country" @change="formCompany.country = $event.target.selectedOptions[0].innerText" class="selectpicker"	 data-live-search="true">
													<option value="" selected disabled>Please select</option>
													@include('partials.countries')
												</select>
												<div v-if="formCompany.errors.has('country')"  class="invalid-feedback" v-text="formCompany.errors.get('country')"></div>
											</fieldset>
										</div>




									</div>


								</form>
							</div>
						</div>
					</div>

				</div>
				<div>
					<div class="row">
						<div class="col-md-12">
							<hr class="mt-5">
							<button :disabled="table.model.loading" type="submit" @click="save()" class="btn btn-primary float-right">
								<i v-if="table.model.loading" class="fa fa-gear faa-spin animated"></i>
								SAVE
							</button>	
							<button :disabled="table.model.loading" @click="back()" type="buttom" class="btn btn-danger float-right mr-2">CANCEL</button>	
						</div>
					</div>
				</div>

			</div>
		</div>
	</section>
</companies>
