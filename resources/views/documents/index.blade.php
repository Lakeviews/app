<documents inline-template>
	<section>
		<div class="slide-2x" :class="slide">
			{{-- SLIDE ONE --}}
			<div class="container-fluid slide-1">
				<div class="row">

					<div class="col-md-6">
						<h6 class="primary">DOCUMENTS</h6>
						
					</div>
					<div class="col-md-6">

						<button type="button" @click="edit(table.model,true)" class="btn btn-primary float-right pot-05">
							
							<i v-if="table.model.loading" class="fa fa-gear faa-spin animated"></i>
							<i class="fa fa-plus mr-1"></i>
							CREATE NEW
						</button>
						<button @click="processDelete()"  type="button" v-if="trash.length > 0" class="btn btn-danger mr-2 pot-05 float-right">
							<i class="fa fa-trash"></i>
						</button>	
					</div>
				</div>
				<div class="row" v-if="table.paginate != null">
					<div class="col-md-12">


						<table class="table table-striped table-hover">
							<thead>         
								<tr class="header"> 
									<td colspan="5"><h3>DOCUMENTS MANAGEMENT </h3></td>
									<td colspan="2"></td>
									<td colspan="4">
										<div class="input-group"> 
											<input type="text" v-model="table.params.search"
											@keyup="table.bind()" class="form-control"
											placeholder="Search here">
											<span class="input-group-addon"> <i class="fa fa-search"></i></span>
										</div>
									</td>
								</tr>
								<tr>
									<th class="w3" scope="col">                 <input
												@click="selectetAll($event.target.checked)"
												type="checkbox">             </th>             <th
												class="w4" @click="table.setSort('id')"
												:class="table.sorts['id']" scope="col">ID</th>
												<th @click="table.setSort('reference')"
												:class="table.sorts['reference']"
												scope="col">Reference</th>             <th
												@click="table.setSort('year')"
												:class="table.sorts['year']" scope="col">Year</th>
												<th @click="table.setSort('customer_id')"
												:class="table.sorts['customer_id']"
												scope="col">Customer</th>             <th
												@click="table.setSort('type')"
												:class="table.sorts['type']" scope="col">type</th>
												<th @click="table.setSort('product_id')"
												:class="table.sorts['product_id']"
												scope="col">Product</th>             <th
												@click="table.setSort('managed')"
												:class="table.sorts['managed']"
												scope="col">Managed</th>             <th
												@click="table.setSort('status')"
												:class="table.sorts['status']" scope="col">Status</th>
												<th @click="table.setSort('payment_type')"
												:class="table.sorts['payment_type']"
												scope="col">Payment</th>             <th
												class="actions" scope="col">Action</th>         </tr>
											</thead>     <tbody>          <tr v-for="(row, index)
												in table.paginate.data">             <td
												scope="row"><input v-model="row.selected"
												type="checkbox"></td>             <td scope="col"
												v-text="row.id"></td>             <td scope="col"
												v-text="row.reference"></td>             <td
												scope="col" v-text="row.year"></td>             <td
												scope="col" v-text="row.customer.name"></td>
												<td scope="col">                 <span
													v-if="row.product.type == 1" class="badge badge-
													primary">INSURANCE</span>                 <span
													v-if="row.product.type == 2" class="badge badge-
													warning">GENERIC</span>             </td>
													<td scope="col" v-text="row.product.full_name"></td>

													<td scope="col">	
														<span v-if="row.managed" class="badge badge-success">ON</span>
														<span v-if="!row.managed" class="badge badge-danger">OFF</span>
													</td>
													<td scope="col">	
														<span v-if="row.status == 'RUNNING'" v-text="row.status" class="badge badge-success"></span>
														<span v-if="row.status == 'MARKETING'" v-text="row.status" class="badge badge-info"></span>
														<span v-if="row.status == 'RENEW'" v-text="row.status" class="badge badge-warning"></span>
														<span v-if="row.status == 'EXPIRED'" v-text="row.status" class="badge badge-danger"></span>
													</td>
													<td scope="col">	
														<span v-if="row.payment_type == 1" class="badge badge-success">MONTHLY</span>
														<span v-if="row.payment_type == 2" class="badge badge-primary">QUATERLY</span>
														<span v-if="row.payment_type == 3" class="badge badge-warning">HALF-YEAR</span>
														<span v-if="row.payment_type == 4" class="badge badge-info">ANNUAL</span>
													</td>

													<td class="tac" scope="col">
														<button @click="edit(row, true)" type="button" class="btn btn-sm btn-primary">
															<i v-if="row.loading" class="fa fa-gear faa-spin animated"></i>
															<i class="fa fa-edit"></i>
														</button>
														<button @click="del(index)" type="button" class="btn btn-sm btn-danger	">
															<i class="fa fa-trash"></i>
														</button>
													</td>
												</tr>
											</tbody>
										</table>
										<nav class="float-right">
											<ul class="pagination">

												<li  class="page-item" :class="{ 'disabled' : table.paginate.current_page == 1}">
													<a class="page-link" @click="table.first()"  tabindex="-1">
														<i class="fa fa-angle-double-left" aria-hidden="true"></i>
													</a>
												</li>

												<li  class="page-item" :class="{ 'disabled' : table.paginate.current_page == 1}">
													<a class="page-link" @click="table.prev()"  tabindex="-1">
														<i class="fa fa-angle-left" aria-hidden="true"></i>
													</a>
												</li>

												<li class="page-item" :class="{'active': table.paginate.current_page == page}" v-for="page in table.pages()"  ><a class="page-link" @click="table.goto(page)"  v-text="page" tabindex="-1"></a></li>

												<li v-if="table.paginate.last_page > 2" class="page-item" :class="{'active': table.paginate.current_page == table.paginate.last_page }" >
													<a class="page-link" @click="table.last()" v-text=" table.paginate.last_page" tabindex="-1">
													</a>
												</li>

												<li  class="page-item" :class="{ 'disabled' : table.paginate.current_page == table.paginate.last_page }">
													<a class="page-link" @click="table.next()"  tabindex="-1">
														<i class="fa fa-angle-right" aria-hidden="true"></i>
													</a>
												</li>

												<li class="page-item" :class="{ 'disabled' : table.paginate.current_page == table.paginate.last_page }">
													<a  @click="table.last()" class="page-link"  tabindex="-1">
														<i class="fa fa-angle-double-right" aria-hidden="true"></i>
													</a>
												</li>
											</ul>
										</nav>

									</div>
								</div>
							</div>



							{{-- SLIDE TWO --}}
							<div v-if="formDocument != null" class="container-fluid slide-2">

								<div class="row lb">

									<div class="col-md-6">
										<h6 class="primary"><span v-text="formDocument.id != undefined ? 'EDITING' : 'NEW'"></span> DOCUMENT</h6>
										<h2 class="pt-1 pb-1"><span v-text="formDocument.reference"></span>&nbsp; </h2>
									</div>
									<div class="col-md-6">
										<button type="button" :disabled="table.model.loading" @click="back()" class="btn btn-primary float-right">BACK</button>
									</div>
									<div class="col-md-12">
										<hr class="mt--05">	
									</div>
								</div>
								<div class="row">
									<div class="col-md-6 mb-3">
										<div class="card">
											<div class="card-header">
												INFO
											</div>
											<div class="card-body">
												<form class="p-3" @change="formDocument.errors.clear($event.target.name)" @keypress="formDocument.errors.clear($event.target.name)"  @keypress.enter="save()">

													<div class="row">

														{{-- REFERENCE --}}
														<div class="col-md-6">
															<fieldset class="form-group">
																<label for="reference">Reference</label>
																<input type="text" v-model="formDocument.reference" class="form-control" name="reference" placeholder="type the reference" :class="{ 'is-invalid' : formDocument.errors.has('reference') }"
																>
																<div v-if="formDocument.errors.has('reference')"  class="invalid-feedback" v-text="formDocument.errors.get('reference')"></div>

															</fieldset>
														</div>

														{{-- YEAR --}}
														<div class="col-md-6">
															<fieldset class="form-group">	
																<label for="year">Year</label>	
																<select id="years" v-model="formDocument.year" class="selectpicker">
																	<option v-for="year in years" :value="year" selected v-text="year"></option>
																</select>
																<div v-if="formDocument.errors.has('year')"  class="invalid-feedback" v-text="formDocument.errors.get('year')"></div>
															</fieldset>
														</div>

														{{-- CUSTOMER_ID --}}
														<div class="col-md-6">
															<fieldset class="form-group" :class="{ 'is-invalid' : formDocument.errors.has('customer_id') }">	
																<label for="customers">Customer</label>	
																<select id="customers" name="customer_id" 
																v-model="formDocument.customer_id" 
																class="selectpicker form-control" data-live-search="true">
																<option value="" selected>Please select or None</option>
																@foreach($customers as $customer)
																<option data-json="{{ $customer }}" value="{{ $customer->id }}">{{ $customer->name }}</option>
																@endforeach
															</select>
															<div v-if="formDocument.errors.has('customer_id')"  class="invalid-feedback" v-text="formDocument.errors.get('customer_id')"></div>
														</fieldset>
													</div>


													{{-- PRODUCT_ID --}}
													<div class="col-md-6">
														<fieldset class="form-group" :class="{ 'is-invalid' : formDocument.errors.has('product_id') }">	
															<label for="products">Product</label>	
															<select id="products"  name="product_id" @change="productsChange($event)" class="selectpicker" data-live-search="true">
																<option value="" selected>Please select or None</option>
																@foreach($products as $product)
																<option data-type="{{ $product->type }}" value="{{ $product->id }}">{{ $product->full_name }}</option>
																@endforeach
															</select>
															<div v-if="formDocument.errors.has('product_id')"  class="invalid-feedback" v-text="formDocument.errors.get('product_id')"></div>
														</fieldset>
													</div>

													{{-- MANAGED --}}
													<div class="col-md-4">

														<fieldset class="form-group">
															<label class="w-100" for="active">Managed</label>
															<div class="btn-group" data-toggle="buttons">
																<label   @click="formDocument.managed = 1" class="btn btn-default" :class="{ 'active' : formDocument.managed == 1}">
																	YES
																</label>
																<label @click="formDocument.managed = 0" class="btn btn-default red" :class="{ 'active' : formDocument.managed == 0}">
																	NO
																</label>
															</div>
														</fieldset>
													</div>

													{{-- ACTIVE --}}
													<div class="col-md-4">

														<fieldset class="form-group">
															<label class="w-100" for="active">Active</label>
															<div class="btn-group" data-toggle="buttons">
																<label   @click="formDocument.active = 1" class="btn btn-default" :class="{ 'active' : formDocument.active == 1}">
																	ACTIVE
																</label>
																<label @click="formDocument.active = 0" class="btn btn-default red" :class="{ 'active' : formDocument.active == 0}">
																	INACTIVE
																</label>
															</div>
														</fieldset>
													</div>

													{{-- CLOSED --}}
													<div class="col-md-4">

														<fieldset class="form-group">
															<label class="w-100" for="active">Closed</label>
															<div class="btn-group" data-toggle="buttons">
																<label   @click="formDocument.closed = 1" class="btn btn-default red" :class="{ 'active' : formDocument.closed == 1}">
																	YES
																</label>
																<label @click="formDocument.closed = 0" class="btn btn-default red" :class="{ 'active' : formDocument.closed == 0}">
																	NO
																</label>
															</div>
														</fieldset>
													</div>

													{{-- DESCRIPTION --}}
													<div class="col-md-12">
														<fieldset class="form-group">
															<label for="description">Description</label>
															<textarea rows="3" maxlength="300"   v-model="formDocument.description" class="form-control" name="description" placeholder="Enter a description"></textarea>
														</fieldset>
													</div>

													{{-- LONG DESCRIPTION --}}
													<div class="col-md-12">
														<label for="long_description">Long Description</label>
														<editor id="title" :field="formDocument.long_description" toolbar="basic"
														height="90"></editor>
													</div>

												</div>


											</form>
										</div>
									</div>
								</div>		

								<div class="col-md-6 mb-3" v-show="formDocument.product_type != null && formDocument.product_type != ''">
									<div class="card">
										<div class="card-header">
											CONTRACT <button @click="copyCustomer" type="button" class="btn btn-primary btn-sm float-right"><strong>COPY CUSTOMER</strong></button>
										</div>
										<div class="card-body">
											<form class="p-3" @change="formDocument.errors.clear($event.target.name)"  @keypress="formDocument.errors.clear($event.target.name)"  @keypress.enter="save()">

												{{-- NAME --}}
												<div class="row">
													<div class="col-md-8" >
														<fieldset class="form-group">
															<label for="name">Name</label>
															<input type="text" v-model="formDocument.name" class="form-control"
															name="name" placeholder="type the name" :class="{ 'is-invalid' : formDocument.errors.has('name') }">
															<div v-if="formDocument.errors.has('name')"  class="invalid-feedback" v-text="formDocument.errors.get('name')"></div>
														</fieldset>
													</div>

													{{-- BIRTDATE --}}
													<div class="col-md-4">
														<fieldset class="form-group">
															<label for="name">Birthdate</label>

															<div class='input-group date' id='birthdate'>
																<input type='text' v-model="formDocument.birthdate"  class="form-control" :class="{ 'is-invalid' : formDocument.errors.has('birthdate') }"/>
																<span class="input-group-addon">
																	<span class="fa fa-calendar"></span>
																</span>
															</div>

															<div v-if="formDocument.errors.has('birthdate')"  class="invalid-feedback" v-text="formDocument.errors.get('birthdate')"></div>
														</fieldset>
													</div>

													{{-- MOBILE --}}
													<div class="col-md-6">	
														<fieldset class="form-group">
															<label for="mobile">Mobile</label>
															<input  type="text" name="mobile" placeholder="type mobile" class="form-control" v-model="formDocument.mobile" :class="{ 'is-invalid' : formDocument.errors.has('mobile') }"/>	
															<div v-if="formDocument.errors.has('mobile')"  class="invalid-feedback" v-text="formDocument.errors.get('mobile')"></div>
														</fieldset>
													</div>

													{{-- PHONE --}}
													<div class="col-md-6">	
														<fieldset class="form-group">
															<label for="phone">Phone</label>
															<input  type="text" name="phone" placeholder="type phone" class="form-control" v-model="formDocument.phone" />
														</fieldset>
													</div>

													{{-- ADDRESS --}}
													<div class="col-md-12">	
														<fieldset class="form-group">
															<label for="address">Address</label>
															<input  type="text" name="address" placeholder="type address" class="form-control" v-model="formDocument.address" :class="{ 'is-invalid' : formDocument.errors.has('address') }"/>
															<div v-if="formDocument.errors.has('address')"  class="invalid-feedback" v-text="formDocument.errors.get('address')"></div>
														</fieldset>
													</div>

													{{-- TOWN --}}
													<div class="col-md-3">	
														<fieldset class="form-group">
															<label for="address">Town</label>
															<input  type="text" name="town" placeholder="type town" class="form-control" v-model="formDocument.town" :class="{ 'is-invalid' : formDocument.errors.has('town') }"/>
															<div v-if="formDocument.errors.has('town')"  class="invalid-feedback" v-text="formDocument.errors.get('town')"></div>
														</fieldset>
													</div>

													{{-- POSTCODE --}}
													<div class="col-md-3">	
														<fieldset class="form-group">
															<label for="postcode">Postcode</label>
															<input  type="text" name="postcode" placeholder="type the postcode" class="form-control" v-model="formDocument.postcode" 
															:class="{ 'is-invalid' : formDocument.errors.has('postcode') }"/>
															<div v-if="formDocument.errors.has('postcode')"  class="invalid-feedback" v-text="formDocument.errors.get('postcode')"></div>
														</fieldset>
													</div>

													{{-- COUNTRY --}}
													<div class="col-md-6">	
														<fieldset class="form-group" :class="{ 'is-invalid' : formDocument.errors.has('country') }">	
															<label for="countries">Country	</label>	
															<select id="countries" name="country" onchange="formDocument.country = this.selectedOptions[0].innerText" class="selectpicker"	 data-live-search="true">
																<option value="" selected disabled>Please select</option>
																@include('partials.countries')
															</select>
															<div v-if="formDocument.errors.has('country')"  class="invalid-feedback" v-text="formDocument.errors.get('country')"></div>

														</fieldset>
													</div>

													{{-- DATE START --}}
													<div class="col-md-6">
														<fieldset class="form-group">
															<label for="date_start">Start</label>
															<div class='input-group date' id='date_start'>
																<input type='text' v-model="formDocument.date_start"  class="form-control" :class="{ 'is-invalid' : formDocument.errors.has('date_start') }"/>
																<span class="input-group-addon">
																	<span class="fa fa-calendar"></span>
																</span>
															</div>
															<div v-if="formDocument.errors.has('date_start')"  class="invalid-feedback" v-text="formDocument.errors.get('date_start')"></div>
														</fieldset>
													</div>

													{{-- DATE END --}}
													<div class="col-md-6">
														<fieldset class="form-group">
															<label for="date_end">End</label>

															<div class='input-group date' id='date_end'>
																<input type='text' v-model="formDocument.date_end" class="form-control" :class="{ 'is-invalid' : formDocument.errors.has('date_end') }"/>
																<span class="input-group-addon">
																	<span class="fa fa-calendar"></span>
																</span>
															</div>

															<div v-if="formDocument.errors.has('date_end')"  class="invalid-feedback" v-text="formDocument.errors.get('date_end')"></div>
														</fieldset>
													</div>

													{{-- MARKETING PERIOD --}}
													<div class="col-md-6 pl-4 pr-3"  v-show="formDocument.product_type == '1'">	
														<fieldset class="form-group">
															<label for="marketing_period">Marketing Period</label>
															<div id="marketing_period"></div>
															<ul class="slider-step seven">
																<li v-for="step in slider" v-text="step"></li>
															</ul>
														</fieldset>
													</div>

													{{-- NOTIFICATION PERIOD --}}
													<div class="col-md-6 pl-3 pr-3" v-show="formDocument.product_type == '1'">	
														<fieldset class="form-group">
															<label for="phone">Notification Period</label>
															<div id="notification_period"></div>
															<ul class="slider-step seven">
																<li v-for="step in slider" v-text="step"></li>
															</ul>

														</fieldset>
													</div>

													{{-- PAYMENT TYPE --}}
													<div class="col-md-6">
														<fieldset class="form-group" :class="{ 'is-invalid' : formDocument.errors.has('payment_type') }">	
															<label for="year">Payment Type</label>	
															<select id="payment_type" name="payment_type" v-model="formDocument.payment_type" class="selectpicker">
																<option value="1">Monthly</option>
																<option value="2">Quarterly</option>
																<option value="3">Half-Year</option>
																<option value="4">Annual</option>
															</select>
															<div v-if="formDocument.errors.has('payment_type')"  class="invalid-feedback" v-text="formDocument.errors.get('payment_type')"></div>
														</fieldset>
													</div>	

													{{-- 	 --}}
													<div class="col-md-6">	
														<fieldset class="form-group">
															<label for="amount">Amount</label>
															<input  type="text" name="amount" @change="formatAmount" placeholder="type amount" class="form-control" v-model="formDocument.amount" :class="{ 'is-invalid' : formDocument.errors.has('amount') }" />
															<div v-if="formDocument.errors.has('amount')"  class="invalid-feedback" v-text="formDocument.errors.get('amount')"></div>
														</fieldset>
													</div>

												</div>

											</form>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="card">
										<div class="card-header">Files</div>
										<div class="card-body">

											<form class="p-3">
												<div class="row">
													<div class="col-md-12">
														<fieldset class="form-group">
															<upload-control :entity="'documents/' + formDocument.id" :ext="['.doc', '.docx', '.xls', '.xlsx', '.ppt', 'pptx', '.pdf','.jpg', '.jpeg','.png']" :size="4096" event="update-document-files" update='update-child-document-files'></upload-control>
														</fieldset>

													</div>
												</div>
											</form>

										</div>
									</div>
								</div>
							</div>
							<div>
								<div class="row">
									<div class="col-md-12">
										<hr class="mt-5">
										<button :disabled="table.model.loading" type="submit" @click="save()" class="btn btn-primary float-right">
											<i v-if="table.model.loading" class="fa fa-gear faa-spin animated"></i>
											SAVE
										</button>	
										<button :disabled="table.model.loading" @click="back()" type="buttom" class="btn btn-danger float-right mr-2">CANCEL</button>	
									</div>
								</div>
							</div>

						</div>
					</div>
				</section>
			</documents>
