@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' =>  env('APP_URL')])
           <img alt="logo lakeviews" src="{{ env('APP_URL').'/images/logo.png' }}" width="240px">
        @endcomponent
    @endslot

    {{-- Body --}}
    {{ $slot }}

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            &copy; {{ date('Y') }} {{ config('app.name') }}. All rights reserved.
        @endcomponent
    @endslot
@endcomponent
