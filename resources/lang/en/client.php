<?php

return [
    'welcome' => 'Welcome',
    'open' => 'Open',
    'closed' => 'Closed',
    'customer' => 'Customer',
    'product' => 'Product',
    'title' => 'Title',
    'message' => 'Message',
    'attach_file' => 'Attach a file',
    'profile' => 'Profile',
    'recover' => 'Récupérer',
    'back' => 'Annuler',
    'details' => 'Details',
    'gender' => 'Gender',
    'male' => 'Male',
    'female' => 'Female',
    'birthdate' => 'Birthdate',
    'phone' => 'Phone',
    'mobile' => 'Mobile',
    'address' => 'Address',
    'town' => 'Town',
    'postcode' => 'Postcode',
    'country' => 'Country',



    'select.text' => 'Please Select',


    'main.quote' => 'WELCOME TO LAKEVIEWS',
    'main.sub_quote' => 'The insurance to be well insured!',
    'main.btn_more' => 'MORE',
    'main.btn_insurances' => 'INSURANCES',

    //Buttons
    'btn.get_quote' => 'GET QUOTE',
    'btn.im_interested' => "I'M INTERESTED",
    'btn.let_me_know' => 'LET ME KNOW',
    'btn.profile' => 'PROFILE',
    'btn.send_email' => 'SEND AN EMAIL',
    'btn.create_ticket' => 'CREATE A TICKET',
    'btn.cancel' => 'CANCEL',
    'btn.send' => 'SEND',
    'btn.authenticate' => 'AUTHENTICATE',
    'btn.password_recover' => 'PASSWORD RECOVER',
    'btn.recover' => 'RECOVER',
    'btn.save' => 'SAVE',
    'btn.back' => 'BACK',
   

    //Forms
    'form.name' => 'Name',
    'form.name.placeholder' => 'Enter your name',
    'form.email' => 'Email',
    'form.email.placeholder' => 'Enter your email',
    'form.password' => 'Password',
    'form.password.placeholder' => 'Enter your password',
    'form.phone_mobile' => 'Phone | Mobile',
    'form.message' => 'Message',
    'form.message.placeholder' => 'Please enter a Message',
    'form.send' => 'SEND',
    'form.title.placeholder' => 'Please enter the title',

    'form.invalid' => 'Please check the form',




    'navigation' => 'NAVIGATION',
    'all_right_reserved' => 'All right reserved',


    //LOGIN
    'login.title' => 'Please introduce your email and password provided by your adviser.',
    'password_recover.quote' => 'Please introduce your email in order to recover your password.',

      //PROFILE
    'profile.title' => "This is your profile, you can edit your information and change your password to.",
   
    //DASHBOARD
    'customer.dashboard.subtitle' => "This is your dashboard, from here you access yout insurances, check you relocation status, save your documents, and ask for questions to our advisers by submiting a ticket.",

    'customer.dashboard.insurances.title' => 'INSURANCES & DOCUMENTS',
    'customer.dashboard.insurances.description' => 'INSURANCES & DOCUMENTS',
    'customer.dashboard.insurances.btn' => 'GO INSURANCES',

    'customer.dashboard.relocation.title' => 'RELOCATION',
    'customer.dashboard.relocation.description' => 'RELOCATION',
    'customer.dashboard.relocation.btn' => 'GO RELOCATION',

    'customer.dashboard.storage.title' => 'MY STORAGE',
    'customer.dashboard.storage.description' => 'MY STORAGE',
    'customer.dashboard.storage.btn' => 'GO STORAGE',


    'customer.dashboard.tickets.title' => 'QUESTION, CHAT - TICKET',
    'customer.dashboard.tickets.description' => 'QUESTION, CHAT - TICKET',
    'customer.dashboard.tickets.btn' => 'GO TICKET',



    'ticket.quote' => 'Here you can submit a ticket, a chat box will appear after submiting the ticket, will reply to you as soon as we can',
];