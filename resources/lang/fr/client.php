<?php


return [
    'welcome' => 'Bienvenue',
    'open' => 'Ouvert',
    'closed' => 'Fermé',
    'customer' => 'Client',
    'product' => 'Produit',
    'title' => 'Titre',
    'message' => 'Message',
    'attach_file' => 'Télécharger un fichier',
    'profile' => 'Profil',
    'recover' => 'Récupérer',
    'back' => 'Back',
    'details' => 'Détails',
    'gender' => 'Genre',
    'male' => 'Homme',
    'female' => 'Femme',
    'birthdate' => 'Date de Naissance',
    'phone' => 'Téléphone',
    'mobile' => 'Mobile',
    'address' => 'Adresse',
    'town' => 'Ville',
    'postcode' => 'Code postal',
    'country' => 'Pays',



    'select.text' => 'Veuillez sélectionner',

    'main.quote' => 'BIENVENUE À LAKEVIEWS',
    'main.sub_quote' => "L'assurance pour être bien assuré",
    'main.btn_more' => 'SAVOIR PLUS',
    'main.btn_insurances' => 'ASSURANCES',
     
    //Buttons
    'btn.get_quote' => 'OBTENIR UNE SIMULATION',
    'btn.im_interested' => "JE SUIS INTÉRESSÉ",
    'btn.let_me_know' => 'SAVOIR PLUS',
    'btn.login' => '',
    'btn.logout' => '',
    'btn.profile' => 'PROFIL',
    'btn.send_email' => 'ENVOYER UN MAIL',
    'btn.create_ticket' => 'CRÉER UN TICKET',
    'btn.cancel' => 'ANNULER',
    'btn.send' => 'ENVOYER',
    'btn.authenticate' => 'AUTHENTIFIER',
    'btn.password_recover' => 'RÉCUPÉRATION DU MOT DE PASSE',
    'btn.recover' => 'RECOVER',
    'btn.save' => 'ENREGISTER',
    'btn.back' => 'RETOUNER',

    //Forms
    'form.name' => 'Nom',
    'form.name.placeholder' => 'Entrer votre nom',
    'form.email' => 'Email',
    'form.email.placeholder' => 'Entrer votre mail',
    'form.password' => 'Mot de Pass',
    'form.password.placeholder' => 'Entrer votre mot de passe',
    'form.phone_mobile' => 'Téléphone | Mobile',
    'form.message' => 'Message',
    'form.message.placeholder' => "S'il vous plaît entrer un message",
    'form.send' => 'ENVOYER',
    'form.title.placeholder' => "S'il vous plaît entrer un titre",

    'form.invalid' => 'Veuillez vérifier le formulaire',
   

    'navigation' => 'NAVIGATION',
    'all_right_reserved' => 'Tous les droits reservé',

    //LOGIN
    'login.title' => 'Veuillez introduire votre email et mot de passe fourni par votre conseiller.',
    'password_recover.quote' => 'Veuillez introduire votre email afin de récupérer votre mot de passe',

    //PROFILE
    'profile.title' => "Ceci est votre profil, vous pouvez modifier vos informations et changer votre mot de passe.",
    
    //DASHBOARD

    'customer.dashboard.subtitle' => "Voici votre tableau de bord, d'ici vous accédez à vos assurances, vérifiez votre statut de relocalisation, enregistrez vos documents et posez des questions à nos conseillers en soumettant un ticket.",

    'customer.dashboard.insurances.title' => 'ASSURANCES ET DOCUMENTS',
    'customer.dashboard.insurances.description' => 'INSURANCES & DOCUMENTS',
    'customer.dashboard.insurances.btn' => 'GO ASSURANCES',

    'customer.dashboard.relocaiton.title' => 'DÉMÉNAGEMENT',
    'customer.dashboard.relocaiton.description' => 'RELOCATION',
    'customer.dashboard.relocaiton.btn' => 'GO DÉMÉNAGEMENT',

    'customer.dashboard.storage.title' => 'MY STORAGE',
    'customer.dashboard.storage.description' => 'MY STORAGE',
    'customer.dashboard.storage.btn' => 'GO STORAGE',


    'customer.dashboard.tickets.title' => 'QUESTION, CHAT - TICKET',
    'customer.dashboard.tickets.description' => 'QUESTION, CHAT - TICKET',
    'customer.dashboard.tickets.btn' => 'GO TICKET',

    'ticket.quote' => 'Ici, vous pouvez soumettre un ticket, une boîte de dialogue apparaîtra après la soumission du ticket, vous répondra dès que possible',
];