
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.busi = new Vue;

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


import { GMap } from './core/maps.js';
import { Table } from './core/table.js';
import { Form } from './core/form.js';
import { Arr } from './core/array.js';
import { ImagePro } from './core/ImagePro.js';
import { Link } from './core/link.js';

window.GMap = GMap;
window.Table = Table;
window.Form = Form;
window.ImagePro = ImagePro;
window.Link = Link;
window.Arr = new Arr();

Vue.component('app', require('./components/app.vue'));
Vue.component('profile', require('./views/profile.vue'));
Vue.component('customer-dashboard', require('./views/customer-dashboard.vue'));
Vue.component('auth', require('./views/auth.vue'));
Vue.component('insurances-documents', require('./views/insurances-documents.vue'));
Vue.component('relocation', require('./views/relocation.vue'));
Vue.component('storage', require('./views/storage.vue'));
Vue.component('ticket-system', require('./views/ticket-system.vue'));
Vue.component('ticket', require('./components/ticket.vue'));
Vue.component('upload-control', require('./components/upload-control.vue'));


const app = new Vue({
	el: '#app',
	methods: {
		getLocaleDict() {
			axios.get('/language').then(request => {
				window.locale = request.data;
			});

		}
	},
	mounted() {
		this.getLocaleDict()
	}
});
