
window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

 try {
 	
 	window.$ = window.jQuery = require('jquery');
 	window.Popper = require('popper.js').default;
 	
 	require('bootstrap');

 } catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */
 import {Noty} from './core/noty.js';
 window.Noty = Noty;

 window.moment = require('moment');

 window.axios = require('axios');
 window.datepicker =require('eonasdan-bootstrap-datetimepicker-bootstrap4beta');
 window.select =require('bootstrap-select');
 window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
 window.toastr = require('toastr');
 window.swal = require('sweetalert2');
 window.numeral = require('numeral');
 window.years =[moment().year() + 1, moment().year(), moment().year()-1, moment().year()-2];

 window.$.fn.datetimepicker.defaults.icons = {
 	time: 'fa fa-clock-o',
 	date: 'fa fa-calendar',
 	up: 'fa fa-chevron-up',
 	down: 'fa fa-chevron-down',
 	previous: 'fa fa-chevron-left',
 	next: 'fa fa-chevron-right',
 	today: 'fa fa-dot-circle-o',
 	clear: 'fa fa-trash',
 	close: 'fa fa-times'
 };
/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

 let token = document.head.querySelector('meta[name="csrf-token"]');

 if (token) {
 	window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
 } else {
 	console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
 }

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

 import Echo from 'laravel-echo'

 window.Pusher = require('pusher-js');

 window.Echo = new Echo({
 	broadcaster: 'pusher',
 	key: '2c61c90b44323723bd45',
 	cluster: 'eu',
 	encrypted: true
 });
