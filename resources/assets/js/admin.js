/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap')
window.$ = $.extend(require('../../../node_modules/jquery-ui-dist/jquery-ui.min.js'))
window.Vue = require('vue')
window.busi = new Vue

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import { GMap } from './core/maps.js'
import { Table } from './core/table.js'
import { Form } from './core/form.js'
import { Arr } from './core/array.js'

window.GMap = GMap
window.Table = Table
window.Form = Form
window.Highcharts = require('highcharts')
window.Arr = new Arr()

Vue.component('admin', require('./views/admin.vue'))
Vue.component('dashboard', require('./views/dashboard.vue'))
Vue.component('categories', require('./views/categories.vue'))
Vue.component('objects', require('./views/objects.vue'))
Vue.component('clients', require('./views/clients.vue'))
Vue.component('segments', require('./views/segments.vue'))
Vue.component('companies', require('./views/companies.vue'))
Vue.component('products', require('./views/products.vue'))
Vue.component('contact-list', require('./views/contact-list-control.vue'))
Vue.component('users', require('./views/users.vue'))
Vue.component('documents', require('./views/documents.vue'))
Vue.component('ticket-system', require('./views/ticket-system.vue'))
Vue.component('ticket', require('./components/ticket.vue'))
Vue.component('editor', require('./components/editor.vue'))
Vue.component('upload-control', require('./components/upload-control.vue'))

const app = new Vue({
  el: '#app',

  mounted() {
    // set moment language
    moment().locale(document.getElementsByTagName('html')[0].getAttribute('lang'))
  }
})
