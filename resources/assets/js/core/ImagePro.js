export class  ImagePro{

	constructor(id = null, src= null){


		if(src != null)
		{
			this.id=id;
			this.image = new Image();
			this.image.src = src;
			this.src = src;
			this.loaded = false;
		}
	}

	load(){
		let local = this;
		return new Promise((resolve, reject) =>{

			$(this.image).bind("load", () =>  {
				
				this.loaded = true;
				console.log(this);
  				// do stuff
  				resolve(this.image);

  			}).on("error", error =>{
  				console.log(this);
  				reject(this.image);
  			});

  		});
	}


}