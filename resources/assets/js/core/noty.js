export class Noty
{
	constructor(id, title, message){
		this.id = id;
		this.title = title;
		this.message = message;
	}
}