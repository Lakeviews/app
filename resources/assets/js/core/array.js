export class Arr {

  constructor () {
    this.items = []
  }

  valuesExists (valueToFind, dataArray) {
    return dataArray.filter(value => {
        return value == valueToFind
      }).length > 0
  }

  addUniqueValue (valueToAdd, dataArray) {
    let foundValue = this.valuesExists(valueToAdd, dataArray)
    if (!foundValue)
      dataArray.push(valueToAdd)
    else {
      let index = dataArray.findIndex(value => {
        value == valueToAdd
      })

      dataArray.splice(index, 1)
    }

    return dataArray
  }

  add (item) {
    this.item.push(item)
  }

  addUnique (model, key = null) {
    let item = this.items.filter((itemFiltered, index) => {
      if (key == null)
        return model == itemFiltered
      else
        return model[key] == itemFiltered[key]
    })
    console.log(item)

    if (item.length == 0)
      this.items.push(model)
  }

  remove (value, key = null) {
    let foundIndex = null

    this.items.forEach((itemFiltered, index) => {
      if (key == null && itemFiltered == value)
        foundIndex = index
      else if (itemFiltered[key] == value)
        foundIndex = index
    })

    if (foundIndex != null)
      this.items.splice(foundIndex, 1)
  }

  clear () {
    this.items = []
  }

  addRemoveValue (data, value) {
    let found = data.filter(item => {

      return item == value
    })

    if (found.length == 0)
      data.push(value)
    else
      data.splice(data.indexOf(value) , 1)

    return data;
  }
  addValueUnique (data, value) {
    let found = data.filter(item => {

      return item == value
    })

    if (found.length == 0)
      data.push(value)

    return data
  }

  removeValue (data, value) {
    let foundIndex = -1

    let index = data.every((item, index) => {

      if (item == value)
        return index
    })

    console.log(index)

    return data
  }

}
