export class Link
{
	constructor(url){
		this.url = url;
		this.loaded = false;
	}

	load(){

		let local = this;
		return new Promise((resolve, reject) =>{

			axios.get(this.url).then(response =>{
				this.loaded = true;
				console.log(this);
				resolve(this);

			}).catch(error =>{
				console.log(this);
				reject(this);
			});

		});
	}
}