export class GMap{

	

	constructor(lat, lng, id){

	
		this.map = null;
		this.id = id;
		this.lat= lat;
		this.lng = lng;

		let local = this;
	
			local.init();
			local.addMarker()
		
	}

	addMarker(){

		let bounds  = new google.maps.LatLngBounds();
		let lat_lng = new google.maps.LatLng(this.lat, this.lng);
		bounds.extend(lat_lng);
		let local = this;
		var image = '../images/marker.png';
		var marker = new google.maps.Marker({
			position: lat_lng,
			map: local.map,
			icon: image,
			draggable: false,
			animation: google.maps.Animation.DROP
		});

	}


	init(){

		this.map = new google.maps.Map(document.getElementById('map'),this.options());
	}

	options(){
		return {
			styles: this.styles(),
			center: {lat: this.lat, lng: this.lng},
			zoom: 13,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}
	}

	styles(){
		return  [{ "stylers": [{ "hue": "#ffffff" }, { "invert_lightness": false }, { "saturation": -200 }, { "lightness": 33 }, { "gamma": 0.5 }] }, 
		{ "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#5a96c9" }] }, 
		{ "featureType": "road.highway", "elementType": "geometry", "stylers": [{ "lightness": 0 }, 
		{ "hue": "#ff1a00" }, { "color": "#999999" }, { "visibility": "simplified" }] }];
	}



}