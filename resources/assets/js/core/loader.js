export class Loader{

	constructor(){
		this.enable = false;
		this.message ="LOADING";
		this.visible = false;
	}

	show(message = "LOADING"){

	
		this.message = message;
		this.enable = true;
		this.visible = true;
	}

	hide(interval = 0){
		
		let local = this;

		setTimeout(()=>{
			local.enable = false;
			setTimeout(() =>{
				this.visible = false;
			}, 1000);
		}, interval);
	}

}