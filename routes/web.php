<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */


 
Route::get('/', 'HomeController@index')->name('root');
Route::get('home', 'HomeController@index')->name('home');
Route::get('admin', 'HomeController@admin')->name('admin');
Route::get('admin/dashboard', 'HomeController@dashboard');
Route::get('language/{locale}', 'HomeController@changeLocale');
Route::get('language', 'HomeController@language');
Route::get('dictionary', 'HomeController@dictionary');

/*AUTHROZITAION*/
Route::get('login', 'AuthController@loginView')->name('login');
Route::post('login', 'AuthController@login')->middleware('guest');
Route::post('recover', 'AuthController@recover')->name('recover');
Route::get('recover/{token}', 'AuthController@recoverByToken');
Route::get('office/{token}', 'AuthController@office');
Route::get('logout', 'AuthController@logout')->name('logout');
Route::post("/remember", 'AuthController@remember');
// Route::post('broadcasting/auth','AuthController@broadcast');

/*CATEGORIES*/
Route::resource('categories', 'CategoriesController');

/*OBJECTS*/
Route::resource('objects', 'ObjectsController');

/*CLIENTS*/
Route::resource('clients', 'ClientsController');

/*SEGMENTS*/
Route::resource('segments', 'SegmentsController');

/*CLIENT CONTACTS*/
Route::get('segment/pdf/{id}', 'SegmentContactsController@pdf');
Route::get('segment/excel/{id}', 'SegmentContactsController@excel');
Route::get('segment/calls', 'SegmentContactsController@calls');
Route::get('/segment/contacts/recall/{contact}/{value}', 'SegmentContactsController@recall');
Route::resource('segment/contacts', 'SegmentContactsController');

/*COMPANIES*/
Route::resource('companies', 'CompaniesController');

/*PRODUCTS*/
Route::get('products/documents/year-monthly/{year}', 'ProductsController@productsDocumentsYearMontly');
Route::resource('products', 'ProductsController');
/*USERS*/
Route::get('dashboard', 'UsersController@dashboardView')->name('dashboard');
Route::get('dashboard/tickets/{id}', 'UsersController@dashboardViewTicket');

Route::get('profile', 'UsersController@profileView')->name('profile');

/*MSG*/
Route::post('send/message', 'UsersController@sendMessage');

Route::get('user/insurances/view', 'UsersController@insurancesView');
Route::get('user/insurances/{year}', 'UsersController@insurances');

Route::get('user/relocation/view', 'UsersController@relocationView');
Route::get('user/relocation', 'UsersController@relocation');

Route::get('user/storage/view', 'UsersController@storageView');
Route::get('user/storage', 'UsersController@storage');

Route::get('user/tickets/view', 'UsersController@ticketsView');
Route::get('user/tickets', 'UsersController@tickets');

Route::resource('users', 'UsersController');

/*DOCUMENTS*/
Route::resource('documents', 'DocumentsController');

/*TICKETS*/
Route::get('tickets/changed/{workflow}', 'TicketsController@ticketsWorkflow');
Route::resource('tickets', 'TicketsController');

/*S3 Files*/
Route::get('s3/{entity}/{id}/{index}', 'HomeController@s3');
