<?php

use Illuminate\Foundation\Inspiring;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
 */

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');


Artisan::command('backup:db', function () {

    $today = Carbon::now();

    $filename = $today->toDateString() . '.sql';
    $cmd = "mysqldump -u ".env('DB_USERNAME')." -p".env('DB_PASSWORD')." ".env('DB_DATABASE')." > /home/forge/lakeviews.ch/storage/app/{$filename}";
    echo $cmd."\n";
    exec($cmd, $output, $return_value);

    $server_path = "backups/";
    
    $sevenDaysAgo = $today->addDay(-7);


    //delete backup from 7 days ago
    $sevenDaysAgoBackupPath = $server_path . $sevenDaysAgo->toDateString().".sql";
    
   
    if (Storage::disk('s3')->exists($sevenDaysAgoBackupPath)) {
        echo "deleting previos backup : {$sevenDaysAgoBackupPath} \n";
        Storage::disk('s3')->delete($sevenDaysAgoBackupPath);
    }
 
    $today->addDays(7);

    if (Storage::disk('local')->exists($filename)) {
      
        $todayBackup = $server_path . $today->toDateString().".sql";

        $file = Storage::disk('local')->get($filename);
       
        echo "saving new backup : {$todayBackup} \n";
        Storage::disk('s3')->put($todayBackup, $file);
    }

})->describe('Backup the database');



